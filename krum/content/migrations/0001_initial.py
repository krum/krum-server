# -*- coding: utf-8 -*-
import datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding model 'Content'
        db.create_table('content_content', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('the_file', self.gf('django.db.models.fields.files.FileField')(max_length=100)),
            ('mimetype', self.gf('django.db.models.fields.CharField')(max_length=200, blank=True)),
            ('original_hash', self.gf('django.db.models.fields.CharField')(default=None, max_length=40, unique=True, null=True, blank=True)),
            ('original_path', self.gf('django.db.models.fields.CharField')(max_length=255)),
            ('added_timestamp', self.gf('django.db.models.fields.DateTimeField')(auto_now_add=True, blank=True)),
            ('metadata', self.gf('django.db.models.fields.related.ForeignKey')(blank=True, related_name='content', null=True, to=orm['metabase.MetaBase'])),
        ))
        db.send_create_signal('content', ['Content'])


    def backwards(self, orm):
        # Deleting model 'Content'
        db.delete_table('content_content')


    models = {
        'content.content': {
            'Meta': {'object_name': 'Content'},
            'added_timestamp': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'metadata': ('django.db.models.fields.related.ForeignKey', [], {'blank': 'True', 'related_name': "'content'", 'null': 'True', 'to': "orm['metabase.MetaBase']"}),
            'mimetype': ('django.db.models.fields.CharField', [], {'max_length': '200', 'blank': 'True'}),
            'original_hash': ('django.db.models.fields.CharField', [], {'default': 'None', 'max_length': '40', 'unique': 'True', 'null': 'True', 'blank': 'True'}),
            'original_path': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'the_file': ('django.db.models.fields.files.FileField', [], {'max_length': '100'})
        },
        'metabase.genre': {
            'Meta': {'object_name': 'Genre'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '40'})
        },
        'metabase.genrelink': {
            'Meta': {'unique_together': "(('meta', 'genre'),)", 'object_name': 'GenreLink'},
            'genre': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['metabase.Genre']"}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'meta': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['metabase.MetaBase']"})
        },
        'metabase.mediatype': {
            'Meta': {'object_name': 'MediaType'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '30'})
        },
        'metabase.metabase': {
            'Meta': {'unique_together': "(('source', 'name', 'year', 'media_type'),)", 'object_name': 'MetaBase'},
            'added_timestamp': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            'genres': ('django.db.models.fields.related.ManyToManyField', [], {'to': "orm['metabase.Genre']", 'through': "orm['metabase.GenreLink']", 'symmetrical': 'False'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'in_library': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'media_type': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['metabase.MediaType']"}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '50'}),
            'other_id': ('django.db.models.fields.IntegerField', [], {}),
            'source': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['metabase.MetaSource']"}),
            'year': ('django.db.models.fields.CharField', [], {'max_length': '4'})
        },
        'metabase.metasource': {
            'Meta': {'object_name': 'MetaSource'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '30'})
        }
    }

    complete_apps = ['content']