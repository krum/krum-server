"""Defines a handler that allows partial uploads and streamed downloads."""
import logging
import re
import hashlib
from tornado import gen
from tornado import httputil
from tornado import web
from tornado.ioloop import IOLoop
from tornado.web import asynchronous, HTTPError

from krum.utilities.basehandler import BaseHandler
from .models import Content

logger = logging.getLogger(__name__)

READ_SIZE = 64*1024     # 64k
RANGE_REG = re.compile(r'bytes ([0-9]+)-([0-9]+)\/([0-9]+)')     # doesn't accept '*', need to know

@web.stream_request_body
class HashingHandler(BaseHandler):
    """
    Quick test class to see if I can upload large files via
    chrome/firefox's inbuilt blob upload handling.
    """
    def initialize(self):
        self.hasher = hashlib.sha1()
        self.body_len = 0

    def prepare(self):
        logger.info('Content-Length: {}'.format(
                    self.request.headers.get('Content-Length','unknown')))

    def data_received(self, data):
        self.hasher.update(data)
        self.body_len += len(data)

    def post(self):
        hexhash = self.hasher.hexdigest()
        self.write({
            'hash': hexhash,
            'hlen': self.request.headers.get('Content-Length','unknown'),
            'tlen': self.body_len
        })

class ContentUpload(object):
    """
    Helper class to move all the content upload specific lifetime crud
    out of StreamingContentHandler.
    """
    def __init__(self, content_obj):
        self.content_obj = content_obj
        # dumb bug with Django file objects and getting the correct mode, so do it myself
        self.file_ = open(content_obj.the_file.path, 'r+b')
        self.hasher = hashlib.sha1()

    def append_data(self, data):
        """Writes data out to the content file and adds it to the hash."""
        self.file_.write(data)
        self.hasher.update(data)

    def save_hash(self):
        """Update the Content model with the current hash as a hex digest."""
        hexhash = self.hasher.hexdigest()
        self.content_obj.original_hash = hexhash
        self.content_obj.save()

    def clean_up(self):
        """Close the file and clean up references."""
        self.content_obj = None
        if self.file_ is not None:
            self.file_.close()
            self.file_ = None

@web.stream_request_body
class StreamingContentHandler(BaseHandler):
    """
    Provides a streaming friendly Content download and upload handler.

    Supports Content-Range header on both GET and POST requests. This
    allows streaming players to work (mplayer/browsers/etc) and
    uploads to be done in small chunks (20MB) and resumes/retries/etc.
    """
    def prepare(self):
        self.upload = None

        # Futher stuff is only used by POST for streaming upload
        if self.request.method != 'POST':
            return

        # get the content object
        try:
            id_ = self.path_args[0]
            content_obj = Content.objects.get(pk=id_)
        except Content.DoesNotExist:
            raise HTTPError(404)

        self.upload = ContentUpload(content_obj)

    def on_connection_close(self):
        self.on_finish()

    def on_finish(self):
        '''Clean up the open file!!'''
        if self.upload is not None:
            self.upload.clean_up()
            self.upload = None

    def data_received(self, data):
        """Add the data on to the file."""
        if self.upload is not None:
            self.upload.append_data(data)

    def post(self, id_):
        """Upload is complete, so just save the hash off for safekeeping."""
        assert self.upload is not None and "POSTs must have an upload"

        self.upload.save_hash()
        self.set_status(204)

    def head(self, id_):
        """Useful for getting file meta-data (Chrome/MPlayer use this)."""
        return self.get(id_, False)

    @gen.coroutine
    def get(self, id_, include_body=True):
        """Streams the content data on request."""
        # get the content object
        try:
            content_obj = Content.objects.get(pk=id_)
        except Content.DoesNotExist:
            raise HTTPError(404)

        # Note: Last-modified stuff is not done because this is only
        #   used for streaming content (which will not be cached).

        # parse the range headers
        # NOTE: Below range handling is taken from tornado source code
        #   thanks guys! All I needed was your last content read bit
        #   to be async.
        request_range = None
        range_header = self.request.headers.get("Range")
        if range_header:
            # As per RFC 2616 14.16, if an invalid Range header is specified,
            # the request will be treated as if the header didn't exist.
            request_range = httputil._parse_request_range(range_header)

        size = len(content_obj.the_file)
        if request_range:
            start, end = request_range
            if (start is not None and start >= size) or end == 0:
                # As per RFC 2616 14.35.1, a range is not satisfiable only: if
                # the first requested byte is equal to or greater than the
                # content, or when a suffix with length 0 is specified
                self.set_status(416)  # Range Not Satisfiable
                self.set_header("Content-Type", "text/plain")
                self.set_header("Content-Range", "bytes */%s" % (size, ))
                return
            if start is not None and start < 0:
                start += size
            if end is not None and end > size:
                # Clients sometimes blindly use a large range to limit their
                # download size; cap the endpoint at the actual file size.
                end = size
            # Note: only return HTTP 206 if less than the entire range has been
            # requested. Not only is this semantically correct, but Chrome
            # refuses to play audio if it gets an HTTP 206 in response to
            # ``Range: bytes=0-``.
            if size != (end or size) - (start or 0):
                self.set_status(206)  # Partial Content
                self.set_header("Content-Range",
                                httputil._get_content_range(start, end, size))
        else:
            start = end = None

        # set up the headers
        self.set_header('Content-Type', content_obj.mimetype)
        self.set_header('Accept-Ranges', 'bytes')

        # normalise start/end (httputil._parse returns python style end
        #                      rather than http-style inclusive end)
        start = start or 0
        end = end or size
        content_length = end - start
        self.set_header("Content-Length", content_length)

        if include_body:
            for chunk in self.get_content(content_obj, start, content_length):
                self.write(chunk)
                yield self.flush()
        else:
            assert self.request.method == 'HEAD'

    def get_content(self, content_obj, start, content_length):
        """Yields chunks of up to 64k bytes in size of the file contents."""

        # get the file and if it's not there, error
        file_ = content_obj.the_file.file
        file_.open(mode='rb')
        file_.seek(start)

        bytes_remaining = content_length
        while bytes_remaining > 0 and not self.request.connection.stream.closed():
            data = file_.read(min(bytes_remaining, READ_SIZE))
            bytes_remaining -= len(data)
            yield data
