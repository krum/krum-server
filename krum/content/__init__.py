from __future__ import absolute_import
'''
    This module allows for files to be imported into krum.

    It tracks them optionally linked against another model.

    By default all files are brought in to a common area.
    Content can be re-pathed relative to the krum data/content folder.
'''

import hashlib
import types
import mimetypes
import os.path as op

from django.core.files import File

from .models import Content, CONTENT_PATH

# TODO: Need to make this a bit less hard-cody :)
VID_EXTNS = ['avi', 'mkv', 'mp4', 'm4v', 'mpg', 'ogg', 'webm']

def sha1file(path):
    hasher = hashlib.sha1()
    with open(path, 'rb') as f:
        while True:
            data = f.read(1024)
            if not data:
                break
            hasher.update(data)

    return hasher.hexdigest()

def import_file(path, move_file=False):
    if not op.exists(path):
        raise Exception('No such file')

    path = op.abspath(path)
    if op.normpath(op.commonprefix([path, CONTENT_PATH])) == op.normpath(CONTENT_PATH):
        raise Exception('Cannot import a file from within Krums content directory.')

    file_hash = sha1file(path)
    if Content.objects.filter(original_hash=file_hash).exists():
        raise Exception('File with same hash is already present')

    # add in to DB
    cf = Content()
    cf.original_hash = file_hash
    cf.original_path = path[-255:] # truncate left rather than right
    cf.mimetype = mimetypes.guess_type(path)[0] or ''
    dfile = File(open(path, 'rb'))

    if move_file:
        # mark it as a django "temp" file so it'll move instead of copying
        def f(self):
            return self.file.name
        dfile.temporary_file_path = types.MethodType(f, dfile)
        dfile.size = op.getsize(path)

    try:
        cf.the_file.save(op.basename(path), dfile)
    except:
        cf.delete()
    return cf

def guess_deets(content):
    '''Takes an imported piece of content and tries to map it to a known piece of metadata.
    If the match is good enough, it will link the content to the metadata, but leave the link
    as unconfirmed'''

    pass

