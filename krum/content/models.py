"""
Defines the basic models and behaviour (file deletion) for Content
storage.
"""
from django.db import models
from django.core.files.storage import FileSystemStorage
from django.db.models.signals import post_delete, post_save
from django.dispatch import receiver
from krum import cfg
from krum.metabase.models import MetaBase

CONTENT_PATH = cfg['content_path']
DEFAULT_FOLDER = 'other_content/%Y%m%d'

# this allows media files to be stored separate to CSS, JS, sqlite DB, etc.
content_storage = FileSystemStorage(location=CONTENT_PATH, base_url='/content/')

class Content(models.Model):
    """Represents a single file stored within Krum.

    The file is then linked against some metadata to indicate what it
    is, e.g. Season 1 Episode 12 of Seinfeld or such.
    """
    the_file        = models.FileField(upload_to=DEFAULT_FOLDER,
                                       storage=content_storage)
    mimetype        = models.CharField(max_length=200, blank=True)
    original_hash   = models.CharField(max_length=40, unique=True, null=True,
                                       blank=True, default=None)
    original_path   = models.CharField(max_length=255)
    added_timestamp = models.DateTimeField(auto_now_add=True)
    metadata        = models.ForeignKey(MetaBase, null=True, blank=True,
                                        related_name='content', unique=True)

# delete the files when content models are deleted
@receiver(post_delete, sender=Content)
def content_delete_file(sender, **kwargs):
    '''
    This signal handler will delete the actual file that "the_file" refered to.

    By default, django doesn't do this.
    '''
    instance = kwargs['instance']

    if instance.the_file.name != '':
        instance.the_file.delete(False)

    # TODO: check if the folder it was in is now empty and remove it

@receiver(post_save, sender=Content)
def content_add_metadata_to_library(sender, **kwargs):
    '''
    This makes sure that if you link some content to a MetaBase instance, that
    that MetaBase instance is "in your library". Otherwise it wouldn't show up
    in requests to /api/1/library
    '''
    instance = kwargs['instance']

    if instance.metadata_id is not None:
        instance.metadata.add_to_library()
