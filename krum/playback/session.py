"""Defines the core classes for Player session tracking and control."""
import logging
from functools import partial
from tornado import gen
from tornado.web import asynchronous, HTTPError
from tornado.ioloop import IOLoop

from krum.utilities.basehandler import BaseHandler
from krum.utilities.json import is_json, to_json, from_json
from krum.utilities.helpers import extract_attrs, update_attributes

import krum.playback.models as m
from .remoteplayer import RemotePlayer

logger = logging.getLogger(__name__)

class NotUniqueError(Exception):
    """A create request has a name that already exists."""
    def __init__(self, name):
        msg = '"{}" is not unique.'.format(name)
        super(NotUniqueError, self).__init__(msg)

class PlayerInUseError(Exception):
    """
    A session can't be changed to use the requested player because
    another session is already using it.
    """
    def __init__(self, player_path, other_session):
        msg = 'The player "{}" is being used by "{}"'.format(player_path, other_session)
        super(PlayerInUseError, self).__init__(self, msg)
        self.player_path = player_path
        self.other_session = other_session

class NoSuchPlayerError(Exception):
    """The requested player does not exist."""
    pass

class SessionProxy(object):
    """
    Maintains the state of a single Playback session. Interleaving
    the requests from clients via process_request and events that
    come back from the Remote Player itself via process_event.

    On return, the session is persisted to SQL.
    The callback is called once the remote players state is up-to-date.
    It will always be called after this method has returned.
    """

    DEFAULTS = {
        'playlist': [],
        'paused': False,
        'position': 0,
        'volume': 1.0,
        'player_path': '',
    }
    def __init__(self, playback_set, name, content_path):
        super(SessionProxy, self).__init__()

        # Precondition: There isn't an active session with this name
        # Note: There can be a saved session with this name

        # ensure the model exists, and update it's properties
        logger.debug('Initialising new playback session')
        try:
            self._model = m.Session.objects.get(name=name)
        except m.Session.DoesNotExist:
            self._model = m.Session(name=name)

            self._update_model(SessionProxy.DEFAULTS)

        # save the boring stuff
        self._playback_set = playback_set
        self._name = name
        self._player = None
        self._content_path = content_path

        # this info comes from the player
        self._duration = None       # will recieve in playback events
        self._history_model = None  # used to shortcut history updates in the common case

    @property
    def id(self):
        return self._model.id
    @property
    def name(self):
        return self._name

    def get_current_state(self):
        """
        A shortcut method to get a dictionary of the current playback state.
        This is the state without the id and player_path
        """
        state = extract_attrs(self._model, SessionProxy.DEFAULTS.keys())
        state['duration'] = self._duration

        return state

    def on_player_lost(self, reason):
        # TODO: Need to signal to front end too
        self._player = None
        self._update_model({'player_path': ''})

    def _update_model(self, new_field_values):
        """
        Saves new session attribute values to the model

        Args
            new_field_values {dict}: Attr->value pairs.
        """
        logger.debug('Saving session model. Changed fields: {}'.format(new_field_values))
        update_attributes(self._model, new_field_values, SessionProxy.DEFAULTS.keys(), False)
        self._model.save()

    @gen.coroutine
    def _clear_player_state(self):
        """Clear the playlist of the current remote player."""
        if self._player is None:
            return;

        logger.debug('Clearing player')
        status, body = yield self._player.send_request(
            'PATCH', '/playback', {'playlist':[]}, on_clear
        )
        if not (200 <= status < 300):
            logger.warn('Got non-2xx response from Remote Player: {}'.format(body))
        else:
            logger.debug('Player cleared')

    @gen.coroutine
    def _acquire_new_player(self, new_player_path):
        """Acquires a new player and releases and clears the old one."""

        # aquire the new one, do first in case of failure
        if (new_player_path or '') == '':
            logger.debug('Removing player from session {}'.format(new_player_path))
            new_player = None
        else:
            logger.debug('Aquiring new player for session')
            new_player = self._playback_set.acquire_player(new_player_path, self)

        # push out a request to clear the old one, retry apply with new player :)
        if self._player:
            yield self._clear_player_state()
            self._playback_set.release_player(self._model.player_path, self)

        # save the change
        self._player = new_player
        self._update_model({'player_path': new_player_path})

    @gen.coroutine
    def apply_state_change(self, delta):
        # TODO: This code is intertwined untestable rubbish. Need to pull out
        #   steps into seperate methods to allow testing...
        '''
        Sends delta out to the Remote Player then persists.

        Return:
            A future that will resolve with the status returned from
            the remote player.
        '''

        # validate fields
        pos_is_relative = False
        if 'position' in delta:
            orig_pos = delta['position']
            new_pos = float(orig_pos)

            if (type(orig_pos) == str and len(orig_pos) > 0
               and orig_pos[0] in ('+','-')):

                new_pos = '{}{}'.format(orig_pos[0], abs(new_pos))
                pos_is_relative = True

            delta['position'] = new_pos

        if 'volume' in delta:
            delta['volume'] = float(delta['volume'])

        logger.debug('Got state change request on session {}, new state {}'.format(self.id, delta))

        # if we're changing player
        new_path = delta.get('player_path')
        if isinstance(new_path, str) and (self._player == None or new_path != self._model.player_path):
            logger.debug('Session setting up a new player, path: {}'.format(new_path))
            yield self._acquire_new_player(new_path)

            # it's a new player, so resend entire state
            full_data = extract_attrs(self._model, SessionProxy.DEFAULTS.keys())
            full_data.update(delta)
            delta = full_data

        # remove relative position changes from model update
        model_delta = delta.copy()
        if pos_is_relative:
            del model_delta['position']

        if self._player is None:
            logger.debug('Session has no player, saving and done')
            self._update_model(model_delta)
            return 204

        # send of the request to the player
        logger.debug('Syncing Session state change to player')
        status, body = yield self._player.send_request('PATCH', '/playback', delta)

        if 200 <= status < 300:
            logger.debug('Session syncing done successfully')
            self._update_model(model_delta)
        else:
            logger.error('Non 2xx response, {}, from Remote Player: {}'.format(status, body))

        return status

    def apply_player_event(self, event):
        '''Events come back from remote players to signal position progress and
        completion of a playlist item. This method incorporates those updates
        into the locally cached state.
        '''

        data = event.body
        logger.debug('Got state EVENT on session {}, change {}'.format(self.id, data))

        # update our version of the model if we need to
        if len({'playlist', 'position'} & set(data.keys())) > 0:
            self._update_model(data)

            # TODO: Notify the front end

        if 'duration' in data:
            self._duration = data['duration']

        # it's 0 if we haven't been told
        playlist = self._model.playlist
        position = data.get('position', 0)
        # update playback history
        if len(playlist) > 0:
            self._update_history(playlist[0], position)

    def _update_history(self, playlist_item, position):
        '''Updates the current history model or makes a new one if position is for a
        different piece of content.

        :param content_url: The URL of the media being played, e.g. /api/1/content/{id}/data
            for media on krum being retrieved by API version 1.
        :param position: The current playback position.
        :return: None
        '''

        content_url = playlist_item['data_url']

        # if this is a different piece of content
        if self._history_model is None or self._history_model.content_url != content_url:
            # make a new entry
            self._history_model = m.History(
                session_id=self.id,
                content_url=content_url
            )

            # try to give us a decent link
            if self._content_path is not None:
                self._history_model.find_metadata_id(self._content_path)

        self._history_model.max_playback_position = position
        self._history_model.save()

    def player_removed(self):
        '''This is called if the player is removed forcibly from this session, e.g. it disconnected from Krum.'''
        self._player = None
        self._model.player_path = None
        self._model.save()
        self._duration = None    # we're not playing, we no longer know our duration

        # TODO: Notify the front-end

    def to_json(self, reverse_url):
        data = {
            'path': reverse_url('api.1.playback.session.item', self.id),
            'name': self._name,
            'duration': self._duration,
        }
        data.update(extract_attrs(self._model, ['id', 'player_path', 'playlist',
            'paused', 'position', 'volume']))
        return data

class RemotePlayerProxy(object):
    def __init__(self, playback_set):
        '''
        This class handles the delegate events of a RemotePlayer and applies
        the changes through to the player_state it was constructed with.
        '''
        self._playback_set = playback_set
        self._player = None
        self._session = None

    @property
    def path(self):
        return self._player.path
    @property
    def id(self):
        return self._player.id
    @property
    def name(self):
        return self._player.name
    @property
    def guid(self):
        return self._player.guid
    @property
    def isvisible(self):
        return self._player.isvisible

    def on_start_register(self, player):
        self._player = player

    def on_finish_register(self, player, path, name):
        self._playback_set.add_player(path, self)

    def on_event(self, player, event):
        self._playback_set.player_event(self, event)

    def on_close(self, player):
        self._playback_set.remove_player(self.path)

    def send_request(self, *args, **kwargs):
        return self._player.socket.send_request(*args, **kwargs)

    def get_status(self):
        """Shortcut to ask the Remote player what it's status is."""
        return

class PlaybackSet(object):
    """
    Holds onto the set of Remote Players and Sessions and passes. It also
    provides a means for sessions to acquire and release Players.
    """

    def __init__(self, io_loop=None, content_path=None):
        super(PlaybackSet, self).__init__()

        self.io_loop = io_loop or IOLoop.current()
        self._content_path = content_path

        # we come from nothing
        self.players_by_path = {}        # player path => RemotePlayerProxy
        self.sessions_by_id = {}         # session id => SessionProxy
        self.owners_by_player_path = {}  # player path => SessionProxy

        # pre-build all the session proxies
        for name in m.Session.objects.values_list('name', flat=True):
            self.create_session(name)

        # clear the status of the players
        m.RegisteredPlayer.objects.all().update(active=False)

    @property
    def players(self):
        '''Returns a sequence of all the currently active, visible players'''
        return [x for x in self.players_by_path.values() if x.isvisible]

    def get_player(self, path):
        return self.players_by_path[path]

    @property
    def sessions(self):
        return self.sessions_by_id.values()

    def get_session(self, session_id):
        return self.sessions_by_id[int(session_id)]

    def on_open(self, krum_socket):
        '''
        This is the only RemotePlayerDelegate method that PlaybackSet
        implements. This is used to get a reference to each newly
        connected RemotePlayers KrumSocket and pass it off to a newly
        created RemotePlayer (layers onto the KrumSocket protocol to
        make it a RemotePlayer socket) and a RemotePlayerProxy which
        is used to track our per-Remote Player state.

        The RemotePlayerProxy is responsible for adding (and removing)
        itself to(from) this PlaybackSet.

        Args:
            krum_socket {KrumSocket} - The KrumSocket that was
            initialised by the protocol (KrumSocketHandler in this case).
        '''
        # this guy is the delegate that translates RemotePlayer events into our gear
        proxy = RemotePlayerProxy(self)
        # this guy implements the RemotePlayer protocol on top of a raw KrumSocket
        p = RemotePlayer(krum_socket, proxy)

        # start the registration process
        fut = p.start_registration()
        self.io_loop.add_future(fut, self.on_remote_player_register_complete)

    def on_remote_player_register_complete(self, future):
        """The registration process completed. Finish it off."""
        try:
            future.result()
        except:
            logger.exception('Remote Player registration failed: {}')

    def add_player(self, path, remote_player_proxy):
        '''Adds the player to the collection'''
        logger.debug('Adding player to playback set, {}, guid: "{}"'
                     .format(path, remote_player_proxy.guid))
        self.players_by_path[path] = remote_player_proxy

    def remove_player(self, path):
        '''
        Removes a player from the collection. This will also notify the session
        that is using it that the player has been "lost"
        '''
        logger.debug('Removing player from playback set, {}'.format(path))

        del self.players_by_path[path]

        # TODO: tell the session it has lost it's player
        try:
            session = self.owners_by_player_path.pop(path)
            session.on_player_lost('Player disconnected')
        except KeyError:
            pass

    def acquire_player(self, player_path, session):
        '''
        Attempts to "acquire" the player with the given path for the
        given session. Returns the requested player on success and the
        requesting Session will "own" the player until the session
        releases it.

        Note: This is not thread-safe.

        Raises
            NoSuchPlayerError - If there is no player with that path.
            PlayerInUseError - If another session already "owns" the
                requested player.
        '''
        try:
            player = self.players_by_path[player_path]
        except KeyError:
            raise NoSuchPlayerError('Requested: {}, available: {}'
                .format(player_path, self.players_by_path.keys()))

        # make sure no other session has it already
        owner = self.owners_by_player_path.get(player_path)
        if owner is not None and owner != session:
            raise PlayerInUseError(player_path, owner)

        self.owners_by_player_path[player_path] = session

        logger.debug('Player "{}" aquired by "{}"'
                     .format(player_path, session.name))

        return player

    def release_player(self, player_path, session):
        """
        Release a player that was previously acquired be a session.
        """
        try:
            sess = self.owners_by_player_path[player_path]
            if sess != session.name:
                raise ValueError('Cannot release player {}, "{}" does not own, "{}" owns this'
                                 .format(player_path, session.name, sess.name))
            else:
                del self.owners_by_player_path[player_path]
        except KeyError:
            raise ValueError("Player {} is not currently owned by anyone".format(player_path))

        logger.debug('Player "{}" released by "{}"'.format(player_path, session.name))

    def player_event(self, remote_player_proxy, event):
        '''Called when the Remote Player sends a KrumSocket Event.

        The event is sent to the relevant session for handling.

        Args
            player - The player from which the event came.
            event_data - The actual event data.

        Return
            None
        '''
        try:
            sess = self.owners_by_player_path[remote_player_proxy.path]
            sess.apply_player_event(event)
        except KeyError:
            pass

    def session_for_name(self, name):
        """
        Returns the named session or raises KeyError if it's not present.
        """
        for sess in self.sessions_by_id.values():
            if sess.name == name:
                return sess

        raise KeyError(name)

    def create_session(self, name):
        # TODO: change this to getsession, add_session. Remove Session creation logic from here.
        #       This is just a collection.
        '''
        Creates the session and adds it to the collection. Does not kick of any
        synchronisation.
        '''

        # try to find it first
        for sess in self.sessions_by_id.values():
            if sess.name == name:
                raise NotUniqueError(name)

        sess = SessionProxy(self, name, self._content_path)
        logger.debug('Saving session with ID: {}'.format(sess.id))
        self.sessions_by_id[sess.id] = sess
        return sess

def player_view(player):
    return {
        'id': player.id,
        'path': player.path,
        'name': player.name
    }

class RemotePlayerListHandler(BaseHandler):
    '''
    Provides a way to enumerate the registered Remote Players.
    '''
    def initialize(self, state):
        self._playback_set = state

    def get(self):
        players = [player_view(x) for x in self._playback_set.players]
        self.write({'list': players})

class RemotePlayerHandler(BaseHandler):
    def initialize(self, state):
        self._playback_set = state

    @gen.coroutine
    def get(self, id_):
        """
        Returns the boring info as well as retrieves the status from
        the player itself, useful to check the player has the same
        state as the session.

        Note: The position attribute may differ as Remote Players are
        only required to tell krum the position value every 5 seconds.
        """
        player = self._playback_set.get_player(self.request.path)
        data = player_view(player)
        data['playback'] = yield player.send_request('GET', '/playback')
        self.write(data)

class SessionListHandler(BaseHandler):
    '''
    This handles requests for querying or creating/re-enstating sessions.
    '''
    def initialize(self, state):
        self.playback_set = state

    def get(self):
        reverse = self.reverse_url
        self.write({'list': [x.to_json(reverse) for x in self.playback_set.sessions]})

    @gen.coroutine
    def post(self):
        """
        Creates new sessions. Trying to create a new
        with the same name as one that already exists is an error.
        """

        if not is_json(self.request.headers.get('Content-Type', '')):
            raise HTTPError(400, 'Client submitted non-json Content-Type')

        data = self.json_body
        try:
            name = data['name']
        except KeyError:
            raise HTTPError(400, 'New sessions must have a name')

        try:
            session = self.playback_set.create_session(name)
        except NotUniqueError:
            session = self.playback_set.session_for_name(name)

        try:
            status = yield session.apply_state_change(data)
            if status != 204:
                self.set_status(status)
            else:
                self.set_status(201)
            self.set_header(
                'Location',
                self.application.reverse_url('api.1.playback.session.item', session.id)
            )
            self.finish()
        except PlayerInUseError as e:
            self.set_status(409)
            self.write({'message': e.message})
            self.finish()

class SessionHandler(BaseHandler):
    '''
    This handles querying an manipulating an individual player session.
    '''
    def initialize(self, state):
        self.playback_set = state

    def get(self, id_):
        sess = self.playback_set.get_session(id_)

        self.write(sess.to_json(self.reverse_url))

    @gen.coroutine
    def patch(self, id_):
        if not is_json(self.request.headers.get('Content-Type', '')):
            raise HTTPError(400, 'Client submitted non-json Content-Type')

        session = self.playback_set.get_session(id_)
        status = yield session.apply_state_change(self.json_body)
        self.set_status(status)

class HistoryHandler(BaseHandler):
    '''
    This handler provides access to the play history of sessions.
    '''
    pass
