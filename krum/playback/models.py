import re
from django.db import models
import krum.metabase.models as kmb
from krum.content.models import Content
from krum.utilities.json import from_json, to_json

class RegisteredPlayer(models.Model):
    name        = models.CharField(max_length=50)
    guid        = models.CharField(max_length=32)
    active      = models.BooleanField(default=False)
    isvisible   = models.BooleanField(default=True)

class Session(models.Model):
    name        = models.CharField(max_length=50)
    player_path = models.URLField()
    _playlist   = models.TextField()
    paused      = models.BooleanField()
    position    = models.FloatField()
    volume      = models.FloatField()

    @property
    def playlist(self):
        return from_json(self._playlist)
    @playlist.setter
    def playlist(self, val):
        self._playlist = to_json(val)

class History(models.Model):
    session     = models.ForeignKey(Session)
    content_url = models.CharField(max_length=250)
    playdate    = models.DateTimeField(auto_now=True)
    metadata    = models.ForeignKey(kmb.MetaBase, null=True)
    max_playback_position = models.PositiveIntegerField(default=0)

    def find_metadata_id(self, content_path):
        '''
        Use the given path to figure out if this content came from us.

        If so, grab the current metadata_id and save it.
        '''
        m = re.search(content_path+'$', self.content_url)
        if m == None:
            print("Skipping, not our content")
            return

        content_id = m.group(1)

        try:
            c = Content.objects.get(pk=content_id)
        except Content.DoesNotExist:
            print("Skipping, content no longer here")
            return

        # figure out which metadata_id
        if c.metadata_id == None:
            print("Skipping, content not yet linked to metadata")
            return

        # add it
        self.metadata_id = c.metadata_id
