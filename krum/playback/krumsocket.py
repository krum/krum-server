"""Provides an implementation of Krum Sockets."""
import logging
import tornado.gen as gen
from tornado.concurrent import Future
from tornado.websocket import WebSocketHandler, websocket_connect

from .messagetypes import *
from krum.utilities.json import to_json

logger = logging.getLogger(__name__)

class KrumSocketDelegate(object):
    """
    Outlines the protocol for delegates of a KrumSocket.
    """

    def on_open(self, socket):
        '''
            This event means the websocket connection has been established.
            Registration is about to start.
        '''
        pass

    def on_event(self, socket, event):
        pass

    def on_request(self, socket, request):
        pass

    def on_close(self, socket):
        pass

class KrumSocketMixin(object):
    # TODO: add timeout capability to requests
    def __init__(self, *args, **kwargs):
        super(KrumSocketMixin, self).__init__(*args, **kwargs)

        self._nextID = 0
        self._response_futures = {}

    def delegate():
        doc = "The delegate property."
        def fget(self):
            return self._delegate
        def fset(self, value):
            self._delegate = value
        def fdel(self):
            del self._delegate
        return locals()
    delegate = property(**delegate())

    def _get_next_id(self):
        id_ = self._nextID
        self._nextID += 1
        return id_

    def send_request(self, method, path, body={}):
        """
        Sends a request to the other party and returns a Future that
        resolves with a tuple of (status, body) when it is received.
        """
        fut = Future()
        request = Request(method, self._get_next_id(), path, body)
        self._response_futures[request.id] = fut

        self.write_message(to_json(request))

        return fut

    def send_response(self, status, id_, body={}):
        response = Response(status, id_, body)
        self.write_message(to_json(response))

    def send_event(self, method, path, body={}):
        event = Event(method, path, body)
        self.write_message(to_json(event))

    def on_close(self):
        for fut in self._response_futures.values():
            fut.set_result(503, {})

        self._response_futures.clear()

        if self._delegate:
            logger.debug('Sending close event to delegate: {}'.format(self._delegate))
            self._delegate.on_close(self)

    def on_message(self, data):
        logger.debug('Message recieved: {}'.format(data))
        msg = deserialise_message(data)
        try:
            h = getattr(self, 'on_{}'.format(msg.type.lower()))
        except AttributeError:
            logger.warn('Got message with invalid type: {}'.format(msg.type))
            return

        # TODO: Put in place some more robust handling
        h(msg)

    def on_event(self, event):
        if self._delegate:
            self._delegate.on_event(self, event)

    def on_request(self, request):
        # TODO: Put in place some more robust handling
        if self._delegate:
            self._delegate.on_request(self, request)

    def on_response(self, response):
        id_ = response.id
        try:
            fut = self._response_futures.pop(id_)
        except KeyError:
            pass
        fut.set_result((response.status, response.body))

# convenience class
class KrumSocketHandler(KrumSocketMixin, WebSocketHandler):

    def initialize(self, delegate):
        self.delegate = delegate

    def open(self):
        if self.delegate:
            self.delegate.on_open(self)

    def close(self):
        '''
        This makes sure we don't bust up if closure means responses that we
        would normally close on.
        '''
        if self.ws_connection:
            self.ws_connection.close()

    def reverse_url(self, url_name, *args):
        """
        Returns a URL path for handler named `name`

        The handler must be added to the application as a named URLSpec.

        Args will be substituted for capturing groups in the URLSpec regex.
        They will be converted to strings if necessary, encoded as utf8,
        and url-escaped.
        """
        return self.application.reverse_url(url_name, *args)

@gen.coroutine
def krumsocket_connect(url, delegate, connect_timeout=None):
    # now create the tornado websocket client
    logger.debug('Starting Websocket connection to url: "{}"'.format(url))
    socket = yield websocket_connect(url, connect_timeout=connect_timeout)
    logger.debug('Websocket connection successful')
    krum_socket = KrumSocketClient(socket, delegate)

    # tell our friend
    if delegate:
        delegate.on_open(krum_socket)

    return krum_socket

class KrumSocketClient(KrumSocketMixin):
    def __init__(self, socket, delegate):
        super(KrumSocketClient, self).__init__()

        self._socket = socket
        self._reading = False
        self.delegate = delegate

    @gen.coroutine
    def read_until_closed(self):
        '''Loop waiting for messages until we close'''
        self._reading = True
        while self._reading:
            msg = yield self._socket.read_message()
            if msg is None:
                logger.debug('KrumSocketClient closed: "{}"'.format(msg))
                self.on_close()
            else:
                self.on_message(msg)


    def write_message(self, message, binary=False):
        self._socket.write_message(message, binary)

    def close(self):
        '''A bit messy because the Tornado guys didn't expose this properly'''
        self._socket.close()
