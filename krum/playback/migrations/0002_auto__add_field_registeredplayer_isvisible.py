# -*- coding: utf-8 -*-
import datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding field 'RegisteredPlayer.isvisible'
        db.add_column('playback_registeredplayer', 'isvisible',
                      self.gf('django.db.models.fields.BooleanField')(default=True),
                      keep_default=False)


    def backwards(self, orm):
        # Deleting field 'RegisteredPlayer.isvisible'
        db.delete_column('playback_registeredplayer', 'isvisible')


    models = {
        'playback.history': {
            'Meta': {'object_name': 'History'},
            'completion': ('django.db.models.fields.FloatField', [], {}),
            'content_url': ('django.db.models.fields.CharField', [], {'max_length': '250'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'playdate': ('django.db.models.fields.DateTimeField', [], {'auto_now': 'True', 'blank': 'True'}),
            'session': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['playback.Session']"})
        },
        'playback.registeredplayer': {
            'Meta': {'object_name': 'RegisteredPlayer'},
            'active': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'guid': ('django.db.models.fields.CharField', [], {'max_length': '32'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'isvisible': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '50'})
        },
        'playback.session': {
            'Meta': {'object_name': 'Session'},
            '_playlist': ('django.db.models.fields.TextField', [], {}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '50'}),
            'paused': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'player_url': ('django.db.models.fields.URLField', [], {'max_length': '200'}),
            'position': ('django.db.models.fields.PositiveIntegerField', [], {}),
            'volume': ('django.db.models.fields.FloatField', [], {})
        }
    }

    complete_apps = ['playback']