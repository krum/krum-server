# -*- coding: utf-8 -*-
from south.utils import datetime_utils as datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Change it. It's actually a path, not a URL... Embarassing!
        db.rename_column('playback_session', 'player_url', 'player_path')

    def backwards(self, orm):
        # Change it back to the old name
        db.rename_column('playback_session', 'player_path', 'player_url')

    models = {
        'metabase.genre': {
            'Meta': {'object_name': 'Genre'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '40'})
        },
        'metabase.genrelink': {
            'Meta': {'object_name': 'GenreLink', 'unique_together': "(('meta', 'genre'),)"},
            'genre': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['metabase.Genre']"}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'meta': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['metabase.MetaBase']"})
        },
        'metabase.mediatype': {
            'Meta': {'object_name': 'MediaType'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '30'}),
            'parent': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'children'", 'to': "orm['metabase.MediaType']", 'default': 'None', 'null': 'True'})
        },
        'metabase.metabase': {
            'Meta': {'object_name': 'MetaBase', 'unique_together': "(('source', 'name', 'year', 'media_type'),)"},
            'added_timestamp': ('django.db.models.fields.DateTimeField', [], {'blank': 'True', 'auto_now_add': 'True'}),
            'genres': ('django.db.models.fields.related.ManyToManyField', [], {'symmetrical': 'False', 'to': "orm['metabase.Genre']", 'through': "orm['metabase.GenreLink']"}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'in_library': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'media_type': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['metabase.MediaType']"}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '50'}),
            'other_id': ('django.db.models.fields.IntegerField', [], {}),
            'source': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['metabase.MetaSource']"}),
            'year': ('django.db.models.fields.CharField', [], {'max_length': '4'})
        },
        'metabase.metasource': {
            'Meta': {'object_name': 'MetaSource'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '30'})
        },
        'playback.history': {
            'Meta': {'object_name': 'History'},
            'content_url': ('django.db.models.fields.CharField', [], {'max_length': '250'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'max_playback_position': ('django.db.models.fields.PositiveIntegerField', [], {'default': '0'}),
            'metadata': ('django.db.models.fields.related.ForeignKey', [], {'null': 'True', 'to': "orm['metabase.MetaBase']"}),
            'playdate': ('django.db.models.fields.DateTimeField', [], {'auto_now': 'True', 'blank': 'True'}),
            'session': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['playback.Session']"})
        },
        'playback.registeredplayer': {
            'Meta': {'object_name': 'RegisteredPlayer'},
            'active': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'guid': ('django.db.models.fields.CharField', [], {'max_length': '32'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'isvisible': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '50'})
        },
        'playback.session': {
            'Meta': {'object_name': 'Session'},
            '_playlist': ('django.db.models.fields.TextField', [], {}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '50'}),
            'paused': ('django.db.models.fields.BooleanField', [], {}),
            'player_path': ('django.db.models.fields.URLField', [], {'max_length': '200'}),
            'position': ('django.db.models.fields.PositiveIntegerField', [], {}),
            'volume': ('django.db.models.fields.FloatField', [], {})
        }
    }

    complete_apps = ['playback']