# -*- coding: utf-8 -*-
import datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding model 'RegisteredPlayer'
        db.create_table('playback_registeredplayer', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=50)),
            ('guid', self.gf('django.db.models.fields.CharField')(max_length=32)),
            ('active', self.gf('django.db.models.fields.BooleanField')(default=False)),
        ))
        db.send_create_signal('playback', ['RegisteredPlayer'])

        # Adding model 'Session'
        db.create_table('playback_session', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=50)),
            ('player_url', self.gf('django.db.models.fields.URLField')(max_length=200)),
            ('_playlist', self.gf('django.db.models.fields.TextField')()),
            ('paused', self.gf('django.db.models.fields.BooleanField')(default=False)),
            ('position', self.gf('django.db.models.fields.PositiveIntegerField')()),
            ('volume', self.gf('django.db.models.fields.FloatField')()),
        ))
        db.send_create_signal('playback', ['Session'])

        # Adding model 'History'
        db.create_table('playback_history', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('session', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['playback.Session'])),
            ('content_url', self.gf('django.db.models.fields.CharField')(max_length=250)),
            ('playdate', self.gf('django.db.models.fields.DateTimeField')(auto_now=True, blank=True)),
            ('completion', self.gf('django.db.models.fields.FloatField')()),
        ))
        db.send_create_signal('playback', ['History'])


    def backwards(self, orm):
        # Deleting model 'RegisteredPlayer'
        db.delete_table('playback_registeredplayer')

        # Deleting model 'Session'
        db.delete_table('playback_session')

        # Deleting model 'History'
        db.delete_table('playback_history')


    models = {
        'playback.history': {
            'Meta': {'object_name': 'History'},
            'completion': ('django.db.models.fields.FloatField', [], {}),
            'content_url': ('django.db.models.fields.CharField', [], {'max_length': '250'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'playdate': ('django.db.models.fields.DateTimeField', [], {'auto_now': 'True', 'blank': 'True'}),
            'session': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['playback.Session']"})
        },
        'playback.registeredplayer': {
            'Meta': {'object_name': 'RegisteredPlayer'},
            'active': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'guid': ('django.db.models.fields.CharField', [], {'max_length': '32'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '50'})
        },
        'playback.session': {
            'Meta': {'object_name': 'Session'},
            '_playlist': ('django.db.models.fields.TextField', [], {}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '50'}),
            'paused': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'player_url': ('django.db.models.fields.URLField', [], {'max_length': '200'}),
            'position': ('django.db.models.fields.PositiveIntegerField', [], {}),
            'volume': ('django.db.models.fields.FloatField', [], {})
        }
    }

    complete_apps = ['playback']