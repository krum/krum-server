'''
This module provides helper classes for the messages that Krum uses to
comminicate over WebSockets.
'''

import logging
from krum.utilities.json import from_json
from krum.utilities.helpers import extract_attrs

logger = logging.getLogger(__name__)

def deserialise_message(bytes_):
    """Deserialise a Krum Protocol message."""
    msg = from_json(bytes_)
    if not isinstance(msg, dict):
        raise ValueError('Message must be a JSON object')

    try:
        t = msg['type']
    except KeyError:
        raise ValueError('Message missing type')

    if t == 'REQUEST':
        f = Request
    elif t == 'RESPONSE':
        f = Response
    elif t == 'EVENT':
        f = Event
    else:
        raise ValueError('The requested message type "{}" is not valid'.format(t))

    return f.from_json_dict(msg)

class Message(object):
    """Represents a single over-the-wire message."""
    type = None         # This is an abstract base, doesn't get sent.
    json_fields = []

    def to_json_dict(self):
        """Convert this message to a JSON encodable dict."""
        return extract_attrs(self, self.json_fields)

    @classmethod
    def from_json_dict(cls, msg):
        """Take a JSON encoded message and return an actual Message
        subclass instance."""

        assert msg['type'] == cls.type
        # filter down to desired fields, except 'type'
        attrs = dict([(k,v) for k,v in msg.items() if k != 'type' and k in cls.json_fields])
        return cls(**attrs)

class Request(Message):
    """A Request, same as HTTP mostly."""
    type = 'REQUEST'
    json_fields = ['type', 'method', 'id', 'path', 'body']

    def __init__(self, method, id, path, body=None):
        self.method = method
        self.id = id
        self.path = path
        self.body = body or {}

class Response(Message):
    """A response, again mostly the same as HTTP."""
    type = 'RESPONSE'
    json_fields = ['type', 'status', 'id', 'body']

    def __init__(self, status, id, body=None):
        self.status = status
        self.id = id
        self.body = body or {}

class Event(Message):
    """An unsolicited event!."""
    type = 'EVENT'
    json_fields = ['type', 'method', 'path', 'body']

    def __init__(self, method, path, body=None):
        self.method = method
        self.path = path
        self.body = body or {}
