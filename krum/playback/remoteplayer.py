"""Provides the RemotePlayer class which implements the server side of
the Remote Player protocol for krum. Also provides
RemotePlayerDelegate, which defines the necessary methods for
delegates of the protocol.
"""
import uuid
import logging
from tornado import gen

import krum.playback.models as m

logger = logging.getLogger(__name__)

KRUM_PLAYER_API_VERSION = 1

class InvalidResponseError(Exception):
    """A Remote Player has replied with an invalid response."""

class CannotAddPlayerError(Exception):
    """Generic exception for when a Remote Player connection/handshake/etc
    fails."""

class RemotePlayerDelegate(object):
    '''
        This delegate class outlines the protocol for a
        something to be the delegate of a RemotePlayerSocket.

        Initial sequence is:
            on_start_register
            on_finish_register

        If anything fails during registration, e.g. wrong protocol version, etc,
        on_close is will be triggered.

        on_close is also called if the socket closes for any other reason, e.g.
        the remote player crashed or was closed, etc.

        on_event is called whenever the remote player sends through an event,
        e.g. playback progress, a playlist change (because the item is complete).
    '''

    def on_start_register(self, player):
        '''
            This event means that the Remote Player has responded to a /meta
            request and is about to be sent a PATCH.
        '''
        pass

    def on_finish_register(self, player, path, name):
        '''
            This event is sent when krumsocket has successfully completed all
            registration steps except for the final PATCH request that informs
            the player of it's PATH and name.
        '''
        pass

    def on_event(self, player, event):
        pass

    def on_close(self, player):
        pass

class RemotePlayer(object):
    def __init__(self, player_socket, delegate=None):
        '''
            This class provides registration and synchronisation for a Krum remote player.

            :param player_socket: A KrumSocket newly connected to by a client.
        '''
        # set ourselves as the delegate for the socket
        self.socket = player_socket
        self.socket.delegate = self

        self.delegate = delegate

        self._path = None
        self._model = None

    def model_prop(name):
        def fetch(self):
            if self._model:
                return getattr(self._model, name)
            else:
                return None
        doc = 'Returns {} attribute of the remote player or None if that value hasn\'t been confirmed yet.'

        return property(fget=fetch)

    # READ-ONLY properties
    @property
    def path(self):
        return self._path
    id = model_prop('id')
    name = model_prop('name')
    guid =  model_prop('guid')
    isvisible =  model_prop('isvisible')

    def delegate():
        doc = "The delegate property."
        def fget(self):
            return self._delegate
        def fset(self, value):
            self._delegate = value
        def fdel(self):
            del self._delegate
        return locals()
    delegate = property(**delegate())

    @gen.coroutine
    def start_registration(self):
        '''This kicks off the registration process.'''
        logger.info('Starting Remote Player registration process')

        if self.delegate:
            self.delegate.on_start_register(self)

        # get the meta-data for this Remote Player
        status, meta = yield self.socket.send_request('GET', '/meta')
        if status != 200:
            self.socket.close()
            raise InvalidResponseError(
                'Initial GET /meta got non-200 response "{}"'.format(status))

        # Set up the player model (persists the GUID, id and name)
        try:
            self._init_player_model(meta)
        except CannotAddPlayerError as e:
            logger.error('Cannot add player. Reason: "{}", dropping connection.'.format(e))
            self.socket.close()
            raise

        # This is the patch with its registered path, signals to the
        # Remote Player that registration is complete.
        patch_body = {
            'registered_path': self.path,
            'registered_name': self.name
        }

        # check it's version, tell it to change if we need
        version = meta.get('version')
        if version != KRUM_PLAYER_API_VERSION:
            logger.info('Got remote player with API version, got {}, need {}'.format(
                        version, KRUM_PLAYER_API_VERSION))

            # it doesn't support our version
            if KRUM_PLAYER_API_VERSION not in meta.get('supported_versions', []):
                self.socket.close()
                msg = ("Remote player doesn't support required API version {}, only {}, dropping connection"
                       .format(KRUM_PLAYER_API_VERSION, meta.get('supported_versions', [])))
                raise CannotAddPlayerError(msg)

            # tell it to switch to our version
            patch_body['version'] = KRUM_PLAYER_API_VERSION

        # this must be done
        if self.delegate:
            self.delegate.on_finish_register(self, self.path, self.name)
        status, reason = yield self.socket.send_request(
            'PATCH', '/meta', patch_body)

        if status != 204:
            self.socket.close()
            msg = 'Remote player, non-204 status on response to final /meta PATCH request, reason "{}". Dropping connection.'.format(reason)
            raise InvalidResponseError(msg)

    def _build_path(self, id):
        """
        Take a Player ID and generate the PATH for the player.
        """
        return self.socket.reverse_url('api.1.playback.player.item', self._model.id)

    def _init_player_model(self, meta):
        """
        Takes the GUID and fetches, or creates, the model to save the GUID + name combo.
        Also saves the visibility status.
        """
        logger.debug('Begin initialising the player model')
        # we need a guid
        try:
            guid = meta['guid']
        except KeyError:
            raise CannotAddPlayerError('player metadata missing guid')

        # normalise the guid
        guid = uuid.UUID(guid).hex
        isvisible = meta.get('isvisible', False) != False

        # make sure the name is unique, and save it
        self._model = self._get_model(guid)
        self._model.name = self._gen_unique_name(self._model.name or meta.get('client_type'))
        self._model.active = True
        self._model.isvisible = isvisible
        self._model.save()

        self._path = self._build_path(self._model.pk)
        logger.debug('Sucessfully initialised the player model')

    def _gen_unique_name(self, name):
        '''
            Returns a uniquified version of the given name.
            It will be unique amonst all active registered players.
        '''
        used_names = set(m.RegisteredPlayer.objects.exclude(pk=self._model.pk, active=False)
                         .values_list('name', flat=True))
        name = name or 'Unknown Player'
        new_name = name
        i = 1
        while new_name in used_names:
            new_name = '{}-{}'.format(name, i)
            i += 1

        return new_name

    def _get_model(self, guid):
        '''Either create a new model with the GUID or get the existing one.'''
        try:
            return m.RegisteredPlayer.objects.get(guid=guid)
        except m.RegisteredPlayer.DoesNotExist:
            return m.RegisteredPlayer(guid=guid)

    def on_request(self, socket, request):
        pass

    def on_event(self, socket, event):
        if self.delegate:
            self.delegate.on_event(self, event)

    def on_close(self, socket):
        logger.info('Remote player closed')

        if self._model:
            self._model.active = False
            self._model.save()

        if self.delegate:
            self.delegate.on_close(self)
