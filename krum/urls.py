from django.conf.urls import patterns, include, url
from krum import cfg
from django.contrib.staticfiles.urls import staticfiles_urlpatterns
from django.shortcuts import redirect

# Uncomment the next two lines to enable the admin:
# from django.contrib import admin
# admin.autodiscover()

urlpatterns = patterns('',
    url(r'^api/',include('krum.api.urls')),
    url(r'',include('krum.webui.urls')),
)

urlpatterns += staticfiles_urlpatterns()

if cfg['include_jasmine']:
    def generate_assets(request):
        from krum.utilities.helpers import json_response
        from django.core import management
        management.call_command('assets','build')
        return redirect('/jasmine')

    urlpatterns += patterns('',
        url(r'jasmine_pre_gen', generate_assets),
        url(r'jasmine', include('django_jasmine.urls'))
    )