"""Defines views and handlers for media resources."""
import logging
from collections import defaultdict
import krum.metabase.models as mbm
import krum.content.models as cm
from django.db.models import Max
from krum.playback.models import History
from krum.utilities.helpers import TimeAndLog

from .baseresource import BaseResource

logger = logging.getLogger(__name__)

# Helpers for the data fetching code
MAX_SQLITE_PARAMS = 999
METABASE_FIELDS = ('id', 'name', 'year', 'media_type_id',
                   'added_timestamp', 'content__added_timestamp')
MOVIE_FIELDS = METABASE_FIELDS
SERIES_FIELDS = METABASE_FIELDS
EPISODE_FIELDS = tuple(list(METABASE_FIELDS) + ['season', 'ep_num', 'ep_name'])

# Simple helper to build a referencable path
def view_metabase_ref(reverse_url, id_):
    return reverse_url('api.1.media.item', id_)

# These work on pre-build dicts from MediaResource.root_qs_to_json
def view_metabase(reverse_url, genre_map, content_map, last_watched, obj):
    """Update MetaBase model dict for /media API."""
    obj.update({
        'path': view_metabase_ref(reverse_url, obj['id']),
        'content': content_map[obj['id']],
        'genres': genre_map[obj['id']],
        'lastwatched': last_watched.get(obj['id'])
    })
    return obj

# Stock standard
view_movie = view_metabase

def view_series(reverse_url, genre_map, content_map, last_watched, episode_map, obj):
    """Update Series model dict for /media API."""
    obj = view_metabase(reverse_url, genre_map, content_map, last_watched, obj)
    obj['episode_paths'] = episode_map[obj['id']]
    return obj

# Has extra fields, but those come straight from Django ORM
view_episode = view_metabase

def view_genre(reverse_url, id_, name):
    """Update Genre model dict for /media API JSON encoding."""

    return {
        'id': id_,
        'path': reverse_url('api.1.genre.item', id_),
        'name': name
    }

def view_content_ref(reverse_url, id_):
    """Return Content path for /media API JSON encoding"""
    return reverse_url('api.1.content.item', id_)

def view_media_type(reverse_url, obj):
    '''Builds the JSON dict for a metabase.MediaType model.'''
    return {
        'id': obj.pk,
        'path': reverse_url('api.1.media_type.item', obj.pk),
        'name': obj.name,
        'parent_id': obj.parent_id,
        'child_ids': list(obj.children.values_list('id', flat=True))
    }

def byslice(l, s):
    """
    Simple generator for splitting up a list (l) into s sized slices for
    batching.
    """
    ll = len(l)
    for x in range(0, ll, s):
        yield l[x:min(ll, x+s)]

class MediaResource(BaseResource):
    """
    Defines necessary methods to work with the Collection and Instance
    RequestHandlers for simple model driven resources.
    """

    def getFilterAttrs(self):
        """Returns the attributes (and their model fields) that /media
        allows filtering and ordering on.
        """
        return {
            'id': 'id',
            'name': 'name',
            'year': 'year',
            'added_timestamp': 'added_timestamp',
            'content_added_timestamp': 'content__added_timestamp',
            'media_type': 'media_type_id'
        }

    def getRootQS(self, request):
        """We want only those MetaBase items which have been added to
        the library.
        """

        return mbm.MetaBase.objects.filter(in_library=1)

    def root_qs_to_json_dicts(self, reverse_url, qs):
        """Me need to be quite custom here to keep performance good.
        Django and SQLite can't do this on their own."""

        # get the sorted, final list of MetaBase IDs
        with TimeAndLog(logger, 'ID query took: {}s'):
            qs = qs.values_list('id', flat=True)
            logger.info('ID Query: %s', qs.query.sql_with_params())
            ids = list(qs)

        # build genres map
        genreMap = defaultdict(list)
        with TimeAndLog(logger, 'Genre map build took: {}s'):
            for idslice in byslice(ids, MAX_SQLITE_PARAMS):
                for x in (mbm.GenreLink.objects.filter(meta_id__in=idslice)
                          .values_list('meta_id', 'genre__id', 'genre__name')):
                    genreMap[x[0]].append(view_genre(reverse_url, *x[1:]))

        # build a content map
        contentMap = defaultdict(list)
        with TimeAndLog(logger, 'Content map build took: {}s'):
            for idslice in byslice(ids, MAX_SQLITE_PARAMS):
                for x in (cm.Content.objects.filter(metadata_id__in=idslice)
                          .values_list('metadata_id', 'id')):
                    contentMap[x[0]].append(view_content_ref(reverse_url, x[1]))

        # build episode map
        episodeMap = defaultdict(list)
        with TimeAndLog(logger, 'episode map build took: {}s'):
            for idslice in byslice(ids, MAX_SQLITE_PARAMS):
                for x in (mbm.Episode.objects.filter(series_id__in=idslice)
                          .values_list('series_id', 'id')):
                    episodeMap[x[0]].append(view_metabase_ref(reverse_url, x[1]))

        # build last-watched map
        lastWatched = {}
        with TimeAndLog(logger, 'watched map build took: {}s'):
            for idslice in byslice(ids, MAX_SQLITE_PARAMS):
                for x in (History.objects.filter(metadata_id__in=idslice)
                          .values_list('metadata_id').annotate(last_played=Max('playdate'))):
                    lastWatched[x[0]] = x[1]

        results = []
        for idslice in byslice(ids, MAX_SQLITE_PARAMS):
            # add movies
            results.extend([view_movie(reverse_url, genreMap, contentMap, lastWatched, x) for x in
                            mbm.Movie.objects.filter(pk__in=idslice).values(*MOVIE_FIELDS)])
            # add series
            results.extend([view_series(reverse_url, genreMap, contentMap, lastWatched, episodeMap, x) for x in
                            mbm.Series.objects.filter(pk__in=idslice).values(*SERIES_FIELDS)])
            # add episodes
            results.extend([view_episode(reverse_url, genreMap, contentMap, lastWatched, x) for x in
                            mbm.Episode.objects.filter(pk__in=idslice).values(*EPISODE_FIELDS)])

        # now sort the actual results
        sort_order = dict((x,i) for i,x in enumerate(ids))
        results.sort(key=lambda x: sort_order[x['id']])

        return results

class GenreResource(BaseResource):
    """A text tag for a piece of media."""

    def getFilterAttrs(self):
        """Allow some filtering."""
        return {
            'id': 'id',
            'name': 'name'
        }

    def getRootQS(self, request):
        return mbm.Genre.objects.all()

    def model_to_json_dict(self, reverse_url, obj):
        return view_genre(reverse_url, obj.id, obj.name)

class MediaTypeResource(BaseResource):
    """The different types of media, Movies, Series, etc."""

    def getFilterAttrs(self):
        """Allow some filtering."""
        return {
            'id': 'id',
            'name': 'name'
        }

    def getRootQS(self, request):
        return mbm.MediaType.objects.all()

    def model_to_json_dict(self, reverse_url, obj):
        return view_media_type(reverse_url, obj)
