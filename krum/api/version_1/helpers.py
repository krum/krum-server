
from sys import maxsize

def apply_limit_and_offset(handler, queryset):
    '''Returns a new django QuerySet after filtering ``queryset`` based on the limit and offset
    arguments from the GET parameters of ``handler``.
    ``handler`` must be a tornado.web.Requesthandler.
    ``queryset`` must be a django.db.models.query.QuerySet.

    Both offset and limit checked to make sure they're positive integers.

    ``offset`` is used to skip over the first ``offset`` results, defaults to 0.
    ``limit`` is used to restrict the number of returned results and defaults to ``None``, meaning
    all results will be returned beyond the first ``offset``.'''

    try:
        offset = int(handler.get_argument('offset', 0))
        limit = handler.get_argument('limit', None)
        # django does slicing, not limit = number of results so convert
        if limit != None:
            limit = offset + int(limit)

    except ValueError:
        raise ValueError('Both limit and offset arguments must be base 10 integers.')

    if (limit != None and (limit < 0 or limit >= maxsize)) or offset < 0 or offset >= maxsize:
        raise ValueError('Both offset and limit must be >= 0, <= {} or not present.'.format(maxsize))

    return queryset[offset:limit]

def update_model(m, data, whitelist):
    '''This updates model ``m``'s attributes from the dictionary ``data``. Only
    those attribute names present in whitelist will be updated.

    The newly updated model is returned.'''

    for k,v in data.items():
        if k not in whitelist:
            continue

        setattr(m, k, v)

    return m