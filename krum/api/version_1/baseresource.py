"""Defines the BaseResource class for simple CRUD style resources."""

# TODO: TEMP
import logging

logger = logging.getLogger(__name__)

class BaseResource(object):
    """
    Defines necessary methods to work with the Collection and Instance
    RequestHandlers for simple model driven resources.
    """

    def getFilterAttrs(self):
        """Returns a dict of (Resource attribute names)->(model fields).

        The model fields are relative to the root model and used to
        both validate and apply filters and ordering.

        By default, no fields are available for filtering or ordering.
        I.e. the default implementation is "return {}"
        """
        return {}

    def getRootQS(self, request):
        """
        Should be overridden and return a Django QuerySet that can be
        used to fetch/delete/etc instances of the root model for this
        resource.

        Each QuerySet will be used:
            Collection GET: To filter/order/etc the result set and
                before JSON encoding and sending them to the client.
                When multiple QuerySets are returned, limit and offset
                are applied to each one.
        """
        raise NotImplementedError

    def root_qs_to_json_dicts(self, reverse_url, qs):
        """Last opportunity to affect and tune the query set before
        JSON encoding.

        This is called with the query set, returned by getRootQS,
        after filtering, ordering and offset:limit have been applied.

        This method is responsible for taking the query set and
        returning a sequence of JSON encodable objects that represent
        instances of this resource.

        The default implementation simply returns
        [self.model_to_json_dict(reverse_url, x) for x in qs].
        Allowing simple resources to be implemented by overriding
        getRootQS and model_to_json_dict.
        """
        return [self.model_to_json_dict(reverse_url, x) for x in qs]

    def model_to_json_dict(self, reverse_url, obj):
        """Take an instance of the root model and build a JSON
        encodable dict from it.

        reverse_url is provided to allow consistent path building.
        """
        raise NotImplementedError
