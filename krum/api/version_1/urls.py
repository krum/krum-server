from django.conf.urls import patterns, include, url
urlpatterns = patterns('krum.api.version_1.views',
    # The following allows the UI to get the metadata about content in the system.
    # Metadata will also contain the content_id of the content for use later
    #
    #url(r'^api/VERSION/search?q=QUERY&page=PAGE&per_page=PER_PAGE') #GET, SEARCH ALL CONTENT IN THE DB, paginated
    #url(r'^api/VERSION/movies?page=PAGE&per_page=PER_PAGE') #GET ALL MOVIES IN THE DB, paginated
    url(r'^media$',                     'tornado_stub'),
    url(r'^media/(?P<id>\d+)$',         'tornado_stub', name='api.1.library.item'),
    url(r'^genres$',                    'tornado_stub'),
    url(r'^genres/(?P<id>\d+)$',        'tornado_stub', name='api.1.genre.item'),
    url(r'^media_types$',               'tornado_stub'),
    url(r'^media_types/(?P<id>\d+)$',   'tornado_stub', name='api.1.media_type.item'),
    url(r'^content/(?P<id>\d+)',        'tornado_stub', name='api.1.content.item'),
    url(r'^content/(?P<id>\d+)/data',   'tornado_stub', name='api.1.content.data'),

    url(r'^autocomplete',               'tornado_stub'),
    #url(r'^api/VERSION/series?page=PAGE&per_page=PER_PAGE') #GET ALL SERIES IN THE DB, paginated
    #url(r'^api/VERSION/movies/XXXX') #GET, gets all info for a particular movie
    #url(r'^api/VERSION/series/XXXX/seasons') #GET, gets all season information for a particular series
    #url(r'^api/VERSION/series/XXXX/seasons/YYY/episodes') #GET, gets all episode information for a particular season
    #url(r'^api/VERSION/series/XXXX/seasons/YYY/episodes/ZZZ') #GET, gets all information for a particular episode

    ###
    # Ability to do something with the content
    #
    #/api/VERSION/content/QQQQQQ/watch #GET, sets up a temporary simlink in the directory managed by nginx and redirects to that.. this is to allow security mechanisms
    #url(r'^content/(P<content_id>[^/]+)$') #GET alias of one of /movies/XXX or /series/XXX/seasons/YYY/episodes/ZZZ
    #/api/VERSION/content/QQQQQ #POST updates metadata fields of the content. NOTE: only updates those fields that were mentioned in the POST, all others stay the same
    #/api/VERSION/content/QQQQQ #DELETE deletes the content form the system perminently

    ###
    # Ability to put content into the system
    #
    #/api/VERSION/content #POST upload content to the server, puts it into the auto-identification Queue
    #/api/VERSION/content/unidentified?page=PAGE&per_page=PER_PAGE #GET paginated list of unidentified content in the system

    ###
    # In order to manually match the content, we will need to be able to search the IMDB data
    # NOTE: Not yet sure the format this should take.. not yet a priority but soon will be
    #/api/VERSION/metadataLLLLLLLLLLLL
)
