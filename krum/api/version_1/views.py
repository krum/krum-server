from collections import defaultdict
from django.http import Http404, HttpResponseBadRequest, HttpResponseServerError
from django.shortcuts import get_object_or_404
from django.core.urlresolvers import reverse
from krum.metabase import models as mbm
from django.db.models import Q
import django.db as db

def tornado_stub(request):
    '''This is to allow mapping the URLs in django for reverse and template usage'''
    return HttpResponseServerError('This method should never be called. Tornado should have hijacked the request rather than pass on to django.')

def autocomplete_list(request):
    '''Allows fast retrieval of media meta data for the purpose of searches/etc.

    GET
        Params:
            - term : String : The search term. This is used with a SQL lite FTS match filter. So all semantics for that are supported.
            - media_type : [int] : One or more media type IDs to filter the results by.
            - limit : int : If present, the number of results will be limited to 'limit' results. This along with 'offset' can be used for pagination.
            - offset : int : If present, the first 'offset' results are not included.
    '''

    if 'term' not in request.GET:
        return HttpResponseBadRequest('Query string argument "term" must be provided')

    # setup starting point
    sql_base = '''select id, name, year, media_type_id
    from metabase_fts as s
        join metabase_metabase as m
        on m.id = s.docid
    where {}'''

    term = request.GET['term'] + '*'
    where_parts = ['content match %s']
    params = [term]

    try:
        limit = int(request.GET.get('limit', 0))
        offset = int(request.GET.get('offset', 0))
    except ValueError:
        return HttpResponseBadRequest('Both limit and offset arguments must be base 10 integers.')

    if limit != 0:
        sql_base += ' limit {}'.format(limit)
    if offset != 0:
        sql_base += ' offset {}'.format(offset)

    # media type filter, if present
    try:
        media_types = [int(x) for x in request.GET.getlist('media_type')]
    except ValueError:
        return HttpResponseBadRequest('media_type values must be base 10 integers.')

    if media_types:
        where_parts.append("media_type_id in ({})".format(','.join(['%s']*len(media_types))))
        params.extend(media_types)

    # TODO: add the ability to filter by genre(s)

    # do the query and hand back some DAATAA!
    where = ' and '.join(where_parts)
    sql = sql_base.format(where)
    cursor = db.connection.cursor()
    cursor.execute(sql, params)
    results = defaultdict(list)
    for r in cursor:
        results[r[3]].append({'id': r[0], 'name': r[1], 'year': r[2]})

    return json_response(results)

#### Templates ####
