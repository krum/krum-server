"""Defines handlers for the Content resource."""

import os.path as op
import logging

from django.core.files.base import ContentFile

from krum.content import models as cm
from krum.utilities.basehandler import BaseHandler, HTTPError
from .helpers import apply_limit_and_offset, update_model

logger = logging.getLogger(__name__)

WRITABLE_CONTENT_FIELDS = ['metadata_id']

def view_content(url_resolver, obj):
    '''Takes a content model ``obj`` and returns a dictionary ready for JSON
    encoding.'''

    data = {
        'id': obj.pk,
        'path': url_resolver.reverse_url('api.1.content.item', obj.pk),
        'data_url': url_resolver.reverse_url('api.1.content.data', obj.pk),
        'mimetype': obj.mimetype,
        'original_hash': obj.original_hash,
        'original_path': obj.original_path,
        'added_timestamp': obj.added_timestamp,
        'metadata_id': obj.metadata_id,
    }
    if obj.metadata_id is not None:
        data['metadata_url'] = url_resolver.reverse_url('api.1.media.item', obj.metadata_id)

    return data

class ContentListHandler(BaseHandler):
    '''This handler provides access to Content metadata in bulk.'''

    def get(self):
        '''This returns a list of content metadata.

        The list can be filtered by providing query string arguments:
            ``media_type`` : int[] : Multiple can be provided for an "or" filter. This
            infers hasmetadata=True as Content derives it's media_type from the media
            metadata it is linked to.

            ``hasmetadata`` : boolean : If present, a value of 'false' will result only
            in content that isn't linked to a piece of media metadata being returned.
            Other non-empty values will mean only linked content will be returned. To
            avoid filtering at all, don't provide the option.

            ``limit`` and ``offset`` : positive integers : Offset skips then limit will
            cap the result count.
        '''
        content = cm.Content.objects.all()

        # media type filter
        try:
            media_types = [int(x) for x in self.get_arguments('media_type')]
        except ValueError:
            raise HTTPError(400, 'media_type values must be base 10 integers.')

        # filter: media type and "is/isn't linked to metadata"
        if media_types:
            content = content.filter(metadata__media_type__in=media_types)
        else:
            metadata_id = self.get_argument('metadata_id', None)
            if metadata_id == None:
                pass
            elif metadata_id in ('null', ''):
                content = content.filter(metadata=None)
            else:
                metadata_ids = self.get_arguments('metadata_id')
                content = content.filter(metadata__in=metadata_ids)

        try:
            content = apply_limit_and_offset(self, content)
        except ValueError as e:
            raise HTTPError(400, str(e))

        # TODO: make async as this is where the sql hits!
        self.write({'list': [view_content(self, x) for x in content]})

    def put(self):
        '''This allows bulk update, but not creation, of content metadata.

        At present, only the medatadata_id attribute can be updated all
        others are read-only. And derived from the content file itself. An
        ``id`` attribute must also be provided for each object.

        A wrapped JSON list is returned with a key ``list`` whose value is
        a list of (index, status code, error message) lists for any failed
        updates.

        Each ``index`` is the index in the originally submitted list.
        Each ``status code`` is the equivalent status code that would be
        returned for this failure if the item was submitted to it's own url.
        Each ``error message`` is a textual description of the reason for the
        failure.
        '''

        records = self.json_body

        # update records in set
        content_objs = cm.Content.objects.in_bulk([r['id'] for r in records])
        failed = []
        for i,r in enumerate(records):
            try:
                pk = r['id']
            except KeyError as e:
                failed.append((i, 400, 'Content object missing ID, {}'.format(r)))
                continue

            try:
                c = content_objs[pk]
                update_model(c, r, WRITABLE_CONTENT_FIELDS).save()
            except KeyError as e:
                failed.append((i, 404, 'No Content object with ID {}'.format(pk)))
            except Exception as e:
                failed.append((i, 500, str(e)))

        self.write({'list': failed})

    def post(self):
        '''This allows the creation of content metadata resources. Just post
        a JSON object with the following attributes:

            ``original_path`` : ``string`` mandatory : Typically the original file path, or
            the title if only that is available, as is the case with HTML5 drag and drop.
            ``mimetype`` : ``string`` optional : This should be a valid
            `Internet media type <https://en.wikipedia.org/wiki/Internet_media_type>`
            ``metadata_id`` : ``int`` optional : This should be the id of a /media resource
            that has the relevant information about this content.

            Once this base metadata is created, the file data itself can be uploaded. This
            is done by POSTing to the ``data_url`` that is specified in the resource at the
            url returned in the Location header of this response.
        '''
        record = self.json_body
        # TODO: enforce writable vs implicit fields
        obj = cm.Content(**record)
        # create a 0 length file named after the original path
        obj.the_file.save(op.basename(obj.original_path), ContentFile(''))

        self.set_status(201)
        self.set_header('Location', self.reverse_url('api.1.content.item', obj.pk))

class ContentHandler(BaseHandler):
    '''This handler provides access to retrieve an manipulate individual content resources.'''

    def get_model(self, id_):
        print ("GETTTING MODEL WITH ID: {}".format(id_))
        try:
            return cm.Content.objects.get(pk=id_)
        except cm.Content.DoesNotExist:
            raise HTTPError(404)

    def get(self, id_):
        '''This returns a single content metadata resource.'''
        self.write(view_content(self, self.get_model(id_)))

    def patch(self, id_):
        '''This updates the content metadata resource.

        At present only the ``metadata_id`` attribute is writable.
        '''
        c = self.get_model(id_)
        new_obj = self.json_body
        update_model(c, new_obj, WRITABLE_CONTENT_FIELDS).save()
        self.set_status(204)

    def delete(self, id_):
        '''This deletes the content metadata resource.'''
        self.get_model(id_).delete()

        self.set_status(204)
