"""Defines a handler that allows autocomplete style lookups of
metabase-data."""

import struct
import django.db as db
from django.db.backends.signals import connection_created
from django.dispatch import receiver

from krum.utilities.basehandler import BaseHandler, HTTPError

MATCHINFO_PARAM = "'pl'"
QUERY_TERMS_OFFSET = 0
MATCHED_TERMS_OFFSET = 1

def rank(matchbytes):
    '''Used to rank autocomplete search results'''
    matchinfo = [x[0] for x in struct.iter_unpack('=I', matchbytes)]

    # calculate a rank score
    qts = matchinfo[QUERY_TERMS_OFFSET]
    mts = matchinfo[MATCHED_TERMS_OFFSET]

    return float(qts)/mts

@receiver(connection_created)
def add_rank_function(connection=None, **kwargs):
    '''Make sure our SQLite connection has the rank function.'''
    connection.connection.create_function('rank', 1, rank)

class AutocompleteHandler(BaseHandler):
    """Provides autocomplete/search oriented access to metabase info."""
    def get(self):
        '''Allows fast retrieval of media meta data for the purpose of searches/etc.

        GET
            Params:
                - term : String : The search term. This is used with a
                    SQL lite FTS match filter. So all semantics for
                    that are supported. It must be at least 3 chars
                    long.
                - media_type : [int] : One or more media type IDs to
                    filter the results by.
                - limit : int : If present, the number of results will
                    be limited to 'limit' results. This along with
                    'offset' can be used for pagination.
                - offset : int : If present, the first 'offset'
                    results are not included.
        '''

        # NOTE: To stop SQLite joining every row in both tables, I had
        #  to split up the text filter from the metabase gear. Hence
        #  the "where id in (select docid) " crud instead of a join.

        # setup starting point
        sql_base = '''
            SELECT
                m.id, m.name, m.year, m.media_type_id
            FROM
                metabase_metabase as m
                JOIN metabase_fts as mfts
                    ON m.id = mfts.docid
            WHERE {}
            ORDER BY rank(matchinfo(metabase_fts, {})) DESC
        '''

        term = self.get_argument('term') # raises MissingArgumentError if missing.
        where_parts = ['content MATCH %s']
        params = [term]

        try:
            limit = int(self.get_argument('limit', 0))
            offset = int(self.get_argument('offset', 0))
        except ValueError:
            raise HTTPError(400, 'Both limit and offset arguments must be base 10 integers.')

        if limit != 0:
            sql_base += ' limit {}'.format(limit)
        if offset != 0:
            sql_base += ' offset {}'.format(offset)

        # media type filter, if present
        try:
            media_types = [int(x) for x in self.get_arguments('media_type')]
        except ValueError:
            raise HTTPError(400, 'media_type values must be base 10 integers.')

        if media_types:
            where_parts.append("media_type_id in ({})".format(','.join(['%s']*len(media_types))))
            params.extend(media_types)

        # do the query and hand back some DAATAA!
        where = ' and '.join(where_parts)
        sql = sql_base.format(where, MATCHINFO_PARAM)

        # need to add the function via the real, sqlite3.connection object
        conn = db.connection

        cursor = conn.cursor()
        try:
            cursor.execute(sql, params)
            results = [{
                'id': r[0],
                'name': r[1],
                'year': r[2],
                'media_type': r[3],
                'url': self.reverse_url('api.1.media.item', r[0])
            } for r in cursor. fetchall()]
        finally:
            cursor.close()

        self.write({'list': results})
