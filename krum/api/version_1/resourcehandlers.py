"""Defines standard Tornado RequestHandlers for resources."""
import logging
from tornado import web
from krum.utilities.json import to_json, JSON_MIMETYPE

logger = logging.getLogger(__name__)

CORS_RESP_METHODS = 'GET,PUT,POST,PATCH,DELETE,OPTIONS'
CORS_RESP_HEADERS = 'Content-Range, Content-Type'
CORS_EXPS_HEADERS = 'Content-Range, Location'

def positiveIntArg(args, name):
    """
    Remove the named arg and parse it as a positive integer.

    Raises HTTPError(400, ...) if the number is invalid.
    """
    try:
        val = args.pop(name)[-1]
    except KeyError:
        return args, None

    try:
        val = int(val, base=10)
        if val < 0:
            raise ValueError()
    except ValueError:
        raise web.HTTPError(400, '{} must be a positive integer'.format(name))

    return args, val

def parseOrdering(args, name, attrMap):
    """Turns a GET ord list into a valid Django order_by argument list."""
    try:
        ordlist = args.pop(name)[-1]
    except KeyError:
        return args, None

    # break up and remove empties
    ordlist = [y for y in [x.strip() for x in ordlist.split(',')] if y != '']
    if len(ordlist) == 0:
        return args, None

    # check that each refers to a valid attribute
    fields = []
    try:
        for a in ordlist:
            prefix = ''
            if a[0] == '-':
                a = a[1:]
                prefix = '-'
            fields.append(prefix + attrMap[a])
    except KeyError as e:
        raise web.HTTPError(400, 'Unknown field {} requested in ordering'
                                    .format(e))

    return args, fields

def buildFilterOp(inverse, field, vals):
    """Build a Django filter/exclude op value pair from query string.

    Note: This exists to optimise produced SQL by replacing exclude(col=null)
    with filter(col__isnull=True) so that related model existence
    checks will result in INNER JOIN behaviour (despite Django using
    LEFT OUTER JOIN).

    Returns (inverse, op, vals) where
        inverse - True the op should go in a exclude instead of filter
        op - The __op suffix to use
        vals - The value that the should be tested with
    """

    # we're testing existence and field is foreign key or related
    if vals == [''] and (field.endswith('_id') or field.find('__') != -1):
        return (False, field + '__isnull', not inverse)
    else:
        return (inverse, field + '__in', vals)

def parseFiltersAndOrdering(args, attrMap):
    """
    Parses a dict of {key->[val,val]} for filtering collections.

    There are 3 standard keys "limit", "offset" and "ord". Both
    limit and offset must be base-10 positive integers. Ord is
    parsed as a comma separated list of attrbute names which is
    checked against the supplied attrbuteMap. The attribute names
    in ord may be prefixed by a '-' character to indicate
    descending ordering for that attribute. For these 3 filters,
    only the last val is considered.

    If any of these three cannot be parsed correctly, a HTTPError
    with status code 400 is raised.

    The remaining keys and values are parsed as filters, which
    means key names can be prefixed with a '!' character to
    indicate the filtering should be inverted (i.e. exclude not
    include). For those keys that don't match an attribute name,
    both the key and value are collected in remainders and
    returned. The filters and excludes are returned, ready to be
    handed to django's QuerySet .filter and .exlude methods.

    Raises:
        HTTPError(400, ...) if limit or offset aren't positive
        integers or the value provided for ord includes unkown
        attributes.

    Returns:
        (remainder, filters, excludes, limit, offset, ord)
    """

    # validate the filters
    args, limit = positiveIntArg(args, 'limit')
    args, offset = positiveIntArg(args, 'offset')
    args, ordering = parseOrdering(args, 'ord', attrMap)

    # pull out attribute filters
    filters = {}
    excludes = {}
    remainder = {}
    for attr, vals in args.items():
        # sort out the filters and the excludes (!attrname == exclude)
        if attr[0] == '!':
            inverse = True
            attr = attr[1:]
        else:
            inverse = False

        try:
            inverse, op, vals = buildFilterOp(inverse, attrMap[attr], vals)
            target = (filters, excludes)[inverse]
            target[op] = vals
        except KeyError:
            remainder[attr] = vals

    return (remainder, filters, excludes, limit, offset, ordering)

class CollectionHandler(web.RequestHandler):
    """
    Provides CRUD methods for a collection of Resources.
    """

    def set_default_headers(self):
        # this means we're open for all requests
        self.set_header("Access-Control-Allow-Origin", "*")
        self.set_header("Access-Control-Allow-Methods", CORS_RESP_METHODS)
        self.set_header("Access-Control-Allow-Headers", CORS_RESP_HEADERS)
        self.set_header("Access-Control-Expose-Headers", CORS_EXPS_HEADERS)

    def initialize(self, resource):
        """Saves the resource def."""
        self.resource = resource

    def get(self):
        """
        Allows retrieval of the entire collection of a resource.

        The view can be filtered, ordered and offset/limit applied.
        """
        # get resource attribute map
        attrMap = self.resource.getFilterAttrs()

        # decode arguments
        args = dict([(k, [self.decode_argument(x) for x in v])
                        for k, v in self.request.arguments.items()])

        _, filters, excludes, limit, offset, ordering = parseFiltersAndOrdering(args, attrMap)

        # get base query set
        qs = self.resource.getRootQS(self.request)

        # apply filters
        if len(filters):
            qs = qs.filter(**filters)
        if len(excludes):
            qs = qs.exclude(**excludes)
        if ordering is not None:
            qs = qs.order_by(*ordering)
        # apply offset possibilities
        if limit is not None and offset is not None:
            qs = qs[offset:offset+limit]
        elif limit is None and offset is not None:
            qs = qs[offset:]
        elif limit is not None and offset is None:
            qs = qs[:limit]

        # now JSON encode the resources
        results = self.resource.root_qs_to_json_dicts(self.reverse_url, qs)

        # write result
        self.set_header('Content-Type', JSON_MIMETYPE)
        self.write(to_json({'list': results}))

class InstanceHandler(web.RequestHandler):
    """
    Allows working with a single instance of the resource.
    """
    def initialize(self, resource):
        """Saves the resource def."""
        self.resource = resource

    def set_default_headers(self):
        # this means we're open for all requests
        self.set_header("Access-Control-Allow-Origin", "*")
        self.set_header("Access-Control-Allow-Methods", CORS_RESP_METHODS)
        self.set_header("Access-Control-Allow-Headers", CORS_RESP_HEADERS)
        self.set_header("Access-Control-Expose-Headers", CORS_EXPS_HEADERS)

    def get(self, id_):
        """Get a single instance of the resource and return it."""

        # we want the one with ID == id_
        qs = self.resource.getRootQS(self.request).filter(pk=id_)

        # now JSON encode the resources
        results = self.resource.root_qs_to_json_dicts(self.reverse_url, qs)

        if len(results) < 1:
            raise web.HTTPError(404, '{} model with ID {} does not exist'
                                .format(qs.model.__name__, id_))
        elif len(results) > 1:
            raise web.HTTPError(500, '{} model with ID {} has multiple entries'
                                .format(qs.model.__name__, id_))

        # write result
        self.set_header('Content-Type', JSON_MIMETYPE)
        self.write(to_json(results[0]))
