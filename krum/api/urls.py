from django.conf.urls import patterns, include, url

# This url file allows us to have multiple versions of the API running at once. 
# Necessary for having ipad apps etc as we cant guarentee they will get updated
urlpatterns = patterns('',
    url('^1/',include('krum.api.version_1.urls'))
)