"""Provides functions to update the IMDB based metadata."""
import logging

import django.db as db

import krum.metabase as metabase
import krum.metabase.models as mbm

import krum.imdb.files as files
import krum.imdb.parser as parser

logger = logging.getLogger(__name__)

CACHE_BYTES = 2**30 # 1 GB of sqlite cache :)
IMDB_SOURCE_NAME  = 'imdb'
SQLITE_MAX_PARAMS = 999

def update_data(update_files=True, force_redownload=False):
    '''Will update the raw IMDB text files then sync the metadata in the app
    database with the newly updated files'''

    # get the files
    if update_files:
        files.update_files(force_redownload)

    # take off SQLlite slowness thingies
    cur = db.connection.cursor()
    page_size = cur.execute('PRAGMA page_size').fetchone()[0]
    # large RAM cache + remove syncronous writes
    cur.execute('PRAGMA cache_size = {}'.format(CACHE_BYTES/page_size))
    cur.execute('PRAGMA synchronous = OFF')
    cur.execute('PRAGMA busy_timeout = 5000')
    db.transaction.commit_unless_managed()

    import_movies()
#    import_genres() # save till they're actually used

class BulkMovieHandler(object):
    '''Loads raw movie, serie and episode data into the database.

    The loads are done in the largest bulk statements possible to
    keep the load fast.

    Once done, finish should be called to update the actual IMDB
    metabase tables based on the raw data.'''
    def __init__(self, conn):
        # set up the sql fragments
        self.conn = conn
        self.cursor = conn.cursor()
        self.param_buffs = {'movie': [], 'episode': []}
        try:
            self.source_id = mbm.MetaSource.objects.get(name=IMDB_SOURCE_NAME).pk
        except mbm.MetaSource.DoesNotExist:
            s = mbm.MetaSource(name=IMDB_SOURCE_NAME)
            s.save()
            self.source_id = s.pk
        self.type_map = dict(mbm.MediaType.objects.values_list('name', 'pk'))
        self.ep_type_id = self.type_map['Episode']
        self.series_type_id = self.type_map['Series']

        self._clear_staging()

    def _clear_staging(self):
        '''Clear out the "staging" tables, i.e. wheer we store the IMDB
        data poured straight in from the files'''
        self.cursor.execute('delete from imdb_movie')
        self.cursor.execute('delete from imdb_episode')
        db.transaction.commit_unless_managed()

    def _commit_data(self, itype):
        '''Take the param buffers of the given object type and bulk push 'em into SQLITE'''
        # commit the pending data
        pbuff = self.param_buffs[itype]
        if len(pbuff) < 1:
            return

        sql_base, sql_params, pcount = BulkMovieHandler.sql_types[itype]
        obj_count = len(pbuff)//pcount
        sql = '{} {}'.format(sql_base, ','.join([sql_params]*obj_count))
        self.cursor.execute(sql, pbuff)
        del pbuff[:]
        db.transaction.commit_unless_managed()

    # SQL insert scaffolding
    sql_types = {
        'movie': ('''insert into imdb_movie (name, year) values''',
                 '''(%s, %s)''', 2),
        'episode': ('''insert into imdb_episode (name, year, ep_name, season, ep_num, ep_full_name) values''',
                    '''(%s, %s, %s, %s, %s, %s)''', 6)
    }
    def _add_item(self, itype, params):
        pbuff = self.param_buffs[itype]
        if len(params) + len(pbuff) > SQLITE_MAX_PARAMS:
            self._commit_data(itype)

        pbuff.extend(params)

    def add_movie(self, name, year):
        self._add_item('movie', (name, year))

    def add_episodes(self, name, year, eps):
        for ep_name, season, ep_num in eps:
            ep_full_name = name
            if ep_name or season or ep_num:
                if season or ep_num:
                    ep_full_name += u' - '
                if season and ep_num:
                    ep_full_name += u'S{}E{}'.format(print_maybenum(season), print_maybenum(ep_num))
                elif ep_num:
                    ep_full_name += u'{}'.format(print_maybenum(ep_num))
                if ep_name:
                    ep_full_name += u' - {}'.format(ep_name)
            self._add_item('episode', (name, year, ep_name or '', season or '', ep_num or '', ep_full_name))

    def finish(self):
        '''Now that the raw IMDB data has been pushed into flat tables,
        de-dup it and merge the new data into the metabase_* schema.'''
        self._commit_data('movie')
        self._commit_data('episode')

        # for all
        # mark existing ones
        logger.info('Marking existing metabase')
        sql = '''
            update metabase_metabase
            set other_id = 0
            where source_id = %s
        '''
        self.cursor.execute(sql, (self.source_id,))
        db.transaction.commit_unless_managed()
        # MOVIES
        # bring over de-duped copy, only new ones
        logger.info('Bringing over movie meta')
        sql = '''
            insert or ignore into metabase_metabase
                (name, year, media_type_id, source_id, added_timestamp, other_id, in_library)
            select name, year, %s, %s, datetime(), 1, 0
            from imdb_movie
            group by name, year
        '''
        self.cursor.execute(sql, (self.type_map['Movie'], self.source_id))
        db.transaction.commit_unless_managed()
        # update metabase_movie table
        logger.info('Adding movie extra')
        sql = '''
            insert or ignore into metabase_movie
                (metabase_ptr_id)
            select id
            from metabase_metabase
            where source_id = %s and media_type_id = %s and other_id = 1
        '''
        self.cursor.execute(sql, (self.source_id, self.type_map['Movie']))
        db.transaction.commit_unless_managed()

        # SERIES
        # insert new, de-duped series
        logger.info('Bringing over series')
        sql = '''
            insert or ignore into metabase_metabase
                (name, year, media_type_id, source_id, added_timestamp, other_id, in_library)
            select name, year, %s, %s, datetime(), 1, 0
            from imdb_episode
            group by name, year
        '''
        self.cursor.execute(sql, (self.series_type_id, self.source_id))
        db.transaction.commit_unless_managed()
        # update metabase_series table
        logger.info('Adding series extra')
        sql = '''
            insert or ignore into metabase_series
                (metabase_ptr_id)
            select id
            from metabase_metabase
            where source_id = %s and media_type_id = %s and other_id = 1
        '''
        self.cursor.execute(sql, (self.source_id, self.series_type_id))
        db.transaction.commit_unless_managed()

        # EPISODES
        # NOTE: These are a bit more complicated, need to cope with episodes
        # getting named later than getting added, e.g. initially just added
        # as {(1x4)} then updated to {Name (#1x4)}. Few more steps...

        #
        # TODO: Need to fix problem where there are 2 eps with same season + epnum
        # but different names in the source data (currently old one will be wiped
        # meaning we'll try to update the old one with the new which will fail
        # because
        #

        logger.info('Marking eps that are exact matches')
        sql = '''
            UPDATE OR IGNORE metabase_metabase
            SET
              other_id = (SELECT ie.rowid
                          FROM metabase_episode AS me
                            JOIN metabase_metabase AS ms
                              ON ms.id = me.series_id
                            JOIN imdb_episode AS ie
                              ON ie.name = ms.name AND
                                 ie.year = ms.year AND
                                 ie.season = me.season AND
                                 ie.ep_num = me.ep_num AND
                                 ie.ep_name = me.ep_name
                          WHERE me.rowid = metabase_metabase.id)
            WHERE
              source_id = %s AND
              media_type_id = %s
        '''
        self.cursor.execute(sql, (self.source_id, self.ep_type_id))
        db.transaction.commit_unless_managed()

        logger.info('Updating eps with name changes extra')
        # update eps that have had name changes
        sql = '''
            UPDATE OR IGNORE metabase_episode
            SET ep_name = (
                SELECT ie.ep_name
                FROM metabase_metabase AS mm
                  JOIN metabase_metabase AS ms
                    ON ms.rowid = metabase_episode.series_id
                  JOIN imdb_episode AS ie
                    ON ie.name = ms.name AND
                       ie.year = ms.year AND
                       ie.season = metabase_episode.season AND
                       ie.ep_num = metabase_episode.ep_num
                WHERE
                  mm.rowid = metabase_episode.rowid AND
                  mm.other_id = 0 AND
                  mm.source_id = %s AND
                  ie.rowid NOT IN (SELECT other_id
                                   FROM metabase_metabase
                                   WHERE source_id = %s AND
                                     media_type_id = %s AND
                                     other_id != 0))
        '''
        self.cursor.execute(sql, (self.source_id, self.source_id, self.ep_type_id))
        # update full name in metabase_metabase too
        logger.info('Updating eps with name changes meta')
        sql = '''
            UPDATE OR IGNORE metabase_metabase
            SET name = (
                SELECT ie.ep_full_name
                FROM metabase_episode AS me
                  JOIN metabase_metabase AS ms
                    ON ms.rowid = me.series_id
                  JOIN imdb_episode AS ie
                    ON ie.name = ms.name AND
                       ie.year = ms.year AND
                       ie.season = me.season AND
                       ie.ep_num = me.ep_num
                WHERE
                  me.rowid = metabase_metabase.rowid AND
                  ie.rowid NOT IN (SELECT other_id
                                   FROM metabase_metabase
                                   WHERE source_id = %s AND
                                     media_type_id = %s AND
                                     other_id != 0))
            WHERE
              source_id = %s AND
              other_id = 0
        '''
        self.cursor.execute(sql, (self.source_id, self.ep_type_id, self.source_id))
        db.transaction.commit_unless_managed()

        logger.info('Marking existing metabase episodes')
        sql = '''
            UPDATE metabase_metabase
            SET other_id = 0
            WHERE source_id = %s
        '''
        self.cursor.execute(sql, (self.source_id,))
        db.transaction.commit_unless_managed()

        # add the metabase bits, ep_full_name, year, id
        logger.info('Bringing over new episode meta')
        sql = '''
            INSERT OR IGNORE INTO metabase_metabase
                (name, year, media_type_id, source_id, added_timestamp, other_id, in_library)
            SELECT ep_full_name, year, %s, %s, datetime(), max(id), 0
            FROM imdb_episode
            GROUP BY ep_full_name, year
        '''
        self.cursor.execute(sql, (self.ep_type_id, self.source_id))
        db.transaction.commit_unless_managed()
        # add the metabase_episode bits for the new episodes
        logger.info('Bringing over new episode extra')
        sql = '''
            INSERT OR IGNORE INTO metabase_episode
                (metabase_ptr_id, series_id, season, ep_num, ep_name)
            SELECT mm.id, ms.id, season, ep_num, ep_name
            FROM metabase_metabase AS mm
                JOIN imdb_episode AS ie
                  ON ie.id = mm.other_id
                JOIN metabase_metabase AS ms
                  ON ms.name = ie.name AND
                     ms.year = ie.year
            WHERE
                mm.other_id != 0
                and mm.source_id = %s and mm.media_type_id = %s
                and ms.source_id = %s and ms.media_type_id = %s
        '''
        params = (self.source_id, self.ep_type_id, self.source_id, self.series_type_id)
        self.cursor.execute(sql, params)
        db.transaction.commit_unless_managed()

        # Finally, add new shows to the library for series that are already in
        # the library
        sql = '''
            UPDATE metabase_metabase
            SET
                in_library = 1
            WHERE
                rowid IN (SELECT epi_ext.rowid
                          FROM metabase_episode AS epi_ext
                              JOIN metabase_metabase AS series_base
                                  ON series_base.rowid = epi_ext.series_id
                              JOIN metabase_metabase AS epi_base
                                  ON epi_base.rowid = epi_ext.rowid
                          WHERE
                              series_base.source_id = %s AND
                              series_base.in_library = 1 AND
                              epi_base.in_library = 0)
        '''
        params = (self.source_id, )
        self.cursor.execute(sql, params)
        db.transaction.commit_unless_managed()

        # Update the Full text search table. Above we've made sure all
        # new/changed items have a non zero other_id
        metabase.reindex_metabase_items()

        self._clear_staging()

def print_maybenum(string):
    try:
        return '{:02}'.format(int(string))
    except ValueError:
        return string

def rollup_movies(movies):
    '''Rolls up the seperate yields from parse_movies. Mainly so that shows are
    yielded with all their episodes and is_movie set.'''
    curr_mov = (None, None, None)
    for name, year, epinf in movies:
        if (name, year) != curr_mov[:2]:
            if curr_mov[0] is not None:
                yield curr_mov

            curr_mov = (name, year, [])

        if epinf:
            curr_mov[2].append(epinf)

    yield curr_mov

def import_movies():
    conn = db.connection
    # load in movies
    logger.info('Starting RAW movie data import')
    with files.get_file('movies') as fd:
        bulky = BulkMovieHandler(conn)

        for name, year, eps in rollup_movies(parser.parse_movies(fd)):
            if eps:
                bulky.add_episodes(name, year, eps)
            else:
                bulky.add_movie(name, year)

    bulky.finish()
    del bulky
    logger.info('Finished RAW movie data import')

def import_genres():
    cursor = db.connection.cursor()

    # get all current genres
    param_buff = []

    def add_item(params):
        if len(param_buff) + len(params) > SQLITE_MAX_PARAMS:
            push_items()

        param_buff.extend(params)

    def push_items():
        if len(param_buff) == 0:
            return

        sql_base = 'insert into imdb_genre (name, year, genre_id) values '
        sql_parms = '(%s, %s, %s)'
        sql = '{} {}'.format(sql_base, ','.join([sql_parms]*(len(param_buff)//3)))
        cursor.execute(sql, param_buff)
        db.transaction.commit_unless_managed()
        del param_buff[:]

    # clean out old items
    logger.info('Clearing old genre staging')
    cursor.execute('delete from imdb_genre')
    db.transaction.commit_unless_managed()

    # load in genres
    logger.info('Importing RAW genres')
    genre_map  = dict(mbm.Genre.objects.values_list('name', 'pk'))
    with files.get_file('genres') as fd:

        for n,y,g in parser.parse_genres(fd):
            try:
                gid = genre_map[g]
            except KeyError:
                new_g = mbm.Genre(name=g)
                new_g.save()
                gid = new_g.pk
                genre_map[g] = gid
                new_g = None

            add_item((n,y,gid))

        push_items()

    # get the ID for this, IMDB, source.
    source_id = mbm.MetaSource.objects.get(name=IMDB_SOURCE_NAME).pk

    # create index to speed lookups
    logger.info('Creating index for genre load speed')
    sql = '''
        create index genre_load_speedup
            on metabase_metabase (name,year,id)
    '''
    cursor.execute(sql)
    db.transaction.commit_unless_managed()

    # now finish up by linking across all the genre DAATAA!!
    logger.info('bringing genres into metabase structure')
    sql = '''
        insert or ignore into metabase_genrelink (meta_id, genre_id)
        select m.id, g.genre_id
        from imdb_genre as g
            join metabase_metabase as m indexed by genre_load_speedup
            on m.name = g.name and m.year = g.year
        where
            m.source_id = %s and m.media_type_id in (%s, %s)'''
    cursor.execute(sql, (source_id, 1, 2))
    db.transaction.commit_unless_managed()

    # drop index to speed lookups
    logger.info('Dropping index for genre load speed')
    sql = '''
        drop index genre_load_speedup
    '''
    cursor.execute(sql)
    db.transaction.commit_unless_managed()

    logger.info('Genres all done')
