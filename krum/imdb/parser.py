import re

AKA_COUNTRY_KEEP = ("International: English title", "USA")
AKA_TITLE_KEEP = ("imdb display title", "uncensored intended title", "alternative title")

class ParseError(Exception):
    pass

def eat_header(f, hdr_line):
    l = f.readline()
    while l:
        if hdr_line in l:
            f.readline() # throw away the empty line
            f.readline()
            return

        l = f.readline()

def parse_epinf(epinf):
    '''Three cases
        Returns (name, season, episode)
         or     (name, None, Episode)
         or     (name, None, None)
    '''
    if '(' in epinf:
        # we want the last () block, if there are 2
        ename, rest = epinf.rsplit('(',1)
        ename = ename[:-1]

        if rest.startswith('#'):
            rest = rest[1:]

            # it's a season.episode style
            seinf, _ = rest.split(')',1)
            if '.' in seinf:
                season, episode = seinf.split('.',1)
            else:
                season, episode = None, seinf

            return (ename, season, episode)
        else:
            # it's something else, e.g. 1988-04-07
            einf, _ = rest.split(')',1)
            return (ename, None, einf)
    else:
        return (epinf, None, None)

def parse_showname(sn):
    if sn.startswith('"'):
        # quoted name, so grab text between quotes
        name, rest = sn[1:].split('"', 1)
        _, rest = rest.split('(', 1)
        year, rest = rest.split(')', 1)
    else:
        match = re.match("(.+)\((.{4}).*\)(.*)", sn)
        groups = match.groups()
        name = groups[0].rstrip()
        year = groups[1].strip()
        rest = groups[2]

    if '{' in rest:
        # it's an episode, so pull out the info
        _, rest = rest.split('{', 1)
        if rest.startswith('{SUSPENDED}}'): # {{SUSPENDED}} sillies
            return (name, year, None)

        epinf, rest = rest.rsplit('}', 1)
        epinf = parse_epinf(epinf)

        return (name, year, epinf)
    else:
        return (name, year, None)

def parse_movies(f):
    eat_header(f, 'MOVIES LIST')

    l = f.readline()
    while l:
        if l == '\n' or "{{SUSPENDED}}" in l:
            l = f.readline()
            continue
        elif l.startswith('-----'):
            break

        try:
            sn = l.split('\t',1)[0]
            # ignoring video games
            if not sn.endswith('(VG)'):
                yield parse_showname(sn)
        except Exception as e:
            raise ParseError("Error with line: \n\t{}, {}".format(l,e))

        l = f.readline()

def parse_genres(f):
    eat_header(f, 'THE GENRES LIST')

    l = f.readline()
    while l:
        if l == '\n':
            continue
        elif l.startswith('-----'):
            break

        try:
            sn, rest = l.split('\t',1)
            name, year, _ = parse_showname(sn)
            yield name, year, rest.strip()

        except Exception as e:
            raise ParseError("Error with line: \n\t{}, {}".format(l,e))

        l = f.readline()

def readparan(s):
    if not s.startswith('('):
        raise Exception('must start with (')

    pc = 1
    for i in range(1, len(s)+1):
        if s[i] == '(':
            pc += 1
        elif s[i] == ')':
            pc -= 1

        if pc == 0:
            return s[1:i], s[i+1:]

    raise Exception('unbalanced parens')

def parse_showaka(s):
    s = s.lstrip()
    if not s.startswith("(aka "):
        raise Exception("Line should start with (aka ")

    sn, s = readparan(s)
    newName, _, _ = parse_showname(sn[4:]) # ignore the "aka "

    parts = []
    while '(' in s:
        s = s.lstrip()
        part, s = readparan(s)
        parts.append(part)

    # find either international title or US title
    if len(parts) != 2:
        return None, 100000

    country, title = parts
    if country in AKA_COUNTRY_KEEP and title in AKA_TITLE_KEEP:
        return newName, AKA_TITLE_KEEP.index(title)

    return None, 100000

def parse_akatitles(f):
    eat_header(f, 'AKA TITLES LIST')

    l = f.readline()
    while l:
        if l == '\n':
            continue
        elif l.startswith('-----'):
            break

        try:
            name, year, _ = parse_showname(l)

            # find best option
            newname, pref = None, 100000
            l = f.readline()
            while l:
                if l == '\n':
                    break

                nn, p = parse_showaka(l)
                if nn != None and p < pref:
                    newname, pref = nn, p

                l = f.readline()

            if newname != None:
                yield name, year, newname

        except ParseError as e:
            raise ParseError("Error with line: \n\t{}, {}".format(l,e))

        l = f.readline()
