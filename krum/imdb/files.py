"""
Provides functions for IMDB file management, i.e. downloading,
updating with diffs, CRC checking, etc.
"""

import os.path as op
import os
import codecs
import logging

from urllib.request import urlretrieve, ContentTooShortError

from subprocess import check_call
from datetime import datetime, date
from shutil import rmtree, copy
from krum import cfg

logger = logging.getLogger(__name__)

FILES = ['movies','aka-titles']

# TODO: support .gz versions
EXTN = '.list'

# TODO: let the user supply the base IMDB path, but default to this
URL_BASE = 'ftp://ftp.fu-berlin.de/pub/misc/movies/database'
DIFF_URL_BASE = 'ftp://ftp.fu-berlin.de/pub/misc/movies/database/diffs'
DIFF_FMT = 'diffs-{:%y%m%d}.tar.gz'

def working_dir():
    return op.abspath(op.join(cfg['data_path'], 'imdb'))

class NoSuchPatch(Exception):
    pass

class NoSuchFile(Exception):
    pass

# TODO: Pathing is in multiple places. Consolidate?
def get_file(name):
    return codecs.open(op.join(working_dir(), '{}{}'.format(name,EXTN)), 'r', 'latin1')

def update_files(force_redownload=False):
    '''Checks all files exist and patches them incrementally from
       oldest file to current. E.g. if you had a movies file that
       was 2 patches old and a genres files that was 1 patch old,
       this would start by patching movies, then it would patch
       both movies and genres with the second patch file.'''

    logger.info('Starting IMDB file update')
    if not op.exists(working_dir()):
        os.makedirs(working_dir())

    # track which patches have already been applied, so we can move on from bodgy files...
    applied_patches = []

    # find oldest date, fetch and apply the patch
        # break when no more patches
    while True:
        earliest = date.max
        for filename in FILES:
            local_path = op.join(working_dir(), '{}{}'.format(filename, EXTN))

            # TODO: add in some logging/reporting on progress
            if not op.exists(local_path) or force_redownload:
                logger.info('Fetching: {}/{}.list.gz'.format(URL_BASE, filename))
                zipped_path = local_path + '.gz'
                # TODO: progress reporting
                fetchurl(URL_BASE + '/' + '{}.list.gz'.format(filename), zipped_path)
                check_call(['gzip', '-f', '--decompress', op.abspath(zipped_path)], cwd=working_dir())

            try:
                hdr = read_header(local_path)
            except:
                logger.warn('Error reading header of file: {}'.format(local_path))
                raise

            if hdr['Date'] not in applied_patches:
                earliest = min(earliest, hdr['Date'])

        logger.info('Youngest file is dated: {}'.format(earliest))

        try:
            fetch_and_apply_patch(earliest)
            applied_patches.append(earliest)
        except NoSuchPatch:
            logger.info('Patching complete, there are no newer patches')
            break

    logger.info('Finished IMDB file update')

def fetch_and_apply_patch(patch_date):
    diffs_filename = DIFF_FMT.format(patch_date)
    diffs_url = DIFF_URL_BASE + '/' + diffs_filename
    diffs_path = op.join(working_dir(), 'diffs')
    diffs_file = op.join(working_dir(), diffs_filename)
    logger.info('Fetching patch: '+diffs_url)
    try:
        fetchurl(diffs_url, diffs_file)
    except NoSuchFile:
        raise NoSuchPatch()

    # extract diffs
    try:
        logger.info('Extracting diffs: {} in {}'.format(diffs_filename, working_dir()))
        check_call(['tar', '-xzf', diffs_filename], cwd=working_dir())

        # patch files
        for f in FILES:
            fp = op.join(working_dir(), '{}{}'.format(f,EXTN))
            pp = op.join(diffs_path, '{}{}'.format(f,EXTN))
            hdr = read_header(fp)

            # skip files that are newer/older than patch is intended for
            if hdr['Date'] != patch_date:
                logger.error('Error: Patch date mismatch for "{}", skipping to next file'.format(f))
                continue
            if not op.exists(pp):
                logger.error('Error: Missing patch file: "{}", skipping to next file'.format(f))
                continue

            logger.info('Patching: {} with {}'.format(fp,pp))
            check_call(['patch', fp, pp], cwd=working_dir())
    finally:
        os.remove(diffs_file)
        rmtree(diffs_path)

def fetchurl(url, localpath):
    '''Tries to download the resource at the given URL to the given file path.
        Raises a NoSuchFile error if errors encountered.
    '''
    try:
        path, info = urlretrieve(url, localpath)

        if path != localpath:
            copy(path, localpath)
    # TODO: error logging/reporting
    except IOError:
        raise NoSuchFile()

# TODO: Not correct. Not sure why...
# Tried skipping up to data, and all the way back to 0 lines, hrmm
def calc_crc(path):
    crc = 0
    with codecs.open(path, 'rb') as f:
        l = f.readline()
        for l in f:
            crc = crc32(l, crc)

    return crc & 0xffffffff

def read_header(path):
    with codecs.open(path, 'r', encoding='latin1') as f:
        l = f.readline()

    # split up the line..
    parts = l.split('  ',2)
    kvs = [x.split(':',1) for x in parts]
    d = dict([(k.strip(),v.strip()) for k,v in kvs])
    d['CRC'] = int(d['CRC'], 16)
    d['Date'] = datetime.strptime(d['Date'], '%a %b %d %H:%M:%S %Y').date()
    return d

