# -*- coding: utf-8 -*-
import datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding model 'Movie'
        db.create_table('imdb_movie', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=30)),
            ('year', self.gf('django.db.models.fields.CharField')(max_length=8)),
        ))
        db.send_create_signal('imdb', ['Movie'])

        # Adding model 'Episode'
        db.create_table('imdb_episode', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=30)),
            ('year', self.gf('django.db.models.fields.CharField')(max_length=8)),
            ('ep_num', self.gf('django.db.models.fields.CharField')(max_length=8)),
            ('ep_name', self.gf('django.db.models.fields.CharField')(max_length=30)),
            ('season', self.gf('django.db.models.fields.CharField')(max_length=8)),
            ('ep_full_name', self.gf('django.db.models.fields.CharField')(max_length=60)),
        ))
        db.send_create_signal('imdb', ['Episode'])

        # Speed up import process
        db.create_index('imdb_episode', ['name', 'year', 'season', 'ep_num', 'ep_name'])

        # Adding model 'Genre'
        db.create_table('imdb_genre', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=30)),
            ('year', self.gf('django.db.models.fields.CharField')(max_length=8)),
            ('genre_id', self.gf('django.db.models.fields.IntegerField')()),
        ))
        db.send_create_signal('imdb', ['Genre'])


    def backwards(self, orm):
        # Deleting model 'Movie'
        db.delete_table('imdb_movie')

        # Deleting model 'Episode'
        db.delete_table('imdb_episode')

        # Deleting model 'Genre'
        db.delete_table('imdb_genre')


    models = {
        'imdb.episode': {
            'Meta': {'object_name': 'Episode'},
            'ep_full_name': ('django.db.models.fields.CharField', [], {'max_length': '60'}),
            'ep_name': ('django.db.models.fields.CharField', [], {'max_length': '30'}),
            'ep_num': ('django.db.models.fields.CharField', [], {'max_length': '8'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '30'}),
            'season': ('django.db.models.fields.CharField', [], {'max_length': '8'}),
            'year': ('django.db.models.fields.CharField', [], {'max_length': '8'})
        },
        'imdb.genre': {
            'Meta': {'object_name': 'Genre'},
            'genre_id': ('django.db.models.fields.IntegerField', [], {}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '30'}),
            'year': ('django.db.models.fields.CharField', [], {'max_length': '8'})
        },
        'imdb.movie': {
            'Meta': {'object_name': 'Movie'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '30'}),
            'year': ('django.db.models.fields.CharField', [], {'max_length': '8'})
        }
    }

    complete_apps = ['imdb']
