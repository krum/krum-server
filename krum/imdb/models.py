
from django.db import models

# NOTE: These are just used for staging data to speed up the import process
class Movie(models.Model):
    name = models.CharField(max_length=30)
    year = models.CharField(max_length=8)

class Episode(models.Model):
    name           = models.CharField(max_length=30)
    year           = models.CharField(max_length=8)
    ep_num         = models.CharField(max_length=8)
    ep_name        = models.CharField(max_length=30)
    season         = models.CharField(max_length=8)
    ep_full_name   = models.CharField(max_length=60)

class Genre(models.Model):
    name     = models.CharField(max_length=30)
    year     = models.CharField(max_length=8)
    genre_id = models.IntegerField()
