# -*- coding: utf-8 -*-
from south.db import db
from south.v2 import SchemaMigration
import krum.metabase as metabase

class Migration(SchemaMigration):
    """
    Move the Full-text index over to contentless FTS4 from FTS3.
    """

    def fill_index(self):
        sql = '''
            select id, name || ' ' || year
            from metabase_metabase
        '''
        db.execute('insert into metabase_fts (docid, content) {}'.format(sql))

    def forwards(self, orm):
        # drop old
        db.execute('DROP TABLE metabase_fts')
        # make new
        db.execute('CREATE VIRTUAL TABLE metabase_fts USING fts4(content="", content)')
        # refill
        self.fill_index()

    def backwards(self, orm):
        # drop new
        db.execute('DROP TABLE metabase_fts')
        # make old
        db.execute('CREATE VIRTUAL TABLE metabase_fts USING fts3(tokenize=porter)')
        # refill
        self.fill_index()

    models = {
        'metabase.episode': {
            'Meta': {'_ormbases': ['metabase.MetaBase'], 'object_name': 'Episode'},
            'ep_name': ('django.db.models.fields.CharField', [], {'max_length': '50'}),
            'ep_num': ('django.db.models.fields.CharField', [], {'max_length': '20', 'blank': 'True', 'default': "''"}),
            'metabase_ptr': ('django.db.models.fields.related.OneToOneField', [], {'unique': 'True', 'to': "orm['metabase.MetaBase']", 'primary_key': 'True'}),
            'season': ('django.db.models.fields.CharField', [], {'max_length': '20', 'blank': 'True', 'default': "''"}),
            'series': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['metabase.MetaBase']", 'related_name': "'episode_list'"})
        },
        'metabase.genre': {
            'Meta': {'object_name': 'Genre'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '40', 'unique': 'True'})
        },
        'metabase.genrelink': {
            'Meta': {'unique_together': "(('meta', 'genre'),)", 'object_name': 'GenreLink'},
            'genre': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['metabase.Genre']"}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'meta': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['metabase.MetaBase']"})
        },
        'metabase.mediatype': {
            'Meta': {'object_name': 'MediaType'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '30', 'unique': 'True'}),
            'parent': ('django.db.models.fields.related.ForeignKey', [], {'null': 'True', 'to': "orm['metabase.MediaType']", 'related_name': "'children'", 'default': 'None'})
        },
        'metabase.metabase': {
            'Meta': {'unique_together': "(('source', 'name', 'year', 'media_type'),)", 'object_name': 'MetaBase'},
            'added_timestamp': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            'genres': ('django.db.models.fields.related.ManyToManyField', [], {'through': "orm['metabase.GenreLink']", 'to': "orm['metabase.Genre']", 'symmetrical': 'False'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'in_library': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'media_type': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['metabase.MediaType']"}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '50'}),
            'other_id': ('django.db.models.fields.IntegerField', [], {}),
            'source': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['metabase.MetaSource']"}),
            'year': ('django.db.models.fields.CharField', [], {'max_length': '4'})
        },
        'metabase.metasource': {
            'Meta': {'object_name': 'MetaSource'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '30', 'unique': 'True'})
        },
        'metabase.movie': {
            'Meta': {'_ormbases': ['metabase.MetaBase'], 'object_name': 'Movie'},
            'metabase_ptr': ('django.db.models.fields.related.OneToOneField', [], {'unique': 'True', 'to': "orm['metabase.MetaBase']", 'primary_key': 'True'})
        },
        'metabase.series': {
            'Meta': {'_ormbases': ['metabase.MetaBase'], 'object_name': 'Series'},
            'metabase_ptr': ('django.db.models.fields.related.OneToOneField', [], {'unique': 'True', 'to': "orm['metabase.MetaBase']", 'primary_key': 'True'})
        }
    }

    complete_apps = ['metabase']