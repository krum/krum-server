# -*- coding: utf-8 -*-
from south.utils import datetime_utils as datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models
import krum.imdb.update
from collections import defaultdict
import krum.metabase.models as mbm
import krum.content.models as cm

def print_maybenum(string):
    """Try to make it an 0 padded number or bail and hand it back direct"""
    try:
        return '{:02}'.format(int(string))
    except ValueError:
        return string

def format_full_name(series_name, season, ep_num, ep_name):
    """The format at the point in time this migration was performed"""
    ep_full_name = series_name
    if ep_name or season or ep_num:
        if season or ep_num:
            ep_full_name += u' - '
        if season and ep_num:
            ep_full_name += u'S{}E{}'.format(print_maybenum(season), print_maybenum(ep_num))
        elif ep_num:
            ep_full_name += u'{}'.format(print_maybenum(ep_num))
        if ep_name:
            ep_full_name += u' - {}'.format(ep_name)

    return ep_full_name

class Migration(SchemaMigration):

    def forwards(self, orm):
        # SQL for winners:
        #  SELECT MIN(rowid)
        #  FROM metabase_episode
        #  GROUP BY series_id, season, ep_num
        #  HAVING COUNT(*) > 1)

        try:
            imdb_source_id = mbm.MetaSource.objects.get(name=krum.imdb.update.IMDB_SOURCE_NAME).pk
        except mbm.MetaSource.DoesNotExist:
            s = mbm.MetaSource(name=krum.imdb.update.IMDB_SOURCE_NAME)
            s.save()
            imdb_source_id = s.pk

        # get all min ids, linked to all ids, season, ep_num, ep_name
        # Note: we only consider episodes with both a non '' season and ep_num
        # otherwise it tries to merge all specials and so on for shows.
        sql = """
        SELECT mineps.rowid, e.rowid, e.series_id, e.season, e.ep_num, e.ep_name
        FROM metabase_episode as mineps
            JOIN metabase_episode as e
                ON e.series_id = mineps.series_id AND
                e.season = mineps.season AND
                e.ep_num = mineps.ep_num
        WHERE mineps.rowid in (SELECT MIN(e.rowid)
            FROM metabase_episode as e
                JOIN metabase_metabase as m
                    ON m.rowid = e.rowid
            WHERE m.source_id = %s AND
                e.season != '' AND
                e.ep_num != ''
            GROUP BY e.series_id, e.season, e.ep_num
            HAVING COUNT(*) > 1)
        """
        eps = db.execute(sql, (imdb_source_id,))
        # roll up by min ep id, but don't include the min
        grouped_eps = defaultdict(list)
        for ep in eps:
            # skip the ep with minimum id (so we don't delete it later)
            if ep[0] == ep[1]:
                continue

            grouped_eps[ep[0]].append(ep[1:])

        print("Found {} dups amonst {} unique eps".format(len(eps), len(grouped_eps)))

        # now perform the fixes
        for minid, othereps in grouped_eps.items():
            series_id, season, epnum, epname = othereps[-1][1:]
            print("START Fixing series: {}".format(series_id))
            series_name = mbm.Series.objects.values_list('name').get(pk=series_id)[0]

            # point all Content at the minid
            otherids = [x[0] for x in othereps]
            cm.Content.objects.filter(metadata_id__in=otherids).update(metadata=minid)

            # delete others (need to do before update because of unique constraint)
            mbm.Episode.objects.filter(pk__in=otherids).delete()

            # update minid full_ep_name to latest known show name
            mbm.Episode.objects.filter(pk=minid).update(name=
                format_full_name(series_name, season, epnum, epname))

            print("FINISH Fixing series: {}".format(series_id))

    def backwards(self, orm):
        # this is a data fix, so no backwards...
        pass

    models = {
        'metabase.episode': {
            'Meta': {'_ormbases': ['metabase.MetaBase'], 'object_name': 'Episode'},
            'ep_name': ('django.db.models.fields.CharField', [], {'max_length': '50'}),
            'ep_num': ('django.db.models.fields.CharField', [], {'default': "''", 'blank': 'True', 'max_length': '20'}),
            'metabase_ptr': ('django.db.models.fields.related.OneToOneField', [], {'primary_key': 'True', 'to': "orm['metabase.MetaBase']", 'unique': 'True'}),
            'season': ('django.db.models.fields.CharField', [], {'default': "''", 'blank': 'True', 'max_length': '20'}),
            'series': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['metabase.MetaBase']", 'related_name': "'episode_list'"})
        },
        'metabase.genre': {
            'Meta': {'object_name': 'Genre'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '40', 'unique': 'True'})
        },
        'metabase.genrelink': {
            'Meta': {'object_name': 'GenreLink', 'unique_together': "(('meta', 'genre'),)"},
            'genre': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['metabase.Genre']"}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'meta': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['metabase.MetaBase']"})
        },
        'metabase.mediatype': {
            'Meta': {'object_name': 'MediaType'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '30', 'unique': 'True'}),
            'parent': ('django.db.models.fields.related.ForeignKey', [], {'default': 'None', 'null': 'True', 'to': "orm['metabase.MediaType']", 'related_name': "'children'"})
        },
        'metabase.metabase': {
            'Meta': {'object_name': 'MetaBase', 'unique_together': "(('source', 'name', 'year', 'media_type'),)"},
            'added_timestamp': ('django.db.models.fields.DateTimeField', [], {'blank': 'True', 'auto_now_add': 'True'}),
            'genres': ('django.db.models.fields.related.ManyToManyField', [], {'through': "orm['metabase.GenreLink']", 'to': "orm['metabase.Genre']", 'symmetrical': 'False'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'in_library': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'media_type': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['metabase.MediaType']"}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '50'}),
            'other_id': ('django.db.models.fields.IntegerField', [], {}),
            'source': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['metabase.MetaSource']"}),
            'year': ('django.db.models.fields.CharField', [], {'max_length': '4'})
        },
        'metabase.metasource': {
            'Meta': {'object_name': 'MetaSource'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '30', 'unique': 'True'})
        },
        'metabase.movie': {
            'Meta': {'_ormbases': ['metabase.MetaBase'], 'object_name': 'Movie'},
            'metabase_ptr': ('django.db.models.fields.related.OneToOneField', [], {'primary_key': 'True', 'to': "orm['metabase.MetaBase']", 'unique': 'True'})
        },
        'metabase.series': {
            'Meta': {'_ormbases': ['metabase.MetaBase'], 'object_name': 'Series'},
            'metabase_ptr': ('django.db.models.fields.related.OneToOneField', [], {'primary_key': 'True', 'to': "orm['metabase.MetaBase']", 'unique': 'True'})
        }
    }

    complete_apps = ['metabase']