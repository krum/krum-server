# -*- coding: utf-8 -*-
import datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding model 'MediaType'
        db.create_table('metabase_mediatype', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('name', self.gf('django.db.models.fields.CharField')(unique=True, max_length=30)),
        ))
        db.send_create_signal('metabase', ['MediaType'])

        # Adding model 'MetaSource'
        db.create_table('metabase_metasource', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('name', self.gf('django.db.models.fields.CharField')(unique=True, max_length=30)),
        ))
        db.send_create_signal('metabase', ['MetaSource'])

        # Adding model 'Genre'
        db.create_table('metabase_genre', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('name', self.gf('django.db.models.fields.CharField')(unique=True, max_length=40)),
        ))
        db.send_create_signal('metabase', ['Genre'])

        # Adding model 'GenreLink'
        db.create_table('metabase_genrelink', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('meta', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['metabase.MetaBase'])),
            ('genre', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['metabase.Genre'])),
        ))
        db.send_create_signal('metabase', ['GenreLink'])

        # Adding unique constraint on 'GenreLink', fields ['meta', 'genre']
        db.create_unique('metabase_genrelink', ['meta_id', 'genre_id'])

        # Adding model 'MetaBase'
        db.create_table('metabase_metabase', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=50)),
            ('year', self.gf('django.db.models.fields.CharField')(max_length=4)),
            ('media_type', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['metabase.MediaType'])),
            ('source', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['metabase.MetaSource'])),
            ('added_timestamp', self.gf('django.db.models.fields.DateTimeField')(auto_now_add=True, blank=True)),
            ('other_id', self.gf('django.db.models.fields.IntegerField')()),
            ('in_library', self.gf('django.db.models.fields.BooleanField')(default=False)),
        ))
        db.send_create_signal('metabase', ['MetaBase'])

        # Adding unique constraint on 'MetaBase', fields ['source', 'name', 'year', 'media_type']
        db.create_unique('metabase_metabase', ['source_id', 'name', 'year', 'media_type_id'])

        # So we can track what's in our library and what's not, and have it be quick :)
        db.create_index('metabase_metabase', ['in_library'])

        # Adding model 'Episode'
        db.create_table('metabase_episode', (
            ('metabase_ptr', self.gf('django.db.models.fields.related.OneToOneField')(to=orm['metabase.MetaBase'], unique=True, primary_key=True)),
            ('series', self.gf('django.db.models.fields.related.ForeignKey')(related_name='episode_list', to=orm['metabase.MetaBase'])),
            ('season', self.gf('django.db.models.fields.CharField')(default='', max_length=20, blank=True)),
            ('ep_num', self.gf('django.db.models.fields.CharField')(default='', max_length=20, blank=True)),
            ('ep_name', self.gf('django.db.models.fields.CharField')(max_length=50)),
        ))
        db.send_create_signal('metabase', ['Episode'])

        # Adding model 'Movie'
        db.create_table('metabase_movie', (
            ('metabase_ptr', self.gf('django.db.models.fields.related.OneToOneField')(to=orm['metabase.MetaBase'], unique=True, primary_key=True)),
        ))
        db.send_create_signal('metabase', ['Movie'])

        # Adding model 'Series'
        db.create_table('metabase_series', (
            ('metabase_ptr', self.gf('django.db.models.fields.related.OneToOneField')(to=orm['metabase.MetaBase'], unique=True, primary_key=True)),
        ))
        db.send_create_signal('metabase', ['Series'])


    def backwards(self, orm):
        # Removing unique constraint on 'MetaBase', fields ['source', 'name', 'year', 'media_type']
        db.delete_unique('metabase_metabase', ['source_id', 'name', 'year', 'media_type_id'])

        # Removing unique constraint on 'GenreLink', fields ['meta', 'genre']
        db.delete_unique('metabase_genrelink', ['meta_id', 'genre_id'])

        # Deleting model 'MediaType'
        db.delete_table('metabase_mediatype')

        # Deleting model 'MetaSource'
        db.delete_table('metabase_metasource')

        # Deleting model 'Genre'
        db.delete_table('metabase_genre')

        # Deleting model 'GenreLink'
        db.delete_table('metabase_genrelink')

        # Deleting model 'MetaBase'
        db.delete_table('metabase_metabase')

        # Deleting model 'Episode'
        db.delete_table('metabase_episode')

        # Deleting model 'Movie'
        db.delete_table('metabase_movie')

        # Deleting model 'Series'
        db.delete_table('metabase_series')


    models = {
        'metabase.episode': {
            'Meta': {'object_name': 'Episode', '_ormbases': ['metabase.MetaBase']},
            'ep_name': ('django.db.models.fields.CharField', [], {'max_length': '50'}),
            'ep_num': ('django.db.models.fields.CharField', [], {'default': "''", 'max_length': '20', 'blank': 'True'}),
            'metabase_ptr': ('django.db.models.fields.related.OneToOneField', [], {'to': "orm['metabase.MetaBase']", 'unique': 'True', 'primary_key': 'True'}),
            'season': ('django.db.models.fields.CharField', [], {'default': "''", 'max_length': '20', 'blank': 'True'}),
            'series': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'episode_list'", 'to': "orm['metabase.MetaBase']"})
        },
        'metabase.genre': {
            'Meta': {'object_name': 'Genre'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '40'})
        },
        'metabase.genrelink': {
            'Meta': {'unique_together': "(('meta', 'genre'),)", 'object_name': 'GenreLink'},
            'genre': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['metabase.Genre']"}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'meta': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['metabase.MetaBase']"})
        },
        'metabase.mediatype': {
            'Meta': {'object_name': 'MediaType'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '30'})
        },
        'metabase.metabase': {
            'Meta': {'unique_together': "(('source', 'name', 'year', 'media_type'),)", 'object_name': 'MetaBase'},
            'added_timestamp': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            'genres': ('django.db.models.fields.related.ManyToManyField', [], {'to': "orm['metabase.Genre']", 'through': "orm['metabase.GenreLink']", 'symmetrical': 'False'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'in_library': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'media_type': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['metabase.MediaType']"}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '50'}),
            'other_id': ('django.db.models.fields.IntegerField', [], {}),
            'source': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['metabase.MetaSource']"}),
            'year': ('django.db.models.fields.CharField', [], {'max_length': '4'})
        },
        'metabase.metasource': {
            'Meta': {'object_name': 'MetaSource'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '30'})
        },
        'metabase.movie': {
            'Meta': {'object_name': 'Movie', '_ormbases': ['metabase.MetaBase']},
            'metabase_ptr': ('django.db.models.fields.related.OneToOneField', [], {'to': "orm['metabase.MetaBase']", 'unique': 'True', 'primary_key': 'True'})
        },
        'metabase.series': {
            'Meta': {'object_name': 'Series', '_ormbases': ['metabase.MetaBase']},
            'metabase_ptr': ('django.db.models.fields.related.OneToOneField', [], {'to': "orm['metabase.MetaBase']", 'unique': 'True', 'primary_key': 'True'})
        }
    }

    complete_apps = ['metabase']
