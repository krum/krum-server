# -*- coding: utf-8 -*-
import datetime
from south.db import db
from south.v2 import DataMigration
from django.db import models

class Migration(DataMigration):

    def forwards(self, orm):
        # insert media types
        mtypes = [('1', 'Movie'), ('2', 'Series'), ('3', 'Episode')]
        for mt in mtypes:
            db.execute('insert into metabase_mediatype (id, name) values (%s, %s)', mt)

    def backwards(self, orm):
        db.execute('delete from metabase_mediatype where id in (1,2,3)')

    models = {
        'metabase.episode': {
            'Meta': {'object_name': 'Episode', '_ormbases': ['metabase.MetaBase']},
            'ep_name': ('django.db.models.fields.CharField', [], {'max_length': '50'}),
            'ep_num': ('django.db.models.fields.CharField', [], {'default': "''", 'max_length': '20', 'blank': 'True'}),
            'metabase_ptr': ('django.db.models.fields.related.OneToOneField', [], {'to': "orm['metabase.MetaBase']", 'unique': 'True', 'primary_key': 'True'}),
            'season': ('django.db.models.fields.CharField', [], {'default': "''", 'max_length': '20', 'blank': 'True'}),
            'series': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'episode_list'", 'to': "orm['metabase.MetaBase']"})
        },
        'metabase.genre': {
            'Meta': {'object_name': 'Genre'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '40'})
        },
        'metabase.genrelink': {
            'Meta': {'unique_together': "(('meta', 'genre'),)", 'object_name': 'GenreLink'},
            'genre': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['metabase.Genre']"}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'meta': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['metabase.MetaBase']"})
        },
        'metabase.mediatype': {
            'Meta': {'object_name': 'MediaType'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '30'})
        },
        'metabase.metabase': {
            'Meta': {'unique_together': "(('source', 'name', 'year', 'media_type'),)", 'object_name': 'MetaBase'},
            'added_timestamp': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            'genres': ('django.db.models.fields.related.ManyToManyField', [], {'to': "orm['metabase.Genre']", 'through': "orm['metabase.GenreLink']", 'symmetrical': 'False'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'media_type': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['metabase.MediaType']"}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '50'}),
            'source': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['metabase.MetaSource']"}),
            'unique_id': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '40'}),
            'year': ('django.db.models.fields.CharField', [], {'max_length': '4'})
        },
        'metabase.metasource': {
            'Meta': {'object_name': 'MetaSource'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '30'})
        }
    }

    complete_apps = ['metabase']
    symmetrical = True
