# -*- coding: utf-8 -*-
from south.utils import datetime_utils as datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models
from django.db.utils import OperationalError


class Migration(SchemaMigration):

    def forwards(self, orm):
        try:
            db.delete_index('metabase_metabase', ['in_library'])
        except OperationalError:
            # older DB versions may not have this...
            pass

        db.create_index('metabase_metabase', ['in_library', 'media_type_id'])

    def backwards(self, orm):
        db.delete_index('metabase_metabase', ['in_library', 'media_type_id'])

        db.create_index('metabase_metabase', ['in_library'])

    models = {
        'metabase.episode': {
            'Meta': {'object_name': 'Episode', '_ormbases': ['metabase.MetaBase']},
            'ep_name': ('django.db.models.fields.CharField', [], {'max_length': '50'}),
            'ep_num': ('django.db.models.fields.CharField', [], {'default': "''", 'max_length': '20', 'blank': 'True'}),
            'metabase_ptr': ('django.db.models.fields.related.OneToOneField', [], {'primary_key': 'True', 'unique': 'True', 'to': "orm['metabase.MetaBase']"}),
            'season': ('django.db.models.fields.CharField', [], {'default': "''", 'max_length': '20', 'blank': 'True'}),
            'series': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'episode_list'", 'to': "orm['metabase.MetaBase']"})
        },
        'metabase.genre': {
            'Meta': {'object_name': 'Genre'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '40'})
        },
        'metabase.genrelink': {
            'Meta': {'object_name': 'GenreLink', 'unique_together': "(('meta', 'genre'),)"},
            'genre': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['metabase.Genre']"}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'meta': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['metabase.MetaBase']"})
        },
        'metabase.mediatype': {
            'Meta': {'object_name': 'MediaType'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '30'}),
            'parent': ('django.db.models.fields.related.ForeignKey', [], {'null': 'True', 'default': 'None', 'related_name': "'children'", 'to': "orm['metabase.MediaType']"})
        },
        'metabase.metabase': {
            'Meta': {'object_name': 'MetaBase', 'unique_together': "(('source', 'name', 'year', 'media_type'),)"},
            'added_timestamp': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            'genres': ('django.db.models.fields.related.ManyToManyField', [], {'symmetrical': 'False', 'through': "orm['metabase.GenreLink']", 'to': "orm['metabase.Genre']"}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'in_library': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'media_type': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['metabase.MediaType']"}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '50'}),
            'other_id': ('django.db.models.fields.IntegerField', [], {}),
            'source': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['metabase.MetaSource']"}),
            'year': ('django.db.models.fields.CharField', [], {'max_length': '4'})
        },
        'metabase.metasource': {
            'Meta': {'object_name': 'MetaSource'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '30'})
        },
        'metabase.movie': {
            'Meta': {'object_name': 'Movie', '_ormbases': ['metabase.MetaBase']},
            'metabase_ptr': ('django.db.models.fields.related.OneToOneField', [], {'primary_key': 'True', 'unique': 'True', 'to': "orm['metabase.MetaBase']"})
        },
        'metabase.series': {
            'Meta': {'object_name': 'Series', '_ormbases': ['metabase.MetaBase']},
            'metabase_ptr': ('django.db.models.fields.related.OneToOneField', [], {'primary_key': 'True', 'unique': 'True', 'to': "orm['metabase.MetaBase']"})
        }
    }

    complete_apps = ['metabase']