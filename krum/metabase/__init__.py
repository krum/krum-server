import logging
import django.db as db

logger = logging.getLogger(__name__)

def reindex_metabase_items():
    '''This reindexes all the metabase info for quick text-based lookup.

    Note: this function has to drop and rebuild the entire table so it will take
    minutes on a full IMDB dataset and
    '''

    logger.info('Updating full text index')

    # because it's contentless, we can't delete
    # so we just drop and recreate
    cur = db.connection.cursor()

    cur.execute('DROP TABLE IF EXISTS metabase_fts')
    db.transaction.commit_unless_managed()
    # make new
    cur.execute('CREATE VIRTUAL TABLE metabase_fts USING fts4(content="", content)')
    db.transaction.commit_unless_managed()

    sql = '''
        select id, name || ' ' || year
        from metabase_metabase
    '''
    cur.execute('insert into metabase_fts (docid, content) {}'.format(sql))
    db.transaction.commit_unless_managed()
