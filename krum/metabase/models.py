from django.db import models
from django.db.backends.signals import connection_created

MB_MT_MOVIE = 1
MB_MT_SERIES = 2
MB_MT_EPISODE = 3

# make sure foreign keys are enforced
def activate_foreign_keys(sender, connection, **kwargs):
    """Enable integrity constraint with sqlite."""
    if connection.vendor == 'sqlite':
        cursor = connection.cursor()
        cursor.execute('PRAGMA foreign_keys = ON;')
connection_created.connect(activate_foreign_keys)

class MediaType(models.Model):
    name            = models.CharField(max_length=30, unique=True)
    parent          = models.ForeignKey('self', null=True, default=None, related_name='children')

class MetaSource(models.Model):
    name            = models.CharField(max_length=30, unique=True)

class Genre(models.Model):
    name            = models.CharField(max_length=40, unique=True)

# Seems pointless, but I want to know the name ahead of time to do mass insert queries
class GenreLink(models.Model):
    meta            = models.ForeignKey('MetaBase')
    genre           = models.ForeignKey(Genre)

    class Meta:
        unique_together = ('meta', 'genre')

class MetaBase(models.Model):
    name            = models.CharField(max_length=50)
    year            = models.CharField(max_length=4)
    media_type      = models.ForeignKey(MediaType)
    source          = models.ForeignKey(MetaSource)
    added_timestamp = models.DateTimeField(auto_now_add=True)
    other_id        = models.IntegerField()
    in_library      = models.BooleanField(default=False)
    genres          = models.ManyToManyField(Genre, through='GenreLink')

    class Meta:
        unique_together = ('source', 'name', 'year', 'media_type')

    def to_json_dict(self):
        return {'name': self.name, 'media_type': self.media_type, 'source': self.source}

    def add_to_library(self):
        if self.in_library:
            return

        self.in_library = True
        self.save()

        if self.media_type_id == MB_MT_SERIES:
            # add my episodes
            self.episode_list.all().update(in_library=True)
        elif self.media_type_id == MB_MT_EPISODE:
            # add my series
            self.episode.series.add_to_library()

class Episode(MetaBase):
    series          = models.ForeignKey(MetaBase, related_name='episode_list')
    season          = models.CharField(max_length=20, blank=True, default='')
    ep_num          = models.CharField(max_length=20, blank=True, default='')
    ep_name         = models.CharField(max_length=50)

class Movie(MetaBase):
    pass

class Series(MetaBase):
    pass
