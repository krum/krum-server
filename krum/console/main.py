'''
This provides a command line interface for krum
'''

import argparse
import sys
import os
import os.path as op
try:
    from setproctitle import setproctitle
except ImportError:
    def setproctitle(name):
        sys.argv[0] = 'krum'    # only windows doesn't support setproctitle, so we do this to make the command line Usage message accurate at least.
from krum import cfg

# TODO: move individual commands out to modules

# This allows you to manually manipulate the IMDB based metadata
def add_imdb(subparsers):
    """Add the 'imdb' command."""
    def do_imdb(args, nostart):
        """Run the 'imdb' command."""
        # NOTE: Must do here so that config path can be set
        import krum.imdb.update as ki
        ki.update_data(args.nodownload, args.redownload)

    p = subparsers.add_parser('imdb')
    group = p.add_mutually_exclusive_group()
    group.add_argument('--nodownload', action='store_false', help='Cannot be set with --redownload. Krum will not download IMDB files or updates. Krum will resync its internal DB with the raw IMDB files.')
    group.add_argument('--redownload', action='store_true', help='Cannot be set with --nodownload. Krum will completely re-download all IMDB raw files the re-sync the interal DB.')
    p.set_defaults(func=do_imdb)

# This is used to initially configure and update krum
def add_setup(subparsers):
    """Add the 'setup' command."""
    def do_setup(args, nostart):
        """Run the 'setup' command."""
        # NOTE: Must do here so that config path can be set
        from django.core import management

        # ensure directory is present
        if not op.exists(cfg['data_path']):
            os.makedirs(cfg['data_path'])

        management.call_command('syncdb')
        management.call_command('migrate')

    p = subparsers.add_parser('setup')
    p.set_defaults(func=do_setup)

# This lets you import files from the command line
def add_import(subparsers):
    """Add the 'import' command."""
    def do_import(args, nostart):
        """Run the 'import' command."""
        # NOTE: Must do here so that config path can be set
        import krum.content

        for f in args.files:
            krum.content.import_file(f, args.move)

    p = subparsers.add_parser('import')
    p.add_argument('files', nargs='+', help='One or more files to import in to Krum.')
    p.add_argument('--move', action='store_true', help='Just move the file into Krum rather than making a copy.')
    p.set_defaults(func=do_import)

# This let's you run Krum
def add_run(subparsers):
    """Add the 'run' command."""
    def do_run(args, nostart):
        """Run the 'run' command."""
        from krum.console.webserver import runserver

        return runserver(args, nostart)

    p = subparsers.add_parser('run')
    p.add_argument('--port', type=int, default=cfg['port'],
                   help='Sets the tcp port that Krum will run on.')
    p.set_defaults(func=do_run)

# This is to give a hook to the standard django manage command set
def add_manage(subparsers):
    """Add the django 'manage' command."""
    def do_manage(args, nostart):
        """Run the 'manage' command."""
        from django.core.management import execute_from_command_line

        execute_from_command_line(sys.argv[0:1] + args.remaining_args)

    p = subparsers.add_parser('manage')
    p.add_argument('remaining_args', nargs=argparse.REMAINDER)
    p.set_defaults(func=do_manage)

class set_config_action(argparse.Action):
    """Poopy hook so that this is set before anything else happens."""
    def __call__(self, p, n, v, o):
        os.environ.setdefault("KRUM_CONFIG_FILE", v)
        cfg.reload()

def main():
    """Entry point for CLI."""
    setproctitle('krum')

    do_main(sys.argv[1:])

def do_main(args, nostart=False):
    """Where the logic is, separate for testing."""
    parser = argparse.ArgumentParser(description='The command line tool for krum.')
    parser.add_argument('--config', '-c', action=set_config_action,
                        help='Provide a path to override the default config')
    parser.set_defaults(func=None)

    # add the commands
    subparsers = parser.add_subparsers(help='command help')
    mod_items = sys.modules[main.__module__].__dict__
    for c in [f for n,f in mod_items.items() if n.startswith('add_') and callable(f)]:
        c(subparsers)

    pargs = parser.parse_args(args)
    if pargs.func is None:
        parser.print_usage()
        exit(1)

    # parse and run
    return pargs.func(pargs, nostart=nostart)
