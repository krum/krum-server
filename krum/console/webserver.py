"""Defines the entry point for running the web server."""
import logging
from urllib.parse import urlunparse

import tornado.log
from tornado import web, gen
from tornado.httpserver import HTTPServer
from tornado.ioloop import IOLoop
from django.conf import settings

import krum.playback.session as ps
import krum.playback.krumsocket as ks
from krum import cfg

# /content handlers
from krum.content.streamer           import StreamingContentHandler, HashingHandler
from krum.api.version_1.content      import ContentListHandler, ContentHandler
# /autocomplete handler
from krum.api.version_1.autocomplete import AutocompleteHandler

# Generic Resource handlers
from krum.api.version_1.resourcehandlers import CollectionHandler, InstanceHandler
# /media resources
from krum.api.version_1.media        import (MediaResource, GenreResource,
                                             MediaTypeResource)

# local player
from krum.player.mplayerremoteplayer import MPlayerRemotePlayer

logger = logging.getLogger(__name__)

@gen.coroutine
def start_local_player(application, port, ssl=False):
    """Starts up a local instance of the MPlayer based remote player."""
    # create the player
    local_player = MPlayerRemotePlayer()
    try:
        # start it up
        scheme = ('ws','wss')[ssl==True]
        # NOTE: this assumes we've bound localhost (or 0.0.0.0)
        netloc = 'localhost:{}'.format(port)
        path = application.reverse_url('api.1.playback.player.register')
        register_url = urlunparse([scheme, netloc, path, '', '', ''])
        logger.info('Starting up MPlayerRemotePlayer on this server. Connecting to: {}'
                    .format(register_url))

        yield local_player.start(register_url)

    except Exception as e:
        logger.info('Got exception: {}'.format(e))

def runserver(args, nostart):
    """Run the web server (i.e. Krum) for ever."""
    # set up logging to std out as default
    logging.getLogger().setLevel(logging.DEBUG)
    tornado.log.enable_pretty_logging()

    content_data_path = r'/api/1/content/(\d+)/data'

    # this guy tracks all the Remote Players
    psargs = {'state': ps.PlaybackSet(content_path=content_data_path)}
    ksargs = {'delegate': psargs['state']}

    application = web.Application([
# content
        (r'/api/1/hasher',
            HashingHandler),
        (r'/api/1/content',
            ContentListHandler),
        (r'/api/1/content/(\d+)',
            ContentHandler, {},            'api.1.content.item'),
        (content_data_path,
            StreamingContentHandler, {},   'api.1.content.data'),

# media
        (r'/api/1/media',
            CollectionHandler, {'resource': MediaResource()}),
        (r'/api/1/media/(\d+)',
            InstanceHandler, {'resource': MediaResource()}, 'api.1.media.item'),
        (r'/api/1/genres',
            CollectionHandler, {'resource': GenreResource()}),
        (r'/api/1/genres/(\d+)',
            InstanceHandler, {'resource': GenreResource()}, 'api.1.genre.item'),
        (r'/api/1/media_types',
            CollectionHandler, {'resource': MediaTypeResource()}),
        (r'/api/1/media_types/(\d+)',
            InstanceHandler, {'resource': MediaTypeResource()}, 'api.1.media_type.item'),

# autocomplete
        (r'/api/1/autocomplete',
            AutocompleteHandler),

# playback
        (r'/api/1/playback/players',
            ps.RemotePlayerListHandler, psargs),
        (r'/api/1/playback/players/(\d+)',
            ps.RemotePlayerHandler,     psargs, 'api.1.playback.player.item'),
        (r'/api/1/playback/players/register',
            ks.KrumSocketHandler,       ksargs, 'api.1.playback.player.register'),
        (r'/api/1/playback/sessions',
            ps.SessionListHandler,      psargs),
        (r'/api/1/playback/sessions/(\d+)',
            ps.SessionHandler,          psargs, 'api.1.playback.session.item')
        ],
        debug=False,
        static_path=settings.STATIC_ROOT,
        static_url_prefix=settings.STATIC_URL
    )

    # has to start after the server has also started
    ioloop = IOLoop.instance()
    logger.debug('player_on_server == {}'.format(cfg['player_on_server']))
    if cfg['player_on_server'] == True:
        logger.info('Starting local player.')
        ioloop.add_callback(start_local_player, application, args.port, ssl=False)

    # this is to allow for wholesale testing of the webserver
    if nostart:
        # just hand back for testing
        logger.info("returning the app!")
        return application
    else:
        # run it forever
        # TODO: Make max_body_size configurable (currently 500GB)
        server = HTTPServer(application, max_body_size=500*pow(2,30))
        server.bind(args.port)
        server.start(1)

        ioloop.start()
