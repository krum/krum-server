from __future__ import absolute_import

import json
import ujson
from datetime import datetime

from django.db.models.query import QuerySet

from .helpers import default_tzinfo

JSON_MIMETYPE = 'application/json; charset=UTF-8'

class NonJsonMimetypeError(Exception):
    def __init__(self, mimetype):
        super(NonJsonMimetypeError, self).__init__(
            'Mimetype "{}" is not a valid JSON mimetype.'.format(mimetype))

def is_json(mimetype):
    '''Returns true if the given mimetype is valid for a JSON document'''
    return mimetype.startswith('application/json')

def from_json(json_string, mimetype=None):
    '''Deserialises the UTF8 JSON byte string *json_string* to a Python object.

    @param json_string: The byte string of json.
    @param mimetype: The mimetype of the bytestring. If present, this will be
    checked to ensure the mimetype is valid for JSON. If not, a
    NonJsonMimetypeError will be raised.
    @return The newly deserialised JSON object.
    '''
    if mimetype is not None and not is_json(mimetype):
        raise NonJsonMimetypeError()

    return ujson.loads(json_string)

def better_json_encoder(object_to_encode):
    if isinstance(object_to_encode, QuerySet):
        return [i for i in object_to_encode]
    elif hasattr(object_to_encode, 'to_json_dict'):
        return object_to_encode.to_json_dict()
    elif isinstance(object_to_encode, datetime):
        return default_tzinfo(object_to_encode).isoformat()
    else:
        raise TypeError("Unable to encode type of: " + repr(object_to_encode))

def to_json(object_):
    '''Serialises *object_* a UTF8 JSON byte string. Supports additional types
    beyond the defaults. These are:

    `django.db.models.query.QuerySet`: This just gets serialised like a list.
    `datetime.datetime`: Gets serialised as the full iso datetime string.

    Any object that has a 'to_json_dict' will also have that method called and
    the resulting value serialised in the objects place.

    Note: This serialiser will push out UTF8 rather than ascii only characters.
    '''
    return json.dumps(object_, ensure_ascii=False, default=better_json_encoder)
