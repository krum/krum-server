"""General helper functions and classes for Time, Futures and
Attribute fuddling."""

from datetime import tzinfo, timedelta, datetime
import time
from tornado.concurrent import Future
from tornado.ioloop import IOLoop

ZERO = timedelta(0)

# A UTC class. I hate pythons shitty avoidance of doing dates properly!
class UTC(tzinfo):
    '''A timezone class that represents the UTC timezone.'''

    def utcoffset(self, dt):
        return ZERO

    def tzname(self, dt):
        return "UTC"

    def dst(self, dt):
        return ZERO

    def __repr__(self):
        return 'UTC()'

UTC = UTC()
'''A timezone object for the UTC timezone'''

def now_datetime():
    '''Returns a UTC timezoned datetime object representing the current time'''
    return default_tzinfo(datetime.utcnow())

def default_tzinfo(date_obj):
    '''Sets a datetimes timezone to UTC if it doesn't already have one'''
    if not date_obj.tzinfo:
        date_obj = date_obj.replace(tzinfo=UTC)
    return date_obj

def extract_attrs(obj, attrs):
    '''
    Copys an objects attributes into a dictionary

    obj: The object from which to copy the attribues.
    attrs: A sequence of attribute names, i.e. strings.
    '''
    return dict([(a, getattr(obj, a)) for a in attrs])

def update_attributes(obj, source, whitelist, required=False):
    '''
    Updates the attributes of an object using a dictionary as the source.

    obj: The object to be updated.
    source: The dictonary from which the values should be taken.
    whitelist: A whitelist of attribute names to limit which attributes are altered.
    required: If True, all items listed in the whitelist must be present or a key error will be raised.
    '''

    for n in whitelist:
        try:
            v = source[n]
            setattr(obj, n, v)
        except KeyError:
            if required:
                raise

class TimeAndLog(object):
    """Use an a context manager. Measures time from enter to exit and
    logs using the given message.

    Args:
        logger {logging.Logger} - The logger to write the message to.
        msg {string} - msg.format(interval) is what gets logged, where
            interval is the number of seconds that elapsed within the
            context.
    """
    def __init__(self, logger, msg):
        self.start = None
        self.logger = logger
        self.msg = msg

    def __enter__(self):
        self.start = time.clock()
        return self

    def __exit__(self, *args):
        interval = time.clock() - self.start
        self.logger.debug(self.msg.format(interval))

def FutureWait(deadline, value, ioloop=None):
    '''Returns a future who's result will be set to value after timeout seconds.

    :param deadline: May be a number denoting a time (on the same
        scale as `IOLoop.time`, normally `time.time`), or a
        `datetime.timedelta` object for a deadline relative to the
        current time.
    :param value: Value to set the returned Future's result attribute to after the timeout.
    :return: A Future that will "complete" after timeout seconds.
    '''
    ioloop = ioloop or IOLoop.current()

    timeout_future = Future()
    ioloop.add_timeout(deadline, timeout_future.set_result, value)
    return timeout_future
