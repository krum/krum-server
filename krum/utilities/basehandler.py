
"""Module defines a handler that provides some handy shortcuts."""

from __future__ import absolute_import

from tornado.web import RequestHandler, HTTPError
from tornado.escape import utf8

from .json import from_json, to_json, JSON_MIMETYPE

CORS_RESP_METHODS = 'GET,PUT,POST,PATCH,DELETE,OPTIONS'
CORS_RESP_HEADERS = 'Content-Range, Content-Type'
CORS_EXPS_HEADERS = 'Content-Range, Location'

class BaseHandler(RequestHandler):
    """A tornado handler with some helpers."""

    def options(self, *args, **kwargs):
        """Answer Options requests. Hand back CORS headers."""
        pass

    def set_default_headers(self):
        # this means we're open for all requests
        self.set_header("Access-Control-Allow-Origin", "*")
        self.set_header("Access-Control-Allow-Methods", CORS_RESP_METHODS)
        self.set_header("Access-Control-Allow-Headers", CORS_RESP_HEADERS)
        self.set_header("Access-Control-Expose-Headers", CORS_EXPS_HEADERS)

    @property
    def json_body(self):
        """Return the request body as JSON."""

        # check the cache
        json_body = getattr(self, '_json_body', None)
        if json_body is None:
            json_body = from_json(self.request.body)
            self._json_body = json_body
        return json_body

    def write(self, chunk):

        """Write a chunk to be sent to the client.

        This is just like `tornado.web.RequestHandler.write` except it uses a
        JSON serialiser that knows how to serialise more types.

        Supported types are listed in the `krum.utilities.json.to_json` docs.
        """

        if self._finished:
            raise RuntimeError("Cannot write() after finish(). May be caused "
                               "by using async operations without the "
                               "@asynchronous decorator.")

        # convert it either to JSON
        if isinstance(chunk, dict) or hasattr(chunk, 'to_json_dict'):
            chunk = to_json(chunk)
            self.set_header('Content-Type', JSON_MIMETYPE)
        # or UTF8
        chunk = utf8(chunk)

        # add it to the write queue
        self._write_buffer.append(chunk)
