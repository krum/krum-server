from os import path, environ
from pkg_resources import resource_string
import yaml

environ["DJANGO_SETTINGS_MODULE"] = "krum.settings"

class InvalidConfigFile(Exception):
    '''Raised when a non-yaml file is provided'''
    pass

def merge_tree(base, update):
    '''Recursively merges 2 dictionaries. Will replace
    non-dictionary values in 'base' with those in 'update'.
    Merges dictionary values by adding new kvps from 'update'.
    '''
    for k,v in update.items():
        if k in base and isinstance(v, dict) and isinstance(base[k], dict):
            merge_tree(base[k], v)
        else:
            base[k] = v

class Config(dict):
    _instance = None
    _initialized = None
    def __new__(cls, *args, **kwargs):
        if(not cls._instance or kwargs.get('reload')):
            cls._instance = dict.__new__(cls, *args,**kwargs)
            cls._instance.clear()
            cls._initialized = False
        return cls._instance

    def __init__(self, reload=False):
        if not self._initialized:
            self.reload()

    def reload(self):
        '''Allows the currently loaded config to be reloaded. The config file
        is chosen as follows:
         1. The value of the KRUM_CONFIG_FILE environment variable
         2. /etc/krum/config.yml
         3. ~/.krum/config.yml
        '''

        # start with krum defaults
        self.update(self.default)

        # environ overrides any others if present
        if(environ.get('KRUM_CONFIG_FILE')):
            paths = [environ.get('KRUM_CONFIG_FILE')]
            manual_override = True
        else:
            manual_override = False
            paths = ['/etc/krum/config.yml', path.expanduser('~/.krum/config.yml')]

        # read in order and merge in updates
        self['config_files'] = []
        for f in paths:
            invalid_config_error = InvalidConfigFile('Cannot open config file: {}'.format(f))
            try:
                try:
                    with open(f) as open_config:
                        d = yaml.load(open_config)
                    merge_tree(self, self._absolutize_paths(d))
                    self['config_files'].append(f)
                except IOError:
                    if manual_override:
                        raise invalid_config_error
                    raise
            except yaml.parser.ParserError as e:
                raise invalid_config_error
            except IOError:
                pass

        self._initialized = True

    def _absolutize_paths(self, config):
        for k,v in config.items():
            if k.endswith('_path'):
                config[k] = path.abspath(v)
        return config

    @property
    def default(self):
        return yaml.load(resource_string(__name__, 'config/default.yml'))

cfg = Config()
