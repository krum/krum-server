"""The MPlayer based Remote player."""

import errno
import logging
import os.path as op
import uuid

from tornado import gen
from tornado.ioloop import IOLoop, PeriodicCallback

# and the krum stuff
from krum import cfg
from krum.utilities.helpers import FutureWait

from .player_base import RemotePlayerProtocolV1, RemotePlayerProtocolV1Delegate
from .mplayer import MPlayerController, MPlayerAnswerError, MPlayerNoAnswerError

logger = logging.getLogger(__name__)

MPLAYER_REMOTE_PLAYER_TYPE = 'Mplayer'
KRUM_NOTIFY_DELAY = 5       # number of seconds between position updates to Krum

class MPlayerRemotePlayer(RemotePlayerProtocolV1Delegate):
    '''This Remote Player runs an external mplayer process (in slave mode)
    and issues commands to it to match the requested.

    As mplayer has no event mechanism, this controller also polls mplayer every
    second to get the playback position. This means that there can be up to 1
    second of delay between playlist items.
    '''

    def __init__(self, io_loop=None):
        super(MPlayerRemotePlayer, self).__init__()

        self.io_loop = io_loop or IOLoop.current()
        self._controller = None
        self._protocol = None

        # this is for the periodic checks to mplayer and periodic notifications to Krum
        self._krum_next_notify_time = 0
        self._timer = None

        # playback info
        self._guid = None
        self._playback = {
            'paused': True,
            'volume': 1.0,
            'position': None,
            'duration': None,
            'playspeed': 1.0,
            'playlist': []
        }

    @gen.coroutine
    def start(self, krum_url):
        '''Starts up MPlayer and returns a Future who's value will be set once
        MPlayer is up and running. And Krum has told us we're registered.
        '''

        # this actually starts things up
        self._controller = MPlayerController()

        yield self._controller.start()

        # set up the periodic check
        self._krum_next_notify_time = 0
        self._timer = PeriodicCallback(self.on_timer, 1000, self.io_loop)
        self._timer.start()

        # now connect up to Krum
        self._protocol = RemotePlayerProtocolV1(self, self.io_loop)
        yield self._protocol.register_to(krum_url, MPLAYER_REMOTE_PLAYER_TYPE)

    @gen.coroutine
    def stop(self):
        """Cleans up timers, connection, mplayer."""

        if self._timer is not None:
            self._timer.stop()
            self._timer = None
        else:
            # no timer == stopping or stopped
            return

        yield self._protocol.async_close()
        yield self._controller.quit()

        self._controller = None
        self._protocol = None

    def get_player_guid(self, protocol):
        '''This simply looks up the guid that is stored in
            cfg['data_path']/mplayer_config.txt
        It's done this way because the krum config file may not be
        writable by this process, but the data_path folder has to be
        writable or krum can't run at all.

        Args
            protocol - The RemotePlayerProtocolV1 for which this
                object is acting as delegate.

        Returns
            A hex encoded GUID string that can be used to uniquely
            identify this remote player on the Krum server.
        '''
        if self._guid is not None:
            return self._guid

        # try to read the file
        guid_file_path = op.join(cfg['data_path'], 'mplayer_guid.txt')
        logger.info('Looking for mplayer_guid in "{}"'.format(guid_file_path))
        try:
            with open(guid_file_path) as guid_file:
                # done this way to parse and normalise
                self._guid = uuid.UUID(guid_file.read()).hex
        except IOError as e:
            # it doesn't exist so create it
            if e.errno == errno.ENOENT:
                with open(guid_file_path, 'w') as guid_file:
                    self._guid = uuid.uuid4().hex
                    guid_file.write(self._guid)
            else:
                raise

        return self._guid

    @gen.coroutine
    def get_playback_data(self, protocol):
        '''Krum wants to know our status. Just get position and paused status
        as the others are all tracked reasonably well and don't change often.

        Returns
            A future who's value will be set to a dictionary ready for
            returning to Krum.
        '''
        paused = yield self._controller.get_prop('paused')
        pos = yield self._controller.get_prop('position')

        self._playback['paused'] = paused
        self._playback['position'] = pos

        return self._playback

    @gen.coroutine
    def play_item(self, new_item):
        '''This will change Mplayer over to playing the provided playlist item.
        This will interrupt playback of the currently playing item.
        '''
        if new_item is None:
            yield self._controller.stop_playback()
        else:
            yield self._controller.play_file(new_item['data_url'],
                                             self._playback['paused'])

            # this only works if the video is not paused
            try:
                yield self._controller.set_prop('volume', self._playback['volume'], True)
            except KeyboardInterrupt:
                raise
            except:
                pass

    @gen.coroutine
    def on_change_playback_data(self, the_patch, protocol):
        '''Krum is asking that we change our status.

        Update order is:
            - playlist
            - playspeed
            - volume
            - position
            - paused

        This allows you to change any/all values and it make sense.

        See `RemotePlayerProtocolV1Delegate` for descriptions of the parameters
        and return type.
        '''
        filename = yield self._controller.get_prop('filename')
        playing = filename is not None
        logger.debug('Changing playback data to: "%s", playing: %s', the_patch, playing)

        # do paused first to make sure videos start in the correct state
        prop_name = 'paused'
        if prop_name in the_patch:
            want_val = the_patch[prop_name]

            # if not playing then always paused, but will unpause on play
            if playing:
                logger.debug('Setting %s to %s', prop_name, want_val)
                yield self._controller.set_prop(prop_name, want_val, True)

            self._playback[prop_name] = want_val

        # do playlist first, because we're faking them...
        new_pl = the_patch.get('playlist')
        if new_pl is not None:
            new_item = len(new_pl) and new_pl[0] or None  # less verbose than if len [0] else None
            curr_pl = self._playback['playlist']
            curr_item = len(curr_pl) and curr_pl[0] or None

            logger.debug('Old item {}, new item {}'.format(curr_item, new_item))

            # the 0th track has changed, tell mplayer
            if (new_item and new_item['data_url']) != (curr_item and curr_item['data_url']):
                logger.debug('Starting request to play item: {}'.format(new_item))
                yield self.play_item(new_item)
                logger.debug('Finished request to play item: {}'.format(new_item))

            self._playback['playlist'] = new_pl

        # now do all the property style gear, in order
        for prop_name in ['playspeed', 'volume', 'position']:
            if prop_name not in the_patch:
                continue

            # tell Mplayer to change
            val = the_patch[prop_name]
            yield self._controller.set_prop(prop_name, val, False)

            # read back from MPlayer to confirm
            self._playback[prop_name] = val

    def on_register(self, protocol):
        '''This will take the registered name and display it :)'''
        pass

    def on_open(self, protocol):
        '''Nothing really to do here, could display update to user...'''
        pass

    def on_close(self, protocol):
        '''Guess we'll keep on playing...'''
        self._controller.stop_playback()

    @gen.coroutine
    def on_timer(self):
        '''This is here to allow periodic polling of mplayer to find out its
        playback position and enqueue new files.

        When MPlayer has told us the currrent playback position, we update our
        status, report back to Krum and possibly change playlist items if
        necessary.
        '''

        # if we're not playing, just skip over
        if self._playback['playlist'] == []:
            return

        # get the playback position
        position = yield self._controller.get_prop('position')
        if position is None:
            # this means playback has finished
            # move to next file
            new_pl = self._playback['playlist'][1:]

            try:
                next_item = new_pl[0]
                self.play_item(next_item)
                logger.debug('Finished top playlist item, moving to next')
            except IndexError:
                logger.debug('Finished entire playlist, will wait')

            # tell krum the playlist has changed
            self._playback['playlist'] = new_pl
            self._playback['position'] = 0
            self._protocol.send_event('PATCH', '/playback', {
                'playlist': new_pl,
                'position': 0
            })

        # we only update Krum on our playback position every 5 seconds
        elif self.io_loop.time() >= self._krum_next_notify_time:
            # set up the next notification time
            self._krum_next_notify_time = self.io_loop.time() + KRUM_NOTIFY_DELAY

            krum_update = {
                'position': position
            }

            # Note: This is done here because Mplayer can take a while
            # to start playing during which it will discard requests
            # If we don't know it already, ask for the playback
            # duration
            curr_item = self._playback['playlist'][0]
            if self._playback.get('duration') is None:
                try:
                    duration = yield self._controller.get_prop('duration')
                    self._playback['duration'] = duration
                    krum_update['duration'] = duration
                except (MPlayerNoAnswerError, MPlayerAnswerError):
                    logger.warn('Could not get duration of playlist item %s', curr_item)

            # report the new position
            self._protocol.send_event('PATCH', '/playback', krum_update)
            logger.debug('Periodic position update sent to krum.')
