"""Defines a handy async class for controlling an MPlayer instance."""

# standard guff
import logging
import os
import atexit
from collections import deque

# tornado libs
from tornado import gen
from tornado.process import Subprocess
from tornado.concurrent import Future
from tornado.ioloop import IOLoop

from krum.utilities.helpers import FutureWait

logger = logging.getLogger(__name__)
msg_logger = logging.getLogger(__name__ + '.messages')
raw_logger = logging.getLogger(__name__ + '.messages.raw')

MPLAYER_RETRY_DELAY = 0.2

class MPlayerAnswerError(Exception):
    '''This exception represents an error where Mplayer answered but with
    an error condition.'''

    def __init__(self, args, error_message):
        super(MPlayerAnswerError, self).__init__()

        self.args = args
        self.error_message = error_message

    def __str__(self):
        return ('Got an Error answer "{}" from MPlayer when running command: {}'
               .format(self.error_message, self.args))

class MPlayerNoAnswerError(Exception):
    """Mplayer did no respond in time.

    Args
        args: The command and arguments that were given to MPlayer.
        timeout: The duration waited for before this error was raised.
    """

    def __init__(self, args, timeout):
        super(MPlayerNoAnswerError, self).__init__()

        self.args = args
        self.timeout = timeout

    def __str__(self):
        """String version baby!"""
        return ('No answer from MPlayer within {} seconds when running command: {}'
                .format(self.timeout, self.args))

class MPlayerController(object):
    '''Allows control of an external MPlayer process that it starts
    and keeps running.

    Commands such as play/pause/etc are turned into Mplayer slave commands and
    the answers waited for and parsed.

    Note: Because MPlayer can reply out-of-order, commands are only
    sent to MPlayer one-at-a-time. If there are multiple, the answer
    to the first will need to come back before the second is even sent
    to MPlayer.
    '''

    def __init__(self, io_loop=None):
        self.io_loop = io_loop or IOLoop.current()

        self._mplayer = None
        # (future, ans, command) of command we are currently waiting
        # for MPlayer to respond to.
        self._command_waiting_for = None
        # The tornado IOLoop timeout handle
        self._command_timeout = None
        # deque of (future, ans, command, timeout) tuples. The list of commands
        # waiting to be sent to MPlayer
        self._command_queue = deque()
        # unparsed chunks of message data received from MPlayer
        self._message_data = []

    def start(self):
        """Starts mplayer process. Also registers the stop method to
        run atexit so that the Mplayer will be correctly shut down
        however Python exits.

        Returns
            A Future who's value will be set once mplayer has finished
            starting and is ready to play media.
        """

        STREAM = Subprocess.STREAM
        DEVNULL = open(os.devnull, 'wb')    # just so we have somewhere to dump stderr...

        # startup mplayer
        args = ['mplayer', '-slave', '-idle', '-quiet', '-prefer-ipv4', '-noar',
                '-nomouseinput', '-nojoystick', '-nolirc']
        self._mplayer = Subprocess(args, stdin=STREAM, stdout=STREAM,
                                   stderr=DEVNULL, close_fds=True)
        self._mplayer.set_exit_callback(self._on_mplayer_exit)
        self._mplayer.stdout.read_until_close(self._on_mplayer_stdout_closed, self._on_mplayer_data)

        # set it up so that this mplayer is cleaned up post python exit
        atexit.register(self.quit)

        # pop the needed info in the command queue to wait for the mplayer started message.
        future = Future()
        self._command_waiting_for = (future, 'MPlayer ', 'special_mplayer_startup', 10)
        self._command_timeout = self.io_loop.call_later(
            10, self._timeout_command, self._command_waiting_for)
        return future

    def quit(self):
        '''Sends a SIGTERM to the mplayer and returns a future who's result is
        set to the exit code of the mplayer process when it terminates.
        '''
        # remove us from atexit
        atexit.unregister(self.quit)

        if self._mplayer is None:
            # then this has already been done and we're okay :)
            return

        # clear our state
        self._command_waiting_for = None
        self._command_queue = deque()
        self._message_data = []

        # tell mplayer to shut down
        fut = Future()
        def on_stop(exit_code):
            """Resolve the future."""
            fut.set_result(exit_code)

        self._mplayer.set_exit_callback(on_stop)
        logger.info('Asking MPlayer to quit')
        self.do_cmd(['quit'])
        self._mplayer = None
        return fut

    def do_cmd(self, args, answer_phrase=None, timeout=0.2):
        """Executes a command on mplayer and returns a future that will be set
        when the command completes. The text after the expected answer_phrase
        will be set as the result of the future.

        Args
            args - A list of the command and then it's arguments to be
                run.
            answer_phrase - The expected answer phrase that will be
                used to match the result of this command.
            timeout - This is to ensure we flush commands out that
                MPlayer has decided to ignore/forget/etc.
        Returns
            None if no answer_phrase was provided otherwise returns a
            Future that will resolve as described above.
        """

        # TODO: Need to implement timeouts because of out-of-order
        # responses. We only submit one-at-a-time, so if we miss one,
        # the whole thing stalls.

        # just add it to the queue of pending commands
        future = Future()
        msg_logger.debug('PENDING MPLAYER added command to queue: {}'.format(args))
        self._command_queue.append((future, answer_phrase, args, timeout))

        # send, if we can
        self._send_next_command()

        # return the future
        return future

    def _send_next_command(self):
        """
        Sends off the front-most pending command to MPlayer.

        Is a null-op if there is already a command waiting on MPlayer
        or if there are no commands left in the queue.
        """
        if self._command_waiting_for is not None:
            return

        # get the next command
        try:
            command = self._command_queue.popleft()
        except IndexError:
            # no commands waiting, so bail
            return

        future, answer_phrase, args, timeout = command

        # build up the raw command string for MPlayer
        cmdbytes = (' '.join([x.replace(' ', '\\ ') for x in args] + ['\n'])).encode('ascii')
        self._mplayer.stdin.write(cmdbytes)
        raw_logger.info('sent: %s', cmdbytes)
        msg_logger.debug('START MPLAYER command: %s', args)

        # if it's a non-answered request, then we're done
        if answer_phrase is None:
            msg_logger.debug('SUCCESS MPLAYER command: {}'.format(args))
            self.io_loop.add_callback(future.set_result, None)
            self._command_waiting_for = None
        else:
            self._command_waiting_for = command
            self._command_timeout = self.io_loop.call_later(
                timeout, self._timeout_command, command)

    def _timeout_command(self, command):
        """
        Times out the given command.

        Is scheduled by _send_next_command and cancelled by
        _parse_messages.
        """
        if self._command_waiting_for != command:
            logger.error('Got timeout for different command than waiting for {} != {}'
                         .format(self._command_waiting_for, command))
            return

        # grab it
        future, _, args, timeout = command

        # expire it
        msg_logger.debug('TIMEOUT MPLAYER command: {}'.format(args))
        future.set_exception(MPlayerNoAnswerError(args, timeout))

        # flush it
        self._command_waiting_for = None
        self._command_timeout = None

        # send next command
        self._send_next_command()

    def _parse_messages(self, message):
        """
        Take a single message from MPlayer and see if it's the answer
        we were looking for. If it's an error or the answer, resolve
        the command.

        We only send one request at a time to avoid MPlayer responding
        out-of-order.
        """
        if self._command_waiting_for is None:
            msg_logger.debug('Got message when not waiting for one: "{}"'.format(message))
            return

        future, answer_phrase, args, _ = self._command_waiting_for

        # if it's what we want
        is_error = message.startswith('ANS_ERROR')
        is_answer = message.startswith(answer_phrase)
        if is_error or is_answer:
            msg_logger.debug('FINISH MPLAYER command: {}'.format(args))
            self._command_waiting_for = None
            self.io_loop.remove_timeout(self._command_timeout)
            self._command_timeout = None
            self._send_next_command()
            if is_error:
                msg_logger.debug('ERROR MPLAYER command: {}. {}'.format(args, message))
                future.set_exception(MPlayerAnswerError(args, message[10:]))
            else:
                msg_logger.debug('SUCCESS MPLAYER command: {}'.format(args))
                future.set_result(message[len(answer_phrase)+1:])
        else:
            msg_logger.debug('Discarding unexpected MPlayer message: {}'.format(message))

    def _on_mplayer_data(self, data):
        """
        Add data to the already collected and break into messages for parsing.
        """
        raw_logger.info('recv: %s', data)

        # are we at a spot where we can parse it?
        while b'\n' in data:
            # stick everything up to the \n into one string
            tail, data = data.split(b'\n', 1)
            message = b''.join(self._message_data) + tail
            # ignore is used because sometime's mplayer can pass back
            # rubbish values for prop answers, so the "ANS_propname="
            # bit is correct but the value is shit. We still want to
            # parse the message though because otherwise the whole
            # queue will wait 10 seconds for no reason.
            self._parse_messages(message.decode('ascii', 'ignore'))

            self._message_data = []

        # save the remainder
        if data != '':
            self._message_data.append(data)

    def play_file(self, path, keep_pause):
        """Changes the file that MPlayer is playing, replacing what it's
        playing if there is already a file present.

        Args
            path -  The path for Mplayer to play (can be a url or such).
        Returns
            A future that will resolve (to None) once the file has
            started playing.
        """
        logger.info('Playing file: "%s"', path)
        cmd = ['loadfile', '{}'.format(path)]

        # stay paused if we should already be paused
        if keep_pause:
            cmd = ['pausing_keep'] + cmd

        return self.do_cmd(cmd, 'Starting playback...', 10.0)

    def stop_playback(self):
        """Stop Mplayer playing whatever it is playing"""
        return self.do_cmd(['stop'])

    @gen.coroutine
    def _cmd_get_prop(self, prop_name):
        """Util function for getting the values of Mplayer properties."""

        for i, last in enumerate((False,False,True)):       # try 3 times
            try:
                val = yield self.do_cmd(['pausing_keep_force', 'get_property',
                                        prop_name], 'ANS_{}'.format(prop_name))
                msg_logger.debug('Got prop "%s" value from mplayer: %s',
                                 prop_name, val)
                return val
            except MPlayerAnswerError:
                msg_logger.debug('Got no value for prop "%s" from mplayer', prop_name)
                return None
            except MPlayerNoAnswerError:
                if last:
                    raise

            msg_logger.debug('Had to loop when fetching prop %s', prop_name)
            # wait retry count * delay
            yield FutureWait(self.io_loop.time() + MPLAYER_RETRY_DELAY*(i+1),
                             None, self.io_loop)

    @gen.coroutine
    def _cmd_set_prop(self, prop_name, value, confirm):
        """Util function for setting the values of Mplayer properties."""

        for i, last in enumerate((False,False,True)):       # try 3 times
            try:
                msg_logger.debug('Setting prop "%s" to value %s', prop_name, value)
                cmd = ['pausing_keep_force', 'set_property', prop_name, str(value)]
                yield self.do_cmd(cmd)
            except MPlayerAnswerError:
                return None
            except MPlayerNoAnswerError:
                if last:
                    raise

            if not confirm:
                return

            msg_logger.debug('Confirming new value for prop "%s"', prop_name)
            actual = self._cmd_get_prop(prop_name)
            if actual is None or actual == value:
                return actual
            elif last:
                raise MPlayerAnswerError(cmd, 'Could not set property!')

            msg_logger.debug('Had to loop when setting prop %s', prop_name)

            # wait retry count * delay
            yield FutureWait(self.io_loop.time() + MPLAYER_RETRY_DELAY*(i+1),
                             None, self.io_loop)

    @gen.coroutine
    def set_prop(self, prop_name, value, confirm):
        """Allows setting playback properties (except playlist)
        of Krum. They are normalised to Mplayers ranges/formats/etc.

        Args
            prop_name - The name of the krum player property to change.
            value - The value to which this property should be set.
            confirm - Should we confirm the value with MPlayer

        Returns
            A future that will resolve to None once mplayer has
            confirmed the property is set.
        """

        if prop_name == 'paused':
            # paused is special because you can't set it, only toggle
            cmd = ['pause']
            cp = yield self.get_prop('paused')
            for i in (False, False, True):
                if cp == value:
                    break
                else:
                    yield self.do_cmd(cmd)

                if not confirm:
                    break

                # now check it
                cp = yield self.get_prop('paused')
                if i == True and cp != value:
                    raise MPlayerAnswerError(cmd, 'Could not set paused!')

            return

        elif prop_name == 'position':
            mprop_name = 'time_pos'

            # is it a relative pos?
            if type(value) == str and value[0] in ('+','-'):
                curr_pos = yield self.get_prop('position')
                mprop_val = curr_pos + float(value)
            else:
                mprop_val = float(value)

        elif prop_name == 'volume':
            if value < 0 or value > 1.0:
                raise ValueError('volume must be between [0.0,1.0], {} is not'.format(value))

            # normalise from krums 0.0-1.0 to mplayers 0-100
            mprop_name = 'volume'
            mprop_val = value*100.0

        elif prop_name == 'playspeed':
            if value < 0.01 or value > 100:
                raise ValueError('playspeed must be in range [0.01,100], {} is not'.format(value))

            mprop_name = 'speed'
            mprop_val = value

        yield self._cmd_set_prop(mprop_name, mprop_val, confirm)

    @gen.coroutine
    def get_prop(self, prop_name):
        """Queries the different playback properties (except playlist)
        that Krum tracks and normalises them to Krums ranges/formats/etc.

        Args
            prop_name - The name of the krum player property to query.

        Returns
            A future that will resolve (with the result) once mplayer
            has provided the requested property.
        """

        # default to just handing back the string value
        parse_val = lambda x: x

        # set up the prop name and a function to parse the repsonse
        if prop_name == 'paused':
            paused = yield self._cmd_get_prop('pause')
            return paused.lower() == 'yes'

        elif prop_name == 'position':
            mprop_name = 'time_pos'
            parse_val = float
        elif prop_name == 'duration':
            mprop_name = 'length'
            parse_val = float
        elif prop_name == 'volume':
            mprop_name = 'volume'
            parse_val = lambda x: float(x)/100.0
        elif prop_name == 'playspeed':
            mprop_name = 'speed'
            parse_val = float
        elif prop_name == 'filename':
            mprop_name = 'path'

            # magic "(null)" value means no file
            parse_val = lambda x: (x, None)[x=='(null)']
        else:
            raise ValueError('Unknown property {}'.format(prop_name))

        str_val = yield self._cmd_get_prop(mprop_name)
        if str_val is None:
            return None
        else:
            return parse_val(str_val)

    def _on_mplayer_stdout_closed(self, data):
        """Not much to do here, likely we'll see MPlayer exit."""
        pass

    def _on_mplayer_exit(self, exit_code):
        """Mplayer exited, we should re-start it..."""
        # TODO: Restart or bubble up for enclosing controller to restart
        logger.info('Mplayer closed with code {}!!'.format(exit_code))
