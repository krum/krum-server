"""
Defines the RemotePlayerProtocol class. This class implements the
network level handling for a Remote Player.
"""
import logging

# tornado libs
from tornado import gen
from tornado.concurrent import Future, is_future
from tornado.ioloop import IOLoop

import krum.playback.krumsocket as krumsocket

logger = logging.getLogger(__name__)

'''
This module defines some handy classes for writing a RemotePlayer, namely it
defines `RemotePlayerProtocolV1` and `RemotePlayerProtocolV1Delegate`.

`RemotePlayerProtocolV1` provides all the machinery for connecting to Krum,
registering as a remote player and calls methods on it's delegate for the
actual playback work.

`RemotePlayerProtocolV1Delegate` is the delegate interface that
`RemotePlayerProtocolV1` expects it delegates to implement. For example
`MPlayerRemotePlayer` implements this interface and uses
`RemotePlayerProtocolV1` to do the common websocket and request/response guff
that Krum requires of all RemotePlayers.

A brief description of RemotePlayers follows:

 * Commands from Krum
  * GET/PATCH /meta
  * GET/PATCH /playback
 * Events to krum
  * PATCH to session_url

Websocket states
 * connecting
 * idle -> (reconnecting, disconnecting, playing)
 * reconnecting
 * playing

External player states
 * playing
 * idle

Generic remote player startup sequence:
 # load up our GUID, client type
 # create external player
 # make sure it's running
 # connect to Krum
  * respond to any requests in the mean-time
 # when we get PATCH'd "registered URL"
  * save it, and the name so we know what we're called
'''

class RemotePlayerProtocolV1Delegate(object):
    '''This is a simple class that outlines the methods that the RemotePlayerProtocol
    will call of it's delegates and their expected semantics.
    '''
    def get_player_guid(self, protocol):
        raise NotImplementedError()

    def get_playback_data(self, protocol):
        """Can return a future that resolves to a /playback dictionary. This
        will be returned to Krum.

        Args
            protocol - The protocole object that received this request.
        Returns
            A playback dictionary, or a future that resolves to one.
        """
        raise NotImplementedError()

    def on_change_playback_data(self, the_patch, protocol):
        """Can return a Future that resolves to None once the necessary
        changes have been made to the actual player.

        Args:
            the_patch - This is the patch dict sent by Krum.
            protocol - The protocole object that received this request.

        Returns
            None or a future that resolves to None.
        """
        raise NotImplementedError()

    def on_register(self, protocol):
        '''Just letting you know. Do what you will'''
        pass

    def on_close(self, protocol):
        '''Just letting you know. Do what you will'''
        pass

class RemotePlayerProtocolV1(object):
    '''This class provides low-level KrumSocket connectivity and message abstraction
    for Krum Remote Players to use to avoid boiler-plate code for each Player type.
    '''
    def __init__(self, delegate, ioloop=None):
        if delegate == None:
            raise ValueError('delegate must be non-None.')

        self.ioloop = ioloop or IOLoop.current()

        self._delegate = delegate
        self._meta = {
                'version': 1,
                'supported_versions': [1],
                'guid': delegate.get_player_guid(self),
                'client_type': 'Unknown Player',
                'registered_path': None,
                'registered_name': None,
                'isvisible': True
        }

        self._registered = False
        self._registered_future = None
        self._close_future = None
        self._socket = None

    def register_to(self, url, player_type='Unkown Player', connect_func=krumsocket.krumsocket_connect):
        '''Returns a future which will complete once the connection has been
        made and krum has confirmed successful registration of the player.

        :param url: The Krum URL to connect to for Remote Player registration.
        :param connect_func: A function that connects up a KrumSocket. See
        `krumsocket.krumsocket_connect` for the default implementation. This
        parameter is here to allow testing with fake KrumSockets.
        :return: A future who's value will be set once registration is complete.
        '''
        if self._registered_future is not None or self._socket is not None:
            msg = 'Asked to open when already opening, {}'.format(url)
            logger.error(msg)
            raise ValueError(msg)

        self._meta['client_type'] = player_type

        logger.debug('Opening RemotePlayerProtocol connection to "{}"'.format(url))
        self._registered_future = Future()
        def on_connect(socket):
            logger.debug('KrumSocket connected.')
            self._socket = socket
            self._socket.read_until_closed()

        connect_func(url, self, callback=on_connect)
        return self._registered_future

    def _register_complete(self):
        '''This is called by on_request once Krum has sent through the PATCH
        that provides the registered name and URL for this player, indicating
        that this player is now registered to Krum.
        '''

        self._registered = True
        self._registered_future.set_result(self)
        self._registered_future = None
        self._delegate.on_register(self)

    def async_close(self):
        '''Call this to request a closure of the socket.

        :return: Returns a future who's result will be set to this protocol
        object once the connection is confirmed closed.

        *Note:* This protocol delegate's on_close method is always called, so
        don't schedule the ioloop to call on_close of the delegate (or a method
        is clalls) or your method will end up being called twice.
        '''
        if self._close_future is not None:
            raise ValueError('Already trying to close.')

        self._close_future = Future()
        self._socket.close()
        return self._close_future

    def on_open(self, socket):
        """The socket is open. Save it and tell our delegate."""
        logger.debug('RemotePlayerProtocol Got OPEN: {}'.format(socket))
        self._socket = socket

        # tell the delegate about it
        self._delegate.on_open(self)

    def on_close(self, socket):
        """The socket closed, clean up and tell the delegate."""
        if self._close_future is not None:
            self._close_future.set_result(self)
            self._close_future = None

        # tell the delegate about it
        self._delegate.on_close(self)
        self._socket = None
        self._registered = False
        self._registered_future = None

    def send_event(self, method, path, body):
        '''This is called to send through an event to Krum, e.g. for position
        updates
        '''
        if self._socket is None:
            raise ValueError('Cannot sent event, socket is closed.')

        self._socket.send_event(method, path, body)

    def on_event(self, socket, event):
        '''This is a stub. Events from Krum to the Remote Player aren't yet
        part of the protocol.'''
        pass

    @gen.coroutine
    def on_request(self, socket, request):
        '''This handles incoming requests from Krum. It just validates that they're
        for either the /meta or /playback path and only GET or PATCH requests.

        It will call the on_meta_request or on_playback_request method of the delegate
        which must return a correct value, see the
        '''
        logger.debug('RemotePlayerProtocol request received: "{}"'.format(request))

        # check it's a support HTTP method
        if request.method not in ('GET', 'PATCH'):
            self._socket.send_response(405, request.id, {'message': 'Only GET and PATCH supported'})
            return
        # check it's a legit path
        if request.path not in ('/meta', '/playback'):
            self._socket.send_response(404, request.id, {'message': 'Path "{}" not found.'.format(request.path)})
            return

        # send/update the meta
        if request.path == '/meta':
            if request.method == 'GET':
                self._socket.send_response(200, request.id, self._meta)
            else:
                # TODO: This should check and make sure only writable attributes are changed
                self._meta.update(request.body)
                self._socket.send_response(204, request.id)

                # are we registered (only the first time counts :)
                if self._meta['registered_path'] is not None and self._registered_future is not None:
                    logger.debug('RemotePlayerProtocol: Registration process completed, path: "{}"'.format(self._meta['registered_path']))
                    self._register_complete()
        else:
            # it must be on /playback
            if request.method == 'GET':
                logger.debug('START Request for playback data')
                yield self.do_playback_get(request)
            else:
                logger.debug('START update playback data to "{}"'.format(request.body))
                yield self.do_playback_post(request)

    @gen.coroutine
    def do_playback_get(self, request):
        """
        Krum has asked for the current playback status, get it.
        """
        try:
            result = self._delegate.get_playback_data(self)
            if is_future(result):
                result = yield result

            self._socket.send_response(200, request.id, result)
            logger.debug('SUCCESS Request for playback data')
        except Exception as e:
            self._socket.send_response(500, request.id, {'message': str(e)})
            logger.exception('FAILED Request for playback data. {}'.format(str(e)))

    @gen.coroutine
    def do_playback_post(self, request):
        """
        Krum has asked that the current playback state be changed.
        """
        try:
            result = self._delegate.on_change_playback_data(request.body, self)
            if is_future(result):
                result = yield result
            if result is not None:
                raise TypeError("Expected on_change_playback_data to return or resolve as None.")

            logger.debug('SUCCESS update playback data')
            self._socket.send_response(204, request.id)
        except Exception as e:
            logger.exception('FAILED update playback data')
            self._socket.send_response(500, request.id, {'message': str(e)})
