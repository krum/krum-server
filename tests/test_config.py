"""Tests the config file parsing/handling."""

from krum import Config,InvalidConfigFile
from tests.test_helper import make_with_file_handler
import os
from os import path
import fudge
import builtins
from nose.tools import raises

class TestConfig:
    """Test the config parsing."""

    def setUp(self):
        self._old_config = os.environ.get("KRUM_CONFIG_FILE")
        if self._old_config:
            del os.environ["KRUM_CONFIG_FILE"]
        self.config = Config(reload=True)

    def tearDown(self):
        if self._old_config:
            os.environ["KRUM_CONFIG_FILE"] = self._old_config
        self.config = Config(reload=True)

    def test_initializing_config_gives_dict_instance(self):
        assert isinstance(self.config, dict)
        assert isinstance(self.config, Config)

    def test_multiple_calls_to_config_returns_same_item(self):
        assert id(self.config), id(Config())

    @fudge.patch('builtins.open')
    def test_multiple_calls_to_config_with_reload_true_returns_different_item(self,fake_open):
        fake_open.expects_call().calls(make_with_file_handler('default: blah'))
        assert id(self.config) != id(Config(reload=True))

    def test_giving_correct_environment_variable_overrides_items(self):
        test_path_name = '/blah/test/path'
        with fudge.patch('builtins.open') as fake_open:
            #open is called twice and returns two different files
            fake_open.is_callable().calls(make_with_file_handler('default: blah')).next_call().with_args(test_path_name).calls(make_with_file_handler('default: replaced'))
            os.environ['KRUM_CONFIG_FILE'] = test_path_name
            assert Config(reload=True)['default'], 'replaced'

    @raises(InvalidConfigFile)
    def test_giving_incorrect_environment_variable_should_raise_error(self):
        test_path_name = '/blah/test/path'
        with fudge.patch('builtins.open') as fake_open:
            fake_open.is_callable().calls(make_with_file_handler('default: blah')).next_call().with_args(test_path_name).raises(IOError)
            os.environ['KRUM_CONFIG_FILE'] = test_path_name
            assert Config(reload=True)['default'], 'blah'

    def test_giving_no_environment_variable_should_read_in_from_etc_krum_config_if_it_exists(self):
        with fudge.patch('builtins.open') as fake_open:
            fake_open.is_callable().calls(make_with_file_handler('default: blah')).next_call().with_args('/etc/krum/config.yml').calls(make_with_file_handler('default: replaced')).next_call().with_args(path.expanduser('~/.krum/config.yml')).raises(IOError)
            assert Config(reload=True)['default'], 'replaced'

    def test_giving_no_environment_variable_should_read_in_from_krum_config_in_home_dir_if_it_exists(self):
        with fudge.patch('builtins.open') as fake_open:
            fake_open.is_callable().calls(make_with_file_handler('default: blah')).next_call().with_args('/etc/krum/config.yml').raises(IOError).next_call().with_args(path.expanduser('~/.krum/config.yml')).calls(make_with_file_handler('default: replaced'))
            assert Config(reload=True)['default'], 'replaced'

    def test_giving_no_environment_variable_should_read_in_from_both_default_locations_and_merge_both_in(self):
        with fudge.patch('builtins.open') as fake_open:
            fake_open.is_callable().calls(make_with_file_handler('default: blah')).next_call().with_args('/etc/krum/config.yml').calls(make_with_file_handler('combine: woot')).next_call().with_args(path.expanduser('~/.krum/config.yml')).calls(make_with_file_handler('default: replaced'))
            config = Config(reload=True)
            assert config['default'], 'replaced'
            assert config['combine'], 'woot'
