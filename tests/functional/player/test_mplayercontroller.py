from tests.test_helper import dicteq, alldictseq, data_file_path, FutureWait
import logging
from datetime import timedelta
from nose.tools import eq_
import tornado.gen as gen
from tornado.testing import AsyncTestCase
from tornado.ioloop import IOLoop
from tornado.concurrent import Future
from tornado.web import url, Application, RedirectHandler
from tornado.testing import gen_test

logger = logging.getLogger(__name__)

from krum.player.mplayer import MPlayerController

class TestMPlayerController(AsyncTestCase):
    def get_app(self):
        '''This builds a Tornado application with the necessary handlers for all the playback APIs.'''
        return Application([
            (r'/',            RedirectHandler, {'url': 'bob'}),
        ])

    def get_new_ioloop(self):
        return IOLoop.instance()

    @gen_test
    def test_startup(self):
        mplayer = MPlayerController()

        # TODO: Assumes mplayer is in PATH
        yield mplayer.start()

        logger.debug('Requesting playback.')
        file_ = yield mplayer.get_prop('filename')
        assert file_ == None, "We didn't start playing anything"

        path = data_file_path('real_sample_movie.mp4')
        yield mplayer.play_file(path, False)

        file_ = yield mplayer.get_prop('filename')
        assert file_ == path, "I thought we were playing..."

        yield mplayer.stop_playback()

    @gen_test
    def test_play_pause_position(self):
        mplayer = MPlayerController()

        # TODO: Assumes mplayer is in PATH
        logger.debug('START Starting up MPlayer')
        yield mplayer.start()
        logger.debug('FINISH Starting up MPlayer')

        logger.debug('START Getting position 1st time')
        pos = yield mplayer.get_prop('position')
        logger.debug('FINISH Getting position 1st time')
        eq_(pos, None)

        logger.debug('START Playing first file')
        yield mplayer.play_file(data_file_path('real_sample_movie.mp4'), False)
        logger.debug('FINISH Playing first file')

        logger.debug('START Getting first file duration')
        duration = yield mplayer.get_prop('duration')
        logger.debug('FINISH Getting first file duration')
        assert duration is not None

        logger.debug('START Waiting 0.5 seconds')
        yield FutureWait(timedelta(seconds=0.5), None)
        logger.debug('FINISH Waiting 0.5 seconds')

        logger.debug('START Getting position 2nd time')
        new_pos = yield mplayer.get_prop('position')
        logger.debug('FINISH Getting position 2nd time')
        assert new_pos >= 0.0, 'Position sould be at least 0.4 seconds further in, was {}'.format(new_pos)

        logger.debug('START Getting paused 1st time')
        paused = yield mplayer.get_prop('paused')
        logger.debug('FINISH Getting paused 1st time')
        eq_(paused, False)

        logger.debug('START Setting paused 1st time')
        yield mplayer.set_prop('paused', True, True)
        logger.debug('FINISH Setting paused 1st time')
        logger.debug('START Getting paused 2nd time')
        new_paused = yield mplayer.get_prop('paused')
        logger.debug('FINISH Getting paused 2nd time')
        eq_(new_paused, True)

        logger.debug('START Stopping MPlayer playback')
        yield mplayer.stop_playback()
        logger.debug('FINISH Stopping MPlayer playback')
