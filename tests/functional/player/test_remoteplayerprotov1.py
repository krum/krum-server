from tests.test_helper import dicteq, alldictseq, data_file_path, FutureWait
import logging
import uuid
import fudge
from datetime import timedelta
from nose.tools import eq_
import tornado.gen as gen
from tornado.testing import AsyncTestCase, gen_test
from tornado.ioloop import IOLoop
from tornado.concurrent import Future
from tornado.web import url, Application, RedirectHandler

logger = logging.getLogger(__name__)

from krum.player.player_base import RemotePlayerProtocolV1
from krum.playback.messagetypes import Request, Event, Message

class FakeKrumSocket(object):
    def __init__(self, delegate):
        self._req_id = 0
        self._delegate = delegate
        self._response_futures = {}

    '''Just a fake krum socket so that we can pass it in and then pretend'''
    def read_until_closed(self):
        pass

    def close(self):
        self._delegate.on_close(self)

    def send_response(self, status, id, body={}):
        fut = self._response_futures.pop(id)
        fut.set_result((status, body))

    def fake_request(self, method, path, body={}):
        '''Sends a request into the delegate (just like it would receive from
            a socket). Returns the response that comes back.

            TODO: Support support async responses. Return future, capture response.
        '''

        # hand the request into the delegates handler (just like it would expect)
        self._req_id += 1

        fut = Future()
        self._response_futures[self._req_id] = fut
        self._delegate.on_request(self, Request(method, self._req_id, path, body))
        return fut

class FakeProtocolDelegate(object):
    '''Fake RemotePlayerProtocolV1 delegate.

    Saves the results so that they can be inspected afterwards.
    '''
    def __init__(self):
        self.guid = uuid.uuid4().hex
        self.playback = {
            'playlist': [],
            'playspeed': 1.0,
            'position': 0.0,
            'volume': 1.0,
            'paused': False
        }
        self.last_patch = None
        self.registered = False
        self.closed = True

    # our fake Proto delegate functions
    def get_player_guid(self, protocol):
        return self.guid
    def get_playback_data(self, protocol):
        # provide the future the protocol wants
        fut = Future()
        fut.set_result(self.playback)
        return fut
    def on_change_playback_data(self, the_patch, protocol):
        self.last_patch = the_patch
        self.playback.update(the_patch)

        # provide the future the protocol wants
        fut = Future()
        fut.set_result(None)
        return fut
    def on_open(self, protocol):
        self.closed = False
    def on_register(self, protocol):
        self.registered = True
    def on_close(self, protocol):
        self.closed = True

@gen.coroutine
def fake_krumsocket_connect(url, delegate, connect_timeout=None):
    '''Can use instead of krum.playback.krumsocket.krumsocket_connect.
    This is used so that tests can insert the mock, FakeKrumSocket.

    @rtype: A future that will get the "connected" FakeKrumSocket as it's value
    '''
    krum_socket = FakeKrumSocket(delegate)

    logger.debug('Fake KrumSocket being connected.')

    if delegate:
        delegate.on_open(krum_socket)

    raise gen.Return(krum_socket)

class TestRemotePlayerProtoV1(AsyncTestCase):

    def get_new_ioloop(self):
        '''Because the helper functions use the global loop :('''
        return IOLoop.instance()

    @gen.coroutine
    def do_register(self, delegate):
        player_proto = RemotePlayerProtocolV1(delegate)

        # now start fake driving it :)
        logger.debug('Test: Kicking off connect')
        reg_future = player_proto.register_to('ws://fake_url.faker/fake', connect_func=fake_krumsocket_connect)

        # need the loop to run a few cycles
        yield FutureWait(timedelta(milliseconds=10), None)

        logger.debug('Test: Checking protocol status')
        # we should have an "open" socket
        eq_(delegate.closed, False)
        # but we shouldn't yet be registered
        eq_(delegate.registered, False)

        # get the info from the player (need to nab it's socket)
        logger.debug('Test: Querying /meta data')
        socket = player_proto._socket
        status, body = yield socket.fake_request('GET', '/meta')
        eq_(status, 200)
        eq_(body['version'], 1)
        eq_(body['supported_versions'], [1])

        # now send through a registered message
        logger.debug('Test: Telling it that it is registered')
        registered_body = {
            'version': body['version'],
            'registered_path': '/api/1/playback/players/1',
            'registered_name': 'Dummy Player'
        }
        status, body = yield socket.fake_request('PATCH', '/meta', registered_body)
        eq_(status, 204)
        assert reg_future.done() and "The player should think it's registered now."

        raise gen.Return((player_proto, socket))

    @gen_test
    def test_register(self):
        # set up our dummy
        delegate = FakeProtocolDelegate()

        player_proto, socket = yield self.do_register(delegate)

    @gen_test
    def test_controls(self):
        # set up our dummy
        delegate = FakeProtocolDelegate()

        player_proto, socket = yield self.do_register(delegate)

        # now run some commands through it, make sure the pop up on the other side

        # a quick playlist request
        pl_body = {
            'playlist': [{
                    'data_url': 'http://nothing.com/sad_movie.mp4',
                    'name': 'A movie for testing!'
                    ''
                }
            ]
        }
        status, body = yield socket.fake_request('PATCH', '/playback', pl_body)
        eq_(status, 204)
        eq_(body, {})
        dicteq(delegate.last_patch, pl_body)
        eq_(delegate.playback['playlist'], pl_body['playlist'])
