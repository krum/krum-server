"""Defines tests that validate the behaviour of the /media APIs."""
from tests.test_helper import data_file_path, dicteq, alldictseq
from tests.functional.api.version_1 import create_version_url
from nose.tools import eq_
import tornado.httpclient
from tornado.testing import gen_test

# base API test case that constructs environment
from .test_base import TestAPIEndpointsBase

# actual used bits of code
from tests.factories import metabase

from krum.utilities.helpers import extract_attrs
from krum.utilities.json import from_json, default_tzinfo, JSON_MIMETYPE
from krum.content import import_file, Content
from krum.metabase import models
from krum.api.version_1.media import MOVIE_FIELDS, SERIES_FIELDS, EPISODE_FIELDS

# remove auto-lookup related fields
OUR_MOVIE_FIELDS = [x for x in MOVIE_FIELDS if '__' not in x]
OUR_SERIES_FIELDS = [x for x in SERIES_FIELDS if '__' not in x]
OUR_EPISODE_FIELDS = [x for x in EPISODE_FIELDS if '__' not in x]

def view_meta(obj, fields):
    data = extract_attrs(obj, fields)
    data.update({
        'lastwatched': None,
        'path': '/api/1/media/{}'.format(obj.id),
        'content': view_content_list(obj.content.all()),
        'genres': view_genres(obj.genres.all()),
        'content__added_timestamp': None
    })
    data['added_timestamp'] = data['added_timestamp'].isoformat()
    content = obj.content.first()
    if content is not None:
        data['content__added_timestamp'] = content.added_timestamp.isoformat()
    return data

def view_movie(mov):
    return view_meta(mov, OUR_MOVIE_FIELDS)

def view_series(ser):
    """Update Series model dict for /media API."""
    data = view_meta(ser, OUR_SERIES_FIELDS)
    data['episode_paths'] = view_episode_list(ser.episode_list.all())
    return data

def view_episode(ep):
    return view_meta(ep, OUR_EPISODE_FIELDS)

def view_episode_list(episodes):
    return ['/api/1/media/{}'.format(x.id) for x in episodes]

def view_content_list(content):
    return ['/api/1/content/{}'.format(x.id) for x in content]

def view_genres(genres):
    return [{
        'id': obj.pk,
        'path': create_version_url('genres/{}'.format(obj.pk)),
        'name': obj.name
    } for obj in genres]

class TestMetadataEndpoints(TestAPIEndpointsBase):

    def _setUp(self):
        self.movie_genre = metabase.GenreFactory()
        self.movie = metabase.MovieFactory()
        models.GenreLink(genre=self.movie_genre, meta=self.movie).save()
        self.movie_content = import_file(data_file_path('movie.mp4'))
        self.episode_content = import_file(data_file_path('movie2.avi'))
        self.series_genre = metabase.GenreFactory()
        self.series = metabase.SeriesFactory()
        self.episode = metabase.EpisodeFactory(series=self.series)
        models.GenreLink(genre=self.series_genre,meta=self.series).save()

        self.episode2_content = import_file(data_file_path('movie4.mp4'))
        self.series2_genre = metabase.GenreFactory()
        self.series2 = metabase.SeriesFactory()
        self.episode2 = metabase.EpisodeFactory(series=self.series2)
        models.GenreLink(genre=self.series2_genre,meta=self.series2).save()

        self.episode3_content = import_file(data_file_path('movie5.mp4'))
        self.episode3 = metabase.EpisodeFactory(series=self.series2)

        #create some more data that should not appear normally
        self.unlinked_content = import_file(data_file_path('movie3.avi'))
        for i in range(0,2):
            movie = metabase.MovieFactory()
            movie_genre = metabase.GenreFactory()
            models.GenreLink(genre=movie_genre,meta=movie).save()
            series_genre = metabase.GenreFactory()
            series = metabase.SeriesFactory()
            episode = metabase.EpisodeFactory(series=series)
            models.GenreLink(genre=series_genre,meta=series).save()

    def add_to_library(self):
        self.movie.in_library = True
        self.movie.save()
        self.series.in_library = True
        self.series.save()
        self.series2.in_library = True
        self.series2.save()

    def link_content(self):
        self.movie_content.metadata = self.movie
        self.movie_content.save()
        self.episode_content.metadata = self.episode
        self.episode_content.save()
        self.episode2_content.metadata = self.episode2
        self.episode2_content.save()
        self.episode3_content.metadata = self.episode3
        self.episode3_content.save()

    @gen_test
    def test_get_media_empty(self):
        response = yield self.http_get(create_version_url('media'))
        eq_(response.code, 200)
        eq_(from_json(response.body), {'list': []}) # wrapped JSON list
        eq_(response.headers['Content-Type'], JSON_MIMETYPE)

    @gen_test
    def test_get_all_media(self):
        self.add_to_library()
        self.link_content()

        response = yield self.http_get(create_version_url('media'))
        eq_(response.code, 200)
        response_data = from_json(response.body)['list'] # wrapped JSON list
        eq_(len(response_data), 6)
        eq_(response.headers['Content-Type'], JSON_MIMETYPE)

    @gen_test
    def test_get_media_only_movies(self):
        self.add_to_library()
        self.link_content()

        response = yield self.http_get(create_version_url('media'), 'media_type=1')
        eq_(response.code, 200)
        response_data = from_json(response.body)['list'] # wrapped JSON list
        eq_(len(response_data), 1)
        eq_(response.headers['Content-Type'], JSON_MIMETYPE)
        dicteq(response_data[0],
            view_movie(self.movie))

    @gen_test
    def test_get_media_only_series(self):
        self.add_to_library()
        self.link_content()

        response = yield self.http_get(create_version_url('media'), 'media_type=2')
        eq_(response.code, 200)
        response_data = from_json(response.body)['list'] # wrapped JSON list
        eq_(len(response_data), 2)
        eq_(response.headers['Content-Type'], JSON_MIMETYPE)
        alldictseq(response_data, sorted([
            view_series(self.series),
            view_series(self.series2)
        ], key=lambda x: x['name']))

    @gen_test
    def test_get_media_only_episodes(self):
        self.link_content()

        response = yield self.http_get(create_version_url('media/{}'.format(self.episode2.pk)))

        eq_(response.code, 200)
        response_data = from_json(response.body, response.headers['Content-Type'])
        dicteq(response_data, view_episode(self.episode2))

    @gen_test
    def test_episodes_endpoint_gives_404_for_non_existing_series(self):
        try:
            response = yield self.http_get(create_version_url('media/{}'.format(7727233321123)))
        except tornado.httpclient.HTTPError as e:
            eq_(e.code, 404)
            return

        raise Exception("A 404 HTTPError should have been raised. We shouldn't have gotten here.")
