from tests.test_helper import KrumAsyncTestCase

from tornado.ioloop import IOLoop
from tornado.web import Application

# /content handlers
from krum.api.version_1.content import ContentListHandler, ContentHandler
from krum.content.streamer import StreamingContentHandler

# Generic Resource handlers
from krum.api.version_1.resourcehandlers import CollectionHandler, InstanceHandler
# /media resources
from krum.api.version_1.media        import (MediaResource, GenreResource,
                                             MediaTypeResource)

class TestAPIEndpointsBase(KrumAsyncTestCase):
    def get_app(self):
        '''This builds a Tornado application with the necessary handlers for all the version 1 APIs.'''

        return Application([
# content
        (r'/api/1/content',
            ContentListHandler),
        (r'/api/1/content/(\d+)',
            ContentHandler, {}, 'api.1.content.item'),
        (r'/api/1/content/(\d+)/data',
            StreamingContentHandler, {}, 'api.1.content.data'),

# media
        (r'/api/1/media',
            CollectionHandler, {'resource': MediaResource()}),
        (r'/api/1/media/(\d+)',
            InstanceHandler, {'resource': MediaResource()}, 'api.1.media.item'),
        (r'/api/1/genres',
            CollectionHandler, {'resource': GenreResource()}),
        (r'/api/1/genres/(\d+)',
            InstanceHandler, {'resource': GenreResource()}, 'api.1.genre.item'),
        (r'/api/1/media_types',
            CollectionHandler, {'resource': MediaTypeResource()}),
        (r'/api/1/media_types/(\d+)',
            InstanceHandler, {'resource': MediaTypeResource()}, 'api.1.media_type.item')
        ])

    def get_new_ioloop(self):
        return IOLoop.instance()
