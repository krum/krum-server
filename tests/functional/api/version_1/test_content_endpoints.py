from tests.test_helper import data_file_path, get_data_file, dicteq

# testing malarky
import logging
from tornado.testing import gen_test
from tornado.httpclient import HTTPRequest
from nose.tools import eq_

# base API test case that constructs environment
from .test_base import TestAPIEndpointsBase

# actual used bits of code
from krum.content import import_file
from tests.factories import metabase

logger = logging.getLogger(__name__)

class TestContentEndpoints(TestAPIEndpointsBase):
    def create_content(self):
        self.movie = metabase.MovieFactory()
        self.movie_content = import_file(data_file_path('movie.mp4'))
        self.episode_content = import_file(data_file_path('movie2.avi'))
        self.series = metabase.SeriesFactory()
        self.episode = metabase.EpisodeFactory(series=self.series)
        self.episode2_content = import_file(data_file_path('movie4.mp4'))
        self.series2 = metabase.SeriesFactory()
        self.episode2 = metabase.EpisodeFactory(series=self.series2)
        self.episode3_content = import_file(data_file_path('movie5.mp4'))
        self.episode3 = metabase.EpisodeFactory(series=self.series2)

        # create some more data that should not appear normally
        self.unlinked_content = import_file(data_file_path('movie3.avi'))

    def link_content(self):
        self.movie_content.metadata = self.movie
        self.movie_content.save()
        self.episode_content.metadata = self.episode
        self.episode_content.save()
        self.episode2_content.metadata = self.episode2
        self.episode2_content.save()
        self.episode3_content.metadata = self.episode3
        self.episode3_content.save()

    @gen_test
    def test_content_endpoint_gives_correct_metadata(self):
        self.create_content()
        self.link_content()

        resp = yield self.fetch_json('/api/1/content')

        eq_(resp.code, 200)
        json = resp.json_body
        eq_(5, len(json['list']))

        # now check we can get each individually
        for c in json['list']:
            resp = yield self.fetch_json(c['path'])

            eq_(resp.code, 200)
            dicteq(resp.json_body, c)

    @gen_test
    def test_create_and_check_content(self):
        content_path = data_file_path('movie.mp4')
        original_content_dict = {
            'original_path': content_path,
            'mimetype': 'video/mp4'
        }

        # create the content resource
        logger.info('creating content resource')
        resp = yield self.fetch_json('/api/1/content', body=original_content_dict)
        eq_(resp.code, 201)

        # check it's as we asked
        logger.info('fetching content resource')
        content_url = resp.headers['Location']
        resp = yield self.fetch_json(content_url)
        eq_(resp.code, 200)
        logger.info('got back content: "{}"'.format(resp.body))

        content_dict = resp.json_body
        for k,v in original_content_dict.items():
            eq_(content_dict[k], v)

        # upload file data by POSTing
        content_data = get_data_file('movie.mp4')
        content_data_url = content_dict['data_url']
        logger.info('Uploading content data to url: {}'.format(content_data_url))
        req = HTTPRequest(
            self.get_url(content_data_url),
            'POST',
            {
                'Content-Range': 'bytes 0-{}/{}'.format(len(content_data)-1, len(content_data))
            },
            content_data
        )
        resp = yield self.http_client.fetch(req)
        eq_(resp.code, 204)

        # check the raw file data is what we uploaded
        logger.info('Fetching raw data')
        resp = yield self.http_get(content_data_url)
        eq_(resp.code, 200)
        eq_(resp.headers['Content-Type'], original_content_dict['mimetype'])
        eq_(resp.body, content_data)

        # now link it up with a "movie"
        movie = metabase.MovieFactory()
        logger.info('Linking with movie')
        resp = yield self.fetch_json(content_url, body={
            'metadata_id': movie.pk
        }, method='PATCH')
        eq_(resp.code, 204)

        resp = yield self.fetch_json(content_url)
        eq_(resp.code, 200)
        eq_(resp.json_body['metadata_id'], movie.pk)
