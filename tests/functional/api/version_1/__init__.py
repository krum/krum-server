def create_version_url(url):
    return '/api/1/{url}'.format(url=url)