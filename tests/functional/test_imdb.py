# coding=utf8
"""Test the IMDB import/update functions"""

from tests.test_helper import KrumTestCase, get_data_file, make_with_file_handler
import logging

import fudge
import io
from nose.tools import eq_

import krum.imdb.parser as imp
import krum.metabase.models as mbm
from krum.imdb.update import update_data
from krum.metabase.models import Movie, Series, Episode, Genre

logger = logging.getLogger(__name__)

class TestIMDBParser(KrumTestCase):
    def test_mov_suspended(self):
        eq_(('Human Factor', '2012', None), imp.parse_showname('Human Factor (2012/I) {{SUSPENDED}}'))

    def test_mov_quoted(self):
        eq_(('#Follow', '2011', None), imp.parse_showname('"#Follow" (2011)'))

    def test_mov_nonquoted(self):
        eq_(('#Follow', '2011', None), imp.parse_showname('"#Follow" (2011)'))

    def test_mov_otherquotes(self):
        eq_(("'Columbia' and 'Defender' Rounding Stake-Boat", '1899', None),
                imp.parse_showname("'Columbia' and 'Defender' Rounding Stake-Boat (1899)"))

    def test_ep_suspended(self):
        eq_(("'Til Death", '2006', ('', '2', '16')), imp.parse_showname('"\'Til Death" (2006) {(#2.16)} {{SUSPENDED}}'))

    def test_read_paran(self):
        cases = (
            ('(aka "Shit My Dad Says" (2010))      (USA) (imdb display title)',
                ('aka "Shit My Dad Says" (2010)', '      (USA) (imdb display title)')),
            ('(aka The Star Wars (1977))   (USA) (original script title)',
                ('aka The Star Wars (1977)', '   (USA) (original script title)')),
            ('(USA) (original script title)',
                ('USA', ' (original script title)')),
        )

        for c in cases:
            eq_(imp.readparan(c[0]), c[1])

    def test_aka_name(self):
        cases = (
            ('(aka "Shit My Dad Says" (2010))      (USA) (imdb display title)',
                ("Shit My Dad Says", 0)),
            ('(aka "Shit My Dad Says" (2010))      (USA) (uncensored intended title)',
                ("Shit My Dad Says", 1)),
            ('(aka "Shit My Dad Says" (2010))      (Italy) (imdb display title)',
                (None, 100000)),
            ('(aka "Beep My Dad Says" (2010))      (USA) (alternative title)',
                ("Beep My Dad Says", 2)),
        )

        for c in cases:
            eq_(imp.parse_showaka(c[0]), c[1])

    def test_aka_names(self):
        data = get_data_file('aka_names.list').decode('UTF8')

        eq_(set([("$#*! My Dad Says", "2010", "Shit My Dad Says")]),
            set(imp.parse_akatitles(io.StringIO(data))))

class TestIMDBImport(KrumTestCase):

    @fudge.patch('krum.imdb.files.get_file')
    @fudge.patch('krum.imdb.files.update_files')
    def test_movie_and_episode_import_with_no_update_files(self, fake_get_file, fake_update_files_should_not_get_called):
        def return_correct_file(filename):
            logger.debug('Handing over fake imdb file')
            test_filename=filename+'.list'
            return make_with_file_handler(get_data_file(test_filename).decode('utf8'))(filename)
        fake_get_file.expects_call().calls(return_correct_file)
        logger.debug('starting IMDB data update')
        update_data(update_files=False)
        self.all_series_imported_correctly()
        self.all_movies_imported_correctly()
        self.all_episodes_imported_correctly()
        self.all_movies_and_series_linked_to_genres_correctly()

    def all_movies_imported_correctly(self):
        all_movies = Movie.objects.filter(source__name='imdb')
        eq_(set(["...And Justice for All - ????","#1 - 2010","(Winter) Time - 2001","The Day the Earth Stood Still - 2008",
                "The Day the Earth Stood Still - 1951","Back to the Future - 1985","Jurassic Park - 1993","Jurassic Park III - 2001",
                "The Lost World: Jurassic Park - 1997","Le fabuleux destin d'Amélie Poulain - 2001"]),
            set(["{} - {}".format(movie.name, movie.year) for movie in all_movies]))

    def all_series_imported_correctly(self):
        #NOTE: Not 100% sure what we do with the two mentions of IT crowd.. decide later
        all_series = Series.objects.all()
        eq_(set(['Mystic Knights of Tir Na Nog - 1998', "Chuck - 2007","The IT Crowd - 2006","The IT Crowd - 2007","Back to the Future - 1991"]),
            set(["{} - {}".format(series.name, series.year) for series in all_series]))

    def all_episodes_imported_correctly(self):
        series_desc = {
            "Back to the Future": {"1991":set(["A Family Vacation - 1 - 2","A Friend in Deed - 2 - 3"])},
            "Chuck":              {"2007":set(["Chuck Versus Agent X - 4 - 22","Chuck Versus Bo - 5 - 10","Chuck Versus First Class - 3 - 5"])},
            "The IT Crowd":       {"2006":set(["Are We Not Men? - 3 - 2","Aunt Irma Visits - 1 - 6","Smoke and Mirrors - 2 - 5"]), "2007":set([' - 1 - 1',' - 1 - 10'])},
            "Mystic Knights of Tir Na Nog": {"1998":set(['Eye of the Beholder - 1 - 19', 'Divide and Conquer - 1 - 19'])},
            }
        for series_name, desc_hash in series_desc.items():
            for year, episodes in desc_hash.items():
                series = Series.objects.get(name=series_name, year=year)
                eq_(episodes,set(["{} - {} - {}".format(episode.ep_name, episode.season, episode.ep_num) for episode in series.episode_list.all()]))

    def all_movies_and_series_linked_to_genres_correctly(self):
        eq_(set(["Comedy","Drama"]), set([genre.name for genre in Series.objects.get(name="Chuck", year="2007").genres.all()]))
        eq_(set(["Sci-Fi"]), set([genre.name for genre in Movie.objects.get(name="Back to the Future", year="1985").genres.all()]))
        eq_(set(["Comedy"]), set([genre.name for genre in Series.objects.get(name="The IT Crowd", year="2006").genres.all()]))
        eq_(set([]), set([genre.name for genre in Series.objects.get(name="The IT Crowd", year="2007").genres.all()]))
        eq_(set(["Fantasy"]), set([genre.name for genre in Movie.objects.get(name="(Winter) Time", year="2001").genres.all()]))

    @fudge.patch('krum.imdb.files.get_file')
    @fudge.patch('krum.imdb.files.update_files')
    def test_episode_name_changes(self, fake_get_file, fake_update_files_should_not_get_called):
        import_run = 0

        def return_correct_file(filename):
            logger.debug('Handing over fake imdb file')
            test_filename='{}_ep_change_{}.list'.format(filename, import_run)
            return make_with_file_handler(get_data_file(test_filename).decode('utf8'))(filename)
        fake_get_file.expects_call().calls(return_correct_file)

        epsets = ({
                    ("The IT Crowd", "2006"): [[1,6,'']],
                    ('Mystic Knights of Tir Na Nog', '1998'): [[1,19,'Divide and Conquer']],
                  },
                  {
                    ("The IT Crowd", "2006"): [[1,6,'First Name']],
                    ('Mystic Knights of Tir Na Nog', '1998'): [[1,19,'Divide and Conquer'],[1,19,'Eye of the Beholder']],
                  },
                  {
                    ("The IT Crowd", "2006"): [[1,6,'Second Name']],
                    ('Mystic Knights of Tir Na Nog', '1998'): [[1,19,'Divide and Conquer'],[1,19,'Eye of the Beholder']],
                  })
        # helpers
        db_series = mbm.MetaBase.objects.filter(media_type_id=mbm.MB_MT_SERIES)
        db_eps = mbm.MetaBase.objects.filter(media_type_id=mbm.MB_MT_EPISODE)
        db_eps_full = mbm.Episode.objects.all()

        for i, eps in enumerate(epsets):
            logger.debug('starting IMDB data update {}'.format(i))
            import_run = i
            update_data(update_files=False)

            # what we want to see
            series_meta = set()
            ep_meta = set()
            ep_full_meta = set()
            for series, epnames in eps.items():
                series_meta.add(series)
                for x in epnames:
                    ep_meta.add("{} - S{:02}E{:02} - {}".format(series[0], *x).rstrip(' -'))

                    ep_full_meta.add(tuple([str(y) for y in x]))

            # check series metabase data is correct
            eq_(set(db_series.values_list("name","year")), series_meta)
            # check episode metabase data is correct
            eq_(set(db_eps.values_list("name", flat=True)), ep_meta)
            # check episode full data is correct
            eq_(set(db_eps_full.values_list('season','ep_num','ep_name')),
                ep_full_meta)
