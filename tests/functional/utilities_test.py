from datetime import datetime
import krum.utilities.helpers as uh
import krum.utilities.json as uj
from nose.tools import eq_

class TestUtilities(object):
    def test_now_datetime_gives_utc_date(self):
        eq_(uh.now_datetime().tzinfo, uh.UTC)

    def test_to_json_gives_iso_format_dates(self):
        json_date = uj.to_json(uh.now_datetime())
        assert json_date.endswith('+00:00"') or json_date.endswith('Z"')
