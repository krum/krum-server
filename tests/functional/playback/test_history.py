from __future__ import absolute_import

from tests.test_helper import KrumTestCase, data_file_path, dicteq, alldictseq, FutureWait

import logging
from datetime import timedelta
from nose.tools import eq_

import tornado.gen as gen
from tornado.httpclient import HTTPRequest

import krum.utilities.helpers as u
import krum.playback.models as pbm
from krum.content import import_file
from krum.metabase import models
from tests.factories import metabase

# local stuff
from .helpers import PlaybackTestCase

logger = logging.getLogger(__name__)

class TestHistory(PlaybackTestCase):
    def setUp(self):
        super(TestHistory, self).setUp()

        # add a movie
        logger.info('Creating movie')
        self.movie_genre = metabase.GenreFactory()
        self.movie = metabase.MovieFactory()
        models.GenreLink(genre=self.movie_genre,meta=self.movie).save()
        self.movie_content = import_file(data_file_path('real_sample_movie.mp4'))

        # link it, so we can test history properly
        self.movie_content.metadata = self.movie
        self.movie_content.save()

    def test_generate_history(self):
        # just a little kick off point so that tornado get's it's wait call (otherwise it's not time bounded)
        def on_finish(val):
            self.stop()

        self._do_generate_history(callback=self.stop)
        self.wait()

    @gen.coroutine
    def _do_generate_history(self):
        # make a session
        session_url = yield self._create_session("Andrew")
        # register a player
        player = yield self._register_player()
        # hook them up and send through a playlist
        content_url = self.get_url('/api/1/content/{}/data'.format(self.movie_content.id))
        logger.debug('START: Setting playlist')
        patch = {
          'player_path': player.player_path,
          'playlist': [{'data_url': content_url, 'duration': 12}],
          'paused': False
        }
        session_dict = yield self._send_patch(session_url, patch)
        logger.debug('FINISH: Setting playlist')
        # advance the playback by 10 seconds
        logger.debug('START: Moving "time" forward')
        player.advance_time(10)

        # need to wait for the history to be written, EVENTs are async with no response...
        yield FutureWait(timedelta(milliseconds=100), None)

        logger.debug('FINISH: Moving "time" forward')
        # clear the playlist
        logger.debug('START: Clearing playlist')
        patch = {
          'playlist': []
        }
        session_dict = yield self._send_patch(session_url, patch)
        logger.debug('FINISH: Clearing playlist')

        # There should be 1 history record
        history = pbm.History.objects.all()
        eq_(len(history), 1)
        hist_row = history[0]
        eq_(hist_row.content_url, content_url)
        eq_(hist_row.max_playback_position, 10)
