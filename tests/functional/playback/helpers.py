"""
Provides DummyRemotePlayer and base PlaybackTestCase classes.
"""
import uuid
import logging

from nose.tools import eq_
import tornado.gen as gen
from tornado.httpclient import HTTPRequest
from tornado.testing import AsyncHTTPTestCase
from tornado.ioloop import IOLoop
from tornado.concurrent import Future
from tornado.web import URLSpec, Application

from krum.utilities.json import from_json, to_json
import krum.playback.session as ps
import krum.playback.krumsocket as ks
from krum.content.streamer           import StreamingContentHandler

logger = logging.getLogger(__name__)

class DummyRemotePlayer(object):
    """
    Acts as a fake remote player. Saving the state the is requested of
    it.

    Can have it's "time" advanced for simulating playback duration and
    the associated events for both playback progress and playlist item
    changes.
    """

    def __init__(self, guid=None):
        if guid is None:
            self.guid = uuid.uuid4().hex
        else:
            self.guid = uuid.UUID(guid).hex
        self.meta = {
                'version': 1,
                'supported_versions': [1],
                'guid': self.guid,
                'client_type': 'DummyRemotePlayer',
                'registered_path': None,
                'registered_name': None,
                'isvisible': True
        }
        self.playback = {
            'playlist': [],
            'position': 0.0,
            'volume': 1.0,
            'paused': False
        }

        # callback defaults
        self._register_future = None
        self._close_future = None
        self._socket = None

    def register(self, url):
        '''Takes a URL and returns a future whose result is the connected and registered dummy player.'''
        logger.debug('Opening DummyRemotePlayer KrumSocket connection')
        self._register_future = Future()
        def on_connect(socket):
            self._socket = socket
            self._socket.read_until_closed()

        ks.krumsocket_connect(url, self, callback=on_connect)
        return self._register_future # this will tell it to wait until we're registered before returning

    @property
    def registered(self):
        return self.meta['registered_path'] is not None

    @property
    def player_path(self):
        return self.meta.get('registered_path')

    def async_close(self):
        self._close_future = Future()
        self._socket.close()
        return self._close_future

    def on_open(self, socket):
        logger.debug('Dummy Got OPEN: {}'.format(socket))

    def on_close(self, socket):
        self.meta['registered_path'] = None
        self.meta['registered_name'] = None

        if self._close_future is not None:
            self._close_future.set_result(None)

    def on_event(self, socket, event):
        pass

    def on_request(self, socket, request):
        logger.debug('DummyRemotePlayer request received: "{}"'.format(request))
        if request.method not in ('GET', 'PATCH'):
            self._socket.send_response(405, request.id, {'message': 'Only GET and PATCH supported'})
            return

        # cruddy path checking
        if request.path == '/meta':
            if request.method == 'GET':
                self._socket.send_response(200, request.id, self.meta)
            elif request.method == 'PATCH':
                self.meta.update(request.body)
                self._socket.send_response(204, request.id)

                # if we've now got a url and it's the first time
                if self.meta['registered_path'] is not None and self._register_future is not None:
                    logger.debug('DummyRemotePlayer: Registration process completed, url: "{}"'.format(self.meta['registered_path']))
                    self._register_future.set_result(self)
                    self._register_future = None
            else:
                raise Exception('WTF?')

        elif request.path == '/playback':
            if request.method == 'GET':
                self._socket.send_response(200, request.id, self.playback)
            elif request.method == 'PATCH':
                self.playback.update(request.body)
                self._socket.send_response(204, request.id)
                logger.debug('New /playback state: {}'.format(self.playback))
            else:
                raise Exception('WTF2?')

        else:
            self._socket.send_response(
                404,
                request.id,
                {'message': 'Path "{}" not found.'.format(request.path)}
            )

    @property
    def isplaying(self):
        '''Returns a boolean to represent if this player is currently playing content or not.'''
        return not self.playback['paused'] and len(self.playback['playlist']) > 0

    def advance_time(self, amount):
        """
        Moves the players playback forward ''amount'' seconds. This
        will trigger a PATCH event to be sent to the server.

        Params:
            amount: The amount of time, in seconds, to move the clock
                forward. Must be a positive number.
        """

        assert amount >= 0.0 and "amount parameter must be >= 0.0"

        if not self.isplaying:
            logger.debug('NOT PLAYING, skipping position PATCH send. Current /playback state: {}'
                         .format(self.playback))
            return

        self.playback['position'] += amount
        logger.debug('Sending position PATCH EVENT')
        self._socket.send_event('PATCH', '/playback', {'position': self.playback['position']})

    def advance_playlist(self):
        '''Simulates the first item in the playlist completing playback.
        Sends appropriate events through to Krum for tracking.
        '''
        if not self.isplaying:
            return

        self.playback['playlist'] = self.playback['playlist'][1:]
        self.playback['position'] = 0
        self._socket.send_event('PATCH', '/playback', {
            'position': self.playback['position'],
            'playlist': self.playback['playlist']
        })

class PlaybackTestCase(AsyncHTTPTestCase):
    """
    Handy base for playback test cases. Allows testing against the APIs.
    """

    def get_app(self):
        """
        Return a Tornado Application with playback API Handlers.
        """

        playback_set = ps.PlaybackSet()
        args = {'state': playback_set}
        sargs = {'delegate': playback_set}

        return Application([
            (r'/api/1/content/(\d+)/data',
                 StreamingContentHandler, {}, 'api.1.content.data'),
            (r'/api/1/playback/players',
                 ps.RemotePlayerListHandler, args),
            (r'/api/1/playback/players/(\d+)',
                 ps.RemotePlayerHandler,     args, 'api.1.playback.player.item'),
            (r'/api/1/playback/players/register',
                 ks.KrumSocketHandler,       sargs),
            (r'/api/1/playback/sessions',
                 ps.SessionListHandler,      args),
            (r'/api/1/playback/sessions/(\d+)',
                 ps.SessionHandler,          args, 'api.1.playback.session.item'),
        ])

    def get_new_ioloop(self):
        return IOLoop.instance()

    @gen.coroutine
    def _create_session(self, name, expected_url=None):
        req = HTTPRequest(
            self.get_url('/api/1/playback/sessions'),
            'POST',
            {'Content-Type': 'application/json; charset=UTF-8'},
            to_json({'name': name})
        )
        response = yield self.http_client.fetch(req)

        eq_(201, response.code)
        url = response.headers.get('Location')
        if expected_url is not None:
            eq_(expected_url, url)

        raise gen.Return(url)

    @gen.coroutine
    def _send_patch(self, url, patch):
        """
        Sends a PATCH request to url with a payload of to_json(patch)
        Then checks that the resource at the URL has been updated and
        that it now matches the requested state.
        """

        # send a patch off
        req = HTTPRequest(
            self.get_url(url),
            'PATCH',
            {'Content-Type': 'application/json; charset=UTF-8'},
            to_json(patch)
        )
        response = yield self.http_client.fetch(req)
        eq_(204, response.code)

        session_dict = yield self._check_new_state(url, patch)
        raise gen.Return(session_dict)

    @gen.coroutine
    def _check_new_state(self, url, expect):
        # checks the dict returned by GETting the url and makes sure all keys in expect
        # are present and the values match in the returned data.

        response = yield self.http_client.fetch(self.get_url(url))
        eq_(200, response.code)

        sess = from_json(response.body, response.headers.get('Content-Type'))

        for k, v in expect.items():
            assert k in sess, 'Session is missing attribute {}'.format(k)
            eq_(v, sess[k])

        raise gen.Return(sess)

    @gen.coroutine
    def _register_player(self, guid=None):
        # register a player so we can request it in the session
        register_url = self.get_url('/api/1/playback/players/register').replace('http://','ws://')
        player = DummyRemotePlayer(guid=guid)
        yield player.register(register_url)

        raise gen.Return(player)

    def _session_player_eq(self, session_dict, player_dict):
        # make sure the session and the player agree on their state
        for k in ('volume', 'paused', 'playlist', 'position'):
            eq_(session_dict[k], player_dict[k])
