from __future__ import absolute_import

import logging
from nose.tools import eq_
import tornado.gen as gen
from tornado.httpclient import HTTPRequest
from tornado.testing import gen_test

from krum.utilities.json import from_json
from .helpers import PlaybackTestCase, DummyRemotePlayer

logger = logging.getLogger(__name__)

class TestRegister(PlaybackTestCase):
    @gen_test
    def test_register_player(self):
        url = self.get_url('/api/1/playback/players/register').replace('http://','ws://')
        player = DummyRemotePlayer()
        yield player.register(url)
        yield self.check_player_list()

    @gen.coroutine
    def check_player_list(self):
        req = HTTPRequest(
            self.get_url('/api/1/playback/players'),
            'GET',
        )
        response = yield self.http_client.fetch(req)

        eq_(200, response.code)
        players = from_json(response.body)['list']
        eq_(1, len(players))
        eq_('DummyRemotePlayer', players[0]['name'])

class TestSessions(PlaybackTestCase):
    @gen_test
    def test_create_session(self):
        # Create a session to fiddle with
        # functions that execute further steps in the test are prefixed with do_

        # create the session
        logger.debug('START: Create session')
        session_url = yield self._create_session("Andrew")
        logger.debug('FINISH: Create session')

        # Check that it's set up correctly
        expect = {
            'name': 'Andrew',
            'player_path': '',
            'playlist': [],
            'paused': False,
            'position': 0,
            'volume': 1.0
        }
        logger.debug('START: Checking session')
        yield self._check_new_state(session_url, expect)
        logger.debug('FINISH: Checking session')
        # POST another with the same name
        logger.debug('START: re-creating session with same name')
        yield self._create_session("Andrew")
        logger.debug('FINISH: re-creating session with same name')
        # verify we're still good
        logger.debug('START: Re-checking session')
        yield self._check_new_state(session_url, expect)
        logger.debug('FINISH: Re-checking session')

        # apply an update
        patch = {
            'volume': 0.5,
            'playlist': ['http://bob.com/vid.mp4']
        }
        logger.debug('START: Giving first playlist')
        yield self._send_patch(session_url, patch)
        logger.debug('FINISH: Giving first playlist')

        logger.debug('START: Register player')
        player = yield self._register_player()
        logger.debug('FINISH: Register player')

        logger.debug('START: Add player to session')
        assert(player.player_path is not None)
        patch = {
          'player_path': player.player_path
        }
        session_dict = yield self._send_patch(session_url, patch)
        logger.debug('FINISH: Add player to session')

        # make sure the session and the player agree on their state
        self._session_player_eq(session_dict, player.playback)

        # send off a player update
        patch = {
            'playlist': ['http://localhost:1212/dood1','http://localhost:1212/dood2']
        }
        session_dict = yield self._send_patch(session_url, patch)

        self._session_player_eq(session_dict, player.playback)

        # save the original URL for a re-connect test
        player_path = player.player_path

        # close then update, make sure we don't poop our pants
        logger.debug("START: Closing remote player (from it's side")
        yield player.async_close()
        logger.debug("FINISH: Closing remote player (from it's side")
        # make sure we can still manipulate the session even though it's player has dropped off
        patch = {
                'volume': 0.5
        }
        session_dict = yield self._send_patch(session_url, patch)

        # make a new player with the same GUID as the old so we can test auto re-acquisition
        player2 = yield self._register_player(player.guid)
        eq_(player_path, player2.player_path)
