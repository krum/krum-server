"""Defines simple fake model instances that can be used for tests."""
import factory
from krum.metabase import models
from datetime import datetime
from random import random, choice, randint

class MovieFactory(factory.DjangoModelFactory):
    FACTORY_FOR     = models.Movie
    name            = factory.Sequence(lambda a: (u' '.join('some silly text')+str(a)))
    source          = factory.LazyAttribute(lambda a: models.MetaSource.objects.get_or_create(name=choice(['test_factory',]))[0])
    media_type_id   = models.MB_MT_MOVIE
    year            = '2010'
    other_id        = 0

class SeriesFactory(MovieFactory):
    FACTORY_FOR     = models.Series
    media_type_id   = models.MB_MT_SERIES

class EpisodeFactory(MovieFactory):
    FACTORY_FOR     = models.Episode
    media_type_id   = models.MB_MT_EPISODE
    season          = factory.LazyAttribute(lambda a: str(randint(1, 12)))
    ep_num          = factory.LazyAttribute(lambda a:  str(randint(1, 54)))
    ep_name         = factory.LazyAttribute(lambda a:  str(randint(1, 54)))

class GenreFactory(factory.DjangoModelFactory):
    FACTORY_FOR     = models.Genre
    name            = factory.Sequence(lambda a: ('_'.join('silly text')+str(a)))

class MovieMediaTypeFactory(factory.DjangoModelFactory):
    FACTORY_FOR                  = models.MediaType
    FACTORY_DJANGO_GET_OR_CREATE = ('name',)
    name                         = 'Movie'

class EpisodeMediaTypeFactory(factory.DjangoModelFactory):
    FACTORY_FOR                  = models.MediaType
    FACTORY_DJANGO_GET_OR_CREATE = ('name',)
    name                         = 'Episode'

class SeriesMediaTypeFactory(factory.DjangoModelFactory):
    FACTORY_FOR                  = models.MediaType
    FACTORY_DJANGO_GET_OR_CREATE = ('name',)
    name                         = 'Series'
