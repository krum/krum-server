from tests.test_helper import dicteq, alldictseq, data_file_path, FutureWait
import logging
from datetime import timedelta

import tornado.gen as gen
from nose.tools import eq_
from tornado.httpclient import HTTPRequest
from tornado.testing import gen_test

# it has all the necessary crud to create a server with sessions and so on.
from krum.utilities.json import from_json
from krum.content import import_file
from ..functional.playback.helpers import PlaybackTestCase

# what we're testing (along with server side gear)
from krum.player.mplayerremoteplayer import MPlayerRemotePlayer

logger = logging.getLogger(__name__)

class TestMplayerWithViaKrum(PlaybackTestCase):
    @gen_test
    def test_no_players(self):
        # just make sure there are no sessions
        response = yield self.http_client.fetch(self.get_url('/api/1/playback/players'))
        eq_(200, response.code)
        players = from_json(response.body)['list']
        eq_([], players)

    @gen.coroutine
    def create_player(self):
        # set up and register the remote player
        remote_player = MPlayerRemotePlayer(io_loop=self.io_loop)
        yield remote_player.start(self.get_url('/api/1/playback/players/register').replace('http://', 'ws://'))

        response = yield self.http_client.fetch(self.get_url('/api/1/playback/players'))
        eq_(200, response.code)
        players = from_json(response.body)['list']
        eq_(1, len(players))

        # return the metadata about the player
        raise gen.Return((remote_player, players[0]))

    @gen_test
    def test_register(self):
        remote_player, player_info = yield self.create_player()
        eq_('Mplayer', player_info['name'])  # the name defaults to the client_type if there aren't any collisions

        # also check that there is actually an mplayer process
        assert remote_player._controller is not None
        assert remote_player._controller._mplayer is not None

    @gen_test
    def test_acquire_player(self):
        remote_player, player_info = yield self.create_player()

        # now make a session and request this player
        session_url = yield self._create_session("Andrew")

        # request our shiny new player
        patch = {
            'player_path': player_info['path']
        }
        yield self._send_patch(session_url, patch)

        # check our session
        expect = {
            'name': 'Andrew',
            'player_path': player_info['path'],
            'playlist': [],
            'paused': False,
            'position': 0,
            'volume': 1.0
        }
        yield self._check_new_state(session_url, expect)

    def set_up_movie(self):
        movie_content = import_file(data_file_path('real_sample_movie.mp4'))
        movie_content.save()
        return movie_content

    @gen_test
    def test_play_item(self):
        # set up the player
        movie_content = self.set_up_movie()
        remote_player, player_info = yield self.create_player()

        # now make a session
        session_url = yield self._create_session("Andrew")

        # request our shiny new player and ask it to play
        playlist = [{
            'data_url': self.get_url('/api/1/content/{}/data'.format(movie_content.pk)),
            'name': 'Test file'
        }]
        patch = {
            'player_path': player_info['path'],
            'playlist': playlist
        }
        yield self._send_patch(session_url, patch)

        # check that the remote player got the updated playlist
        rm_playlist = remote_player._playback['playlist']
        item = rm_playlist[0]

        # the remote player adds the duration in
        if 'duration' in item:
            del item['duration']

        eq_(playlist, rm_playlist)

        # wait for a tick, just long enough for the window to pop up
        yield FutureWait(timedelta(seconds=0.3), None)

        # TODO: Need to find out why this bugs out
        yield remote_player.stop()
