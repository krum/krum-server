"""Defines tests that exercise the CLI commands."""

import logging
from ..test_helper import KrumAsyncTestCase
from tornado.testing import gen_test
from nose.tools import eq_

import krum.console.main as main

logger = logging.getLogger(__name__)

class TestCommandRun(KrumAsyncTestCase):
    """Tests related to the 'run' CLI command."""

    def get_app(self):
        """Go in via the main route and get the app!"""
        app = main.do_main(['run'], nostart=True)
        logger.info('App: {}'.format(app))
        return app

    @gen_test
    def test_basic(self):
        """Start up the webserver, hit it a coupla times."""

        resp = yield self.fetch_json('/api/1/media')
        eq_(resp.json_body, {'list': []})
