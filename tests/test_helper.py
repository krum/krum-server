"""Defines helpful test utility classes and configures django for the
test environment."""
import shutil
from os import environ, path as op
import os

environ["KRUM_CONFIG_FILE"] = op.join(op.dirname(__file__),'../krum/config/test.yml')
environ['DJANGO_SETTINGS_MODULE'] = 'krum.settings'

# delete the test database
from krum import cfg

if op.exists(cfg['data_path']):
    shutil.rmtree(cfg['data_path'])
os.makedirs(cfg['data_path'])

import krum         # imported to make sure django and such are done too
from django.core import management
management.call_command('syncdb')
management.call_command('migrate')
import django.test
try:
    from StringIO import StringIO
except ImportError:
    from io import StringIO
from tornado.concurrent import Future
from tornado.ioloop import IOLoop
from tornado.testing import AsyncHTTPTestCase
from subprocess import Popen
import atexit
import logging

logger = logging.getLogger(__name__)

class KrumTestCase(django.test.TransactionTestCase):
    def setUp(self):
        super(KrumTestCase, self).setUp()
        self._set_up_lookup_data()
        if hasattr(self,'_setUp'):
            self._setUp()

    def _set_up_lookup_data(self):
        """Set up the lookups that are required throughout the app."""
        from .factories import metabase

        metabase.MovieMediaTypeFactory()
        metabase.SeriesMediaTypeFactory()
        metabase.EpisodeMediaTypeFactory()

from tornado.httpclient import HTTPRequest
import tornado.gen as gen

from krum.utilities.json import from_json, to_json, JSON_MIMETYPE

# TODO: Eventually we'll get rid of the django bits...
class KrumAsyncTestCase(KrumTestCase, AsyncHTTPTestCase):
    '''Just has some helpers for JSON API based testing.'''

    def http_get(self, path, query=None):
        url = self.get_url(path)
        if query is not None:
            url = "{}?{}".format(url, query)

        return self.http_client.fetch(url)

    @gen.coroutine
    def fetch_json(self, path, query=None, body=None, method=None):
        url = self.get_url(path)
        if query is not None:
            url = "{}?{}".format(url, query)

        if method is None:
            method = ('POST', 'GET')[body is None]
        if body is not None:
            headers = {'Content-Type': JSON_MIMETYPE}
            body = to_json(body)
        else:
            headers = {}

        req = HTTPRequest(
            url,
            method,
            headers,
            body
        )
        resp = yield self.http_client.fetch(req)

        # decode if it should be there
        if resp.code == 200:
            resp.json_body = from_json(resp.body, resp.headers['Content-Type'])

        return resp

def data_file_path(file_name):
    return op.join(op.dirname(__file__), 'test_data', file_name)

def get_data_file(file_name):
    with open(data_file_path(file_name), 'rb') as test_file:
        return test_file.read()

def make_with_file_handler(contents):
    class ContextStringIO(StringIO):
        def __init__(self, contents):
            StringIO.__init__(self, contents)
        def __enter__(self, *args, **kwargs):
            return self
        def __exit__(self, *args, **kwargs):
            pass

    return lambda f, m='rb': ContextStringIO(contents)

def dictIncludesAll(a, b):
    """Checks that all key-value pairs in a are in b."""
    for k,v in a.items():
        if k not in b:
            assert False, '\n\nKey "{}" in {} but not {}'.format(k, a, b)
        elif v != b[k]:
            assert False, ('\n\nVal for key "{}" differs:\n\t"{}" != "{}"\nSo:\n\t{}\n !=\n\t{}'
                           .format(k, repr(v), repr(b[k]), a, b))

def dicteq(a,b):
    '''Helper function to compare dictionaries with more useful info rather than just x != y'''
    dictIncludesAll(a, b)
    dictIncludesAll(b, a)

    if a != b:
        assert False, "\n\n{} != {}, but can't tell why".format(a,b)

def alldictseq(sa, sb):
    '''Helper func that compares 2 sequences of dictionaries, providing better information'''
    assert len(sa) == len(sb), ("The sequences aren't the same length therefore\n\t{}\n !=\n\t{}"
                                .format(sa, sb))

    for i in range(len(sa)):
        dicteq(sa[i], sb[i])

def FutureWait(deadline, value):
    '''Returns a future who's result will be set to value after timeout seconds.

    :param deadline: May be a number denoting a time (on the same
        scale as `IOLoop.time`, normally `time.time`), or a
        `datetime.timedelta` object for a deadline relative to the
        current time.
    :param value: Value to set the returned Future's result attribute to after the timeout.
    :return: A Future that will "complete" after timeout seconds.
    '''
    timeout_future = Future()
    def on_timeout():
        timeout_future.set_result(value)

    IOLoop.current().add_timeout(deadline, on_timeout)
    return timeout_future
