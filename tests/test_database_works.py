"""Tests basic DB connectivity."""
from .test_helper import KrumTestCase
from tests.factories.metabase import GenreFactory
from krum.metabase.models import Genre
from nose.tools import eq_

class TestTheDjangoStuffs(KrumTestCase):
    """Test it!"""

    def test_factories_are_able_to_create_data(self):
        genre = GenreFactory()
        eq_(Genre.objects.get(pk=genre.pk).pk, genre.pk )

    def test_data_is_cleared_between_tests(self):
        eq_(len(Genre.objects.all()),0)
