#!python

import platform
from setuptools import setup, find_packages

long_description = open('README.md').read()
install_requirements = open('requirements.txt').read().split("\n")
test_requirements = open('requirements_development.txt').read().split("\n")

# Windows doesn't need setproctitle because it will be built into an exe anyway
# TODO: This is a bit ugly...
if platform.system() == 'Windows':
    install_requirements = [x for x in install_requirements if not x.startswith('setproctitle')]

setup(
    name='krum',
    version='0.0.1',
    description='Krum is a media library and server',
    long_description=long_description,
    author='Sally, Chris & Andrew',
    author_email='andrewstewis+krum@gmail.com',
    url='https://bitbucket.org/satook/krum_server',
    packages=find_packages(exclude=['tests']),
    package_data={
        '': ['*.txt', '*.rst'],
        'krum': ['config/*.yml']
    },
    tests_require=test_requirements,
    install_requires=install_requirements,
    entry_points={
        'console_scripts': [
            'krum = krum.console.main:main',
        ]
    },
    zip_safe=False
)
