
HTTP Requests
==============

These all start with Server -> App -> Handler

HTTP Requests
 - Handler  -> State?       (may change resource state)
            -> ORM?         (may change resource state)
            -> View func    (translates model to resource)
            -> Serialise    (dict->JSON)

Some rules - maybe they hold?
    - handlers act as the endpoint for a resource location
        - they can take in, validate and assert patches
        - they should only translate and pass to the resource piece
        - resource should be "collection aware"
            - reach resource always exists within a colletion
            - resource & collection are tied to URLs

Mutable things
    - media resources (needs collection)
    - content resources (needs collection)
    - session resources (doesn't need collection)

Immutable things
    - media types
    - genres (don't get deleted only added)
