Handy Krum tips and tricks
--------------------------

WebUI usage
^^^^^^^^^^^

Faster series matching
""""""""""""""""""""""
    You can just type the season number and episode numbers and Krum will know
    what you mean. E.g. If picking season 2, just type 2 instead of picking "2"
    from the dropdown, and press tab to move on to picking episodes. The same
    can be done for episodes, which you can observe as the text box will update
    with the number and name, whereas you only typed in the number.

Server install
^^^^^^^^^^^^^^

Run Krum in a VirtualEnv
""""""""""""""""""""""""
    Using VirtualEnv is a great way to avoid different python applications
    having conflicts with their dependencies.
