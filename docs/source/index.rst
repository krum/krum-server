.. Krum documentation master file, created by
   sphinx-quickstart2 on Sun Oct 14 13:33:56 2012.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to Krum's documentation!
================================

Contents:

.. toctree::
   :maxdepth: 2

   features
   conventions
   urls
   tips
   browser_reqs
   message_protocol
   remote_players

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`

