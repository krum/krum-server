Browser requirements
====================

Required features
-----------------

Krum needs a modern browser to run. Your browser must have all the following features in order to support all of Krum's functionality.

 - EcmaScript 6 Generators - Most Web UI functionality.
 - Video element (with H264 codec support) - Web UI video playback
 - Video element (subtitles - webVTT) - Web UI subtitles support
 - Audio element (with PCM, AAC, MP3 codec support) - Web UI audio playback
 - XMLHttpRequest Level 2 (Upload files) - Web UI uploads
 - Drag and Drop (all attributes and events) - Web UI uploads
 - File API - Web UI uploads

To check your browser is fully capable, visit html5test.com and check that each
of these features is available in your browser.

Limitations
-----------

Only in desktop browsers:
 - Drag and drop content uploads are only supported in desktop
   browsers. This is because mobile browsers don't implement drag and
   drop.
