==================
Krum URL structure
==================

Querying the library
--------------------

Media library listing
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
/api/1/media?media_type=[]&genre=[]
    This will return a list of summarised items that match the
    requested media_types and/or genres.

Media library item listing
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
/api/1/media/{media_id}
    This will return a more full representation of the item,
    e.g. including episodes in a season.

Autocomplete searching
^^^^^^^^^^^^^^^^^^^^^^
/api/1/autocomplete?term=#&media_type=[]&offset=#&limit=#
    Allows fast text based search of the entire set of metadata, whether or
    not you have any content linked to that metadata.

    :Supports: GET

Querying content
----------------

Content listing
^^^^^^^^^^^^^^^
/api/1/content?media_type=[]&hasmetadata=#&offset=#&limit=#
    Represents all content, linked or not to metadata.

    :Supports: GET, PUT

Content object
^^^^^^^^^^^^^^
/api/1/content/{content_id}
    This returns, or updates, the data surroudning 1 piece of content.
    PUT is used to update the metadata_id, i.e. link this content with
    a piece of metadata.

    :Supports: GET, PUT

Content data
^^^^^^^^^^^^
/api/1/content/{content_id}/data
    Provides access to the raw data (i.e. bytes) of a piece of content.
    All content will be returned with an accurate mimetype, in the
    Content-Type header. Range requests can be used to retrieve subsets
    of the data.

    :Supports: GET

Content thumbnail
^^^^^^^^^^^^^^^^^
/api/1/content/{content_id}/thumbnail?time=#
    This provides a thumbnail for the content, usually derived from the
    content itself, rather than being a banner for the series/movie/etc.
    This only works for visual content, i.e. video files.

    :Supports: GET

Controlling remote playback devices
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
To control remote playback devices, clients must create a player session and in
doing so select the device they wish to control. If the remote player is already
part of an existing session, Krum will return a 409 Conflict to indicate this.
A client can force Krum to move the remote player into it's session by
passing forceplayer=true on the query string. Clients MUST first PUT
without this and allow users to choose whether to take the remote player or not.

If a session that no longer has any content playing is holding a remote player that
another client has requested in a different session, the remote player will be moved
to the new session without requiring forceplayer=true on the query string.
If the client for that session has a notification path, Krum will notify it of
this.

/api/1/playback/sessions {GET, POST for create}::

    {
      "id": 123123,
      "url": "/api/1/playback/sessions/{id}",
      "name": "Sally",
      "player_url": "",        // One of Krum server's URLs for remote players
      "playlist": [],
      "paused":
      "position":
      "volume":
    }

/api/1/playback/sessions/{id} {GET, PATCH for changes}

/api/1/playback/sessions/{id}/history {GET}

/api/1/playback/players {GET}

Remote players
--------------
The implementation of Remote Players is described here, :ref:`remote_player_implementation`.
