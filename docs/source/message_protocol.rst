.. _websocket_message_protocol:

Krum message protocol
===================
Krum uses web sockets to keep live connections to/from clients that require it,
e.g. players or notification clients.

Message structure
--------------------
- Requests, Responses and Events
- encoded as UTF8 JSON
- All messages must have:
    - type: REQUEST | RESPONSE | EVENT
    - body: Can be "{}"
- Requests MUST provide
    - method: Just like HTTP
    - id: to link up responses with requests
    - path: Just like HTTP
- Responses MUST provide
    - id: which came from the request that prompted this response
    - status: Just like http. Must be a JSON number, not a string.
- Events MUST provide
    - method: either PATCH or PUT to represent a delta or a complete object respectively
    - path: The path of the resource that this event represents change to/creation of.
    - body: The body in Events represents either the JSON data for a PATCH of the original
        object or the complete JSON representation of the resource. Essentially, the JSON
        should allow the event receiver to update their copy of the object, or add it to
        their set of the objects.

Examples
----------

.. _websocket_message_protocol_get_request:

GET request
^^^^^^^^^^^^^^^
This is a GET request on the /meta path.::

    {
        type: 'REQUEST',
        method: 'GET',
        id: 12,
        path: '/meta',
        body: {}
    }

GET response
^^^^^^^^^^^^^^^
This is a possible response to the above GET request.::

    {
        type: 'RESPONSE',
        id: 12,
        status: 200,
        body: {
            position: 24,
            duration: 240,
            content_list: [
                http://krum:8888/api/1/content/24/data
            ]
        }
    }

PATCH event
^^^^^^^^^^^
This example is to notify Krum that the playback position of a
:ref:`Remote Player <remote_player_implementation>` has changed.::

    {
        type: 'EVENT',
        method: 'PATCH',
        path: '/',
        body: {position: 25.032},
    }

PUT event
^^^^^^^^^^^^
This event could be used to represent a new Content resource being created. Krum might send
this back to a client that was interested in maintaining an up-to-date index of Content objects.::

    {
        type: 'EVENT',
        method: 'PUT',
        path: '/api/1/content/52',
        body: {
            id: 52,
            url: /api/1/content/52,
            original_filename: "Jon get's a new head.m4v",
            original_hash: ad94191fd9277df51a4a93aa5747ea36be138510,
            mimetype: 'text/plain',
        }
    }

