.. _conventions:

Conventions
============

HTTP Usage
-----------
All the API's in Krum are implemented using HTTP, in a way that is hopefully not too abusive.
We use the following HTTP verbs as described:

GET:
    Used in the usual manner to request a full copy of a resource on the given URI. On success
    the resource body will be returned with a 200, or 206, response.

PUT:
    Not being used. Use PATCH instead.

PATCH:
    Used to request a partial update to a resource. On success, Krum will respond with a 204
    status code.

POST
    Used to create new resources by POSTing to a "list" URI, e.g. /sessions. On success,
    Krum will return a 201 status code with the Location header set to allow retrieval of
    the new resource.

DELETE
    Used to delete a resource. On success, Krum will return a 204 status code.

Resource representation
-------------------------
JSON is being used as the serialisation format for resources. No other format will be supported.
This is not out of spite but simplicity and a desire to do interesting things :).

**Note:** In places where a JSON *list* would be returned as the resource, e.g. /api/1/content
semantically returns a list of content resources, Krum will return a JSON *object* with a
single key "list". This is to avoid a subtle security exploit that returning JSON lists
directly allows. More details at
http://haacked.com/archive/2008/11/20/anatomy-of-a-subtle-json-vulnerability.aspx

For example, the resource at /api/1/content will look something like this:

    {
        "list": [
            {
                "mimetype": "video/mp4",
                "original_path": "/data/downloads/video1.mp4",
                "data_url": "/api/1/content/1/data",
                "url": "/api/1/content/1",
                "metadata_url": "/api/1/media/1512122",
                "added_timestamp": "2013-06-10T01:54:30.276543+00:00",
                "id": 1,
                "metadata_id": 1512122,
                "original_hash": "bf5b55c06882b60bc8e4aeeede2bc8c695c123a9"
            },
            {
                "mimetype": "video/mp4",
                "original_path": "/data/downloads/video2.mp4",
                "data_url": "/api/1/content/2/data",
                "url": "/api/1/content/2",
                "metadata_url": "/api/1/media/1512123",
                "added_timestamp": "2013-06-10T01:54:30.276543+00:00",
                "id": 2,
                "metadata_id": 1512123,
                "original_hash": "bf5b55c06882b60bc8e4aeeede2bc8c695c123a9"
            },
        ]
    }
