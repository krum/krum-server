.. _remote_player_implementation:

Remote Players
=================
Remote players are what provide content playback functionality to Krum. An example
is the Raspberry Pi player that can be remotely controlled via many krum clients.

**Note:** This document describes *version 1* of the Krum Remote Player API. If any
API breaking changes are required to support additional functionality, the version
number will be incremented.

Purpose
------------
A remote player for Krum only needs to register itself and allow krum to control it
for playback. It is intended that all playback of content from a Krum server be done
through a player that is actually a remote player of Krum. This is just so that your
playback session can be moved between players, paused, etc and kept up-to-date. This
is also how krum tracks what has been watched, as all playback history is tracked by
progress the remote players report back to Krum.

Implementing a remote player
-------------------------------
Remote players need to be able to register to Krum, respond to requests from Krum
and report playback events back to Krum. All of this is done via a single WebSocket
connection per Remote Player.

The :ref:`websocket_message_protocol` is used on top of
the raw WebSocket messaged based protocol to provide a means to send requests,
responses and events between Krum and the Remote Player.

To implement a player requires only responding correctly to requests on 2 different
paths:

#. **/meta:** To allow krum to get information about this player.
#. **/playback:** To allow krum to control the playback occuring on this player.

Players can obviously use the Krum APIs as much as they like, and provide whatever
other features they like, e.g. visualisations, info lookup, etc. A player could even
be a full client as well, allowing control of the playback session for example.

Registration to Krum
--------------------
**Note:** The :ref:`websocket_message_protocol` is used, not raw HTTP.

The registration steps are as follows:

#. Remote players make a WebSocket connection to Krum on the URL:
   ws://{krum server:port}/api/1/playback/players/register
#. Krum will accept the connection.
#. Krum will send a GET request to /meta
#. On response to the GET request, if the *version* attribute is not a version
   Krum can support, Krum will pick the latest version from
   *supported_versions* that it *can* support and add it to the payload for the
   PATCH request to /meta. If there aren't any versions available that Krum can
   support, it will close the websocket connection.
#. Krum will send a PATCH request for /meta which has the *registered_url* and
   *registered_name* attribute present. If the PATCH includes a different
   *version* value than what the player has started with, the remote player
   should change the API it will respond to on all paths and subsequent GET
   requests to /meta should reflect the new *version* also. As per
   :ref:`conventions`, Krum expects a 204 status response to the PATCH request.
   This PATCH is done to allow a remote player to "know" that it is registered
   rather than having to infer this from the GET query and the websocket
   staying open.
#. Finally, Krum will add the player to the listing at /api/1/playback/players,
   generating a unique name for the device if it doesn't already have one.

From here Krum will make requests on the /playback path, to
add items for playback, pause, etc.

The player when queried, via a :ref:`GET request <websocket_message_protocol_get_request>`,
on the "/meta" path, should supply JSON data in the following form::

    {
        version: 1,
        supported_versions: [1,2,etc],
        guid: "dead-beef-eaff-8a3e-ffff-ffff-ffff-ffff",
        client_type: "iPad",
        registered_url: "/api/1/playback/players/1",
        registered_name: "Web player 1",
        isvisible: true|false
    }

Each attribute has the following meaning:

version : number (read/write)
    The Krum Remote Player API version the player is currently running.
    This is always an integer. This should always default to the latest
    version the remote player supports.

supported_versions : array[number] (read)
    An array of Krum Remote Player API versions that this remote player
    can be told to operate under.

guid : string (read)
    A 16 byte UUID v4 value, represented as 32 hexidecimal digits. This
    should be generated once on the Remote Player and the same value
    returned on all subsequent requests. This should persist even across
    reboots of the hardware the remote player is running on.

    **Note 1:** Dashes are ignored, so pad it if you want to make it readable.

    **Note 2:** Krum uses the guid to make sure devices are always given
    the same name upon reboots/reconnects/etc.

client_type : string (read)
    A human readable string that describes the client. This is used to
    generate the initial name for the remote player for easier identification
    by users, e.g. iPad, Web UI, iPhone, etc. If possible, the devices name
    should be included, e.g. "Gecko - Web UI" where Gecko is the hostname of
    the computer the Web UI is running on.

registered_url : string (read/write)
    The URL for this player within Krums server side namespace. Krum sends
    this to the remote player in a PATCH to the /meta path. This allows a
    player to know that it has successfully registered to Krum, and know how
    to refer to itself when talking to the Krum server.

registered_name : string (read/write)
    The name for this player on Krum. Krum will send this in the same PATCH
    request as the registered_url. If the same GUID is provided every startup
    then this name will remain the same, but that's just a conveniance. The
    GUILD and the URL are the unique ways that krum internally refers to each
    player.

isvisible : true|false (read)
    This tells Krum whether or not to include this player in the list of
    players on /api/1/playback/players. This allows a players to register
    for smart playlists, session persistence, history tracking, etc, but
    they won't show up for others to select as players for their session.
    An example is an iPad player, we want it to be a remote player for the
    mentioned reasons, but the iPad user doesn't want the player to be
    controlled another device.

.. _remote_player_playlist_items:

Playlist items
--------------------

Playlist items are a JSON objects with the following form::

    {
        data_url: "http://something.somehwere/a_video_or_audio_file",
        title: "Family Guy S02E02 - Holy Crap",
        duration: 1537.3
    }

Each attribute has the following meaning:

data_url : string
    This is the URL of the raw binary content for the media file, e.g. an mp4.

title : string
    This is a pretty (i.e. human readable) name for the content/media.

duration : number (optional)
    This is the duration of the media at "data_url" in seconds. This
    is optional because the duration cannot be known for some types of media,
    e.g. streaming TV.

**Note:** Additional attributes MUST be allowed and should be be ignored by the
player.

Control requests
--------------------
The control of all playback is done through a single path on the Remote Player, "/playback".
Krum will make PATCH requests on that path. The path should also respond to GET requests
by returning the current state, in JSON, as follows::

    {
        paused: true|false,
        volume: 0.0-1.0,
        position: 2.1,
        playspeed: 1.0,
        playlist: [
            {playlist item 1},
            etc
        ],
    }

Each attribute has the following meaning:

paused : true|false (read/write)
    true or false. Send to pause/unpause playback.

volume : number (read/write)
    A real number between 0.0 and 1.0, with 0.0 representing complete silence and 1.0 the full
    volume of the player.

    There is currently no specific scaling that remote players should use (e.g. linear,
    logrithmic, etc).

position : string (read/write)
    When reading, is the playback position within the currently playing content, in seconds.
    When writing, an absolute value can be given or an offset in the form "+{offset}" or
    "-{offset}". Any resulting positions will be clamped within the range [0,duration].

playspeed : number (read/write)
    Is the playback speed of that the player will use. The speeds supported depend on the player.
    And actual playback speed may be reduced due to network latency and such. For example mplayer
    will allow up to 100x playback speed but this might be lower when streaming high quality
    content over lower speed networks (e.g. wireless G or such).

playlist : array (read/write)
    The list of :ref:`remote_player_playlist_items` to play. The first item will always be the currently
    playing content. All items will play once, in the order given. As an item finishes
    playing, it is removed from the list. When a PATCH request is made that includes
    this attribute, playback of the current item MUST only be interrupted if the first
    (0th) item is changed (or other attributes like paused/position are provided). This
    is to allow Krum to queue further content without affecting playback of the current
    item.

#. A URL for a video/audio resource
    e.g. an audio/mp4 encoded content.

#. A URL for an application/json resource
    The resource must be a JSON object includes a string "name" attribute and a "data_url" attribute that is the actual video/audio resource.

The PATCH requests that Krum sends will be to manipulate these very same parts of the
Remote Players state.

If multiple attributes are present in a single PATCH request they should be processed
in such a way that a subsequent GET request issued with no time passing would return
each attribute with the newly PATCHed value. An example is a PATCH that changes both
the playlist and position attributes. If the first item in the list is a new
content URL and the position was non-zero, e.g. "change to 5 minutes in on a new movie",
then the remote player should change the content it is playing and then seek to the
new position.http://www.javmodel.com/javdata/uploads/mild821_b.jpg

Examples
^^^^^^^^^
The following examples show the required JSON data, sent as the body of a PATCH request
on "/playback", for the described task.

#. Play a single file::

    {
        "playlist": [
            {
                "data_url": "http://krum:8888/api/1/content/4/data",
                "name": "Family Guy S02E02 - Holy Crap"
                "duration": 1537.3
            }
        ]
    }

#. Stop all playback::

    {
        "playlist": []
    }

#. Pause the currently playing file::

    {
        "paused": true
    }

#. Skip forward 5 seconds::

    {
        "position": "+5"
    }

Event notifications
---------------------
In order for Krum to keep track of what's going on, each Remote player needs to send
the following Events over the WebSocket.

All of the following examples will come through as Krum socket PATCH events on the
"/" path. The listed JSON is just the body of the EVENT message.

It is not incorrect to provide additional details in the PATCH events, e.g. providing
the duration attribute in a playback progress event, but this data will likely be
ignored. Events can be merged, however. For example a single event could merge both
attributes for reporting playback progress and a content change satisfying the
requirement to send each.

Playback progress
^^^^^^^^^^^^^^^^^^
This is so Krum can maintain up-to-date knowledge of watch history, whether content has
been completely watched, etc.

This event MUST be sent whilst there is content playing, as close to every 5 seconds as
managable. The event MUST include the position attribute.::

    {
        position: 25
    }

Content changed
^^^^^^^^^^^^^^^^
This is so Krum can track what content the Remote Player has left in it's content list,
providing additional content when required. The playlist attribute MUST always be
provided and the duration MUST be provided if the Remote Player is sill playing content.

This event MUST be sent whenever the playlist changes, either due to a request or
due to content completing playback and being removed from the playlist.

This first example is of the content list running out.::

    {
        playlist: []
    }

This second example could be used to indicate that the first item initially requested
has completed and the Remote Player has moved on to the second item.::

    {
        playlist: [
            {
                "data_url": "http://krum:8888/api/1/content/4/data",
                "name": "Family Guy S02E02 - Holy Crap"
                "duration": 1537.3
            },
            {
                "data_url": "http://krum:8888/api/1/content/5/data",
                "name": "Family Guy S02E03 - Da Boom"
                "duration": 1467.3
            }
        ]
    }

