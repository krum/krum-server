#Krum
##Prerequisites
### All platforms
* Python 3.4.x

### MacOS X

**Note:** Users of older versions of OS X may need to install a newer
    version of Python and a newer version of SQLite3, this is because
    Krum uses batching which the older versions of SQLite included in
    OS X don't seem to support.

### Linux

* Sqlite3. This is likely already installed, or already here because
  of Python. But if you're getting weird "cannot find module sqlite3"
  or database errors, this might be the cause.

### Windows
* Please update Python to use the latest SQLite:

  1. Download the latest SQLite DLL for Windows from [sqlite.org](http://www.sqlite.org/download.html).
  1. Take the sqlite3.dll inside and replace the Python's, it's
     located within the DLLs folder just inside wherever you installed
     Python, so the default is C:\Python27\DLLs.
  1. When you run **krum setup** you should no longer get a
     "django.db.utils.DatabaseError: no such module: fts3" error.

**Note 1:** We do this because the version of SQLite that comes with
    Python for Windows doesn't include the full text search modules
    (which Krum uses for fast media searching...).

##Setting up a development environment
1. Clone the git repository:

        $ git clone https://bitbucket.org/satook/krum_server.git

1. (optional) If using a virtualenv (pyenv, virtualenv etc)

**Note:** We recommend setting the KRUM_CONFIG_FILE environment
    variable within the activate script to point at (using an
    absolute path). This avoids pain if you're running a Krum server
    on the same machine as you do development.

1. Install Krum:

        $ python setup.py develop

    Using the develop flag tells [setuptools][1] to just link to the
    source code rather than installing it. This allows you to edit the
    code in the cloned repository, make commits, etc. The krum command
    line app will still run but referencing the repo code instead of
    an installed egg.

1. Configure Krum

    Just make a copy of krum/config/development.yml somewhere. Whilst
    you can give Krum a path to the configuration file as one of it's
    arguments, Krum will also look for the KRUM\_CONFIG\_FILE
    environment variable. If provided, Krum will load this
    configuration file and won't look for /etc/krum or ~/.krum.

1. Let Krum set himself up

        $ krum setup

1. Now run Krum

        $ krum run

    This runs Krum on port 8888, use --port to specify a different
    port. Krum will always bind all interfaces, though we haven't yet
    done any extensive IPV6 testing.

##Testing
Tests are written and run using [nose][2]

##Generating the docs

The documentation is written using [Sphinx][3]. All the source files
for the docs are in docs/source. To generate the docs just navigate to
docs directory and do the following:

    $ cd docs
    $ make

The html documentation should now be viewable from within the
docs/build/html directory. There's also a html\_install target within
the makefile that does a sudo'd copy of the HTML docs into
/usr/share/nginx/html/krum\_docs so that default Nginx installs will
then server out the files.

[1]: http://peak.telecommunity.com/DevCenter/setuptools
[2]: http://readthedocs.org/docs/nose/en/latest/
[3]: http://sphinx-doc.org/
